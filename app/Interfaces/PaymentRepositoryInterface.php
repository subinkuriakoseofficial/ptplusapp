<?php

namespace App\Interfaces;

interface PaymentRepositoryInterface 
{
    public function generatePaymentUrl($data);
    public function create($data);
}