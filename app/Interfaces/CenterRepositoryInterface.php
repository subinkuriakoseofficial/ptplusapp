<?php

namespace App\Interfaces;

interface CenterRepositoryInterface 
{
    public function getBanners();
    public function getParentCategories();
    public function getSubCategories($catId);
    public function getCentersByFilter($mainCategory = '', $category = '');
    public function getCenterSpecialities($centerId);
    public function getCenterTrainers($centerId, $limit = '');
    public function getPlans($centerId, $limit = '', $filterRating = '');
    public function getCenterProducts($centerId, $limit = '');
    public function getCenter($id);
    public function getTrainerGallery($centerId, $limit = '');

    public function addToWishlist($data);
    public function deleteWishlist($id);
    public function getWishlist($userId);
    public function checkWishlist($data);

    public function getBannerDataTable($request);
    public function deleteBanner($id);
    public function getCenterPlansTable($request);
}