<?php

namespace App\Interfaces;

interface HomeBannerRepositoryInterface 
{
    public function getForDataTable();
    public function create($data);
    public function find($id);
    public function get();
    public function update($where, $set);
    public function delete($id);
    public function getByCondition($where);
}