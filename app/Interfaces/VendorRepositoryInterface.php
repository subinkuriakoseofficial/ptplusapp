<?php

namespace App\Interfaces;

interface VendorRepositoryInterface 
{
    public function getVendors($limit = '');
    public function getParentCategories();
    public function getSubCategories($catId);
    public function getVendor($id);
    public function getAllCategories();
    public function getVendorProductsTable($vendorId);
}