<?php

namespace App\Interfaces;

interface CategoryRepositoryInterface 
{
    public function getMainCategoryDataTable();
    public function getCategoryDataTable();
    public function create($data);
    public function get($id);
    public function update($where, $set);
    public function findMainCategory($id);
    public function updateMainCategory($where, $set);
    public function createMainCategory($data);
    public function delete($id);
    public function deleteMainCategory($id);
    public function getMainCategories();
    public function getCategories();
    public function getByCondition($where);
}