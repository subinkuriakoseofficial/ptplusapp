<?php

namespace App\Interfaces;

interface TrainingPlanRepositoryInterface 
{
    public function getForDataTable();
    public function create($data);
    public function find($id);
    public function get();
    public function update($where, $set);
    public function delete($id);
    public function getByCondition($where);
    public function getBannerForDataTable($data);
    public function createBanner($data);
    public function findBanner($id);
    public function getBanner();
    public function updateBanner($where, $set);
    public function deleteBanner($id);
    public function getBannerByCondition($where);
}