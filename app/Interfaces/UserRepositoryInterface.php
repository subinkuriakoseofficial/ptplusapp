<?php

namespace App\Interfaces;

interface UserRepositoryInterface 
{
    public function getUser($id);
    public function createUser($data);
    public function getByEmail($email);
    public function addAddress($data);
    public function getAddressAll();
    public function getAddress($id);
    public function getAddressForUser($id);
    public function updateUser($where, $set);
    public function getUserDataTable($type='');
    public function getByType($type);
    public function delete($id);
}