<?php

namespace App\Interfaces;

interface BookingRepositoryInterface 
{
    public function get($id);
    public function create($data);
    public function myBookings();
    public function updateBooking($where, $set);
}