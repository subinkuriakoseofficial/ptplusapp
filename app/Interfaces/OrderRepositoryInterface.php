<?php

namespace App\Interfaces;

interface OrderRepositoryInterface 
{
    public function get($id);
    public function getByCondition($where);
    public function create($data);
    public function addOrderItems($data);
    public function addShippingAddress($data);
    public function myOrders();
    public function getOrderItemDetail($id);
    public function updateOrder($where, $set);
    public function getOrderDataTable();
}