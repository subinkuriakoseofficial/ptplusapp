<?php

namespace App\Interfaces;

interface TrainerRepositoryInterface 
{
    public function getTrainers($limit = '');
    public function getParentCategories();
    public function getSubCategories($catId);
    public function getFilterServiceTypes();
    public function getTrainersByFilter();
    public function getTrainer($id);
    public function getTrainerMainCategories($trainerId);
    public function getTrainerSpecialities($trainerId);
    public function getTrainerGallery($trainerId, $limit = '');
    public function getTrainerPlans($trainerId, $limit = '', $filterRating = '');
    public function getTrainerFavProducts($trainerId, $limit = '');
    public function getTrainerPlan($id);
    public function addToWishlist($data);
    public function deleteWishlist($id);
    public function getWishlist($userId);
    public function checkWishlist($data);
    public function getTrainerPlansSub($planId, $limit = '');
    public function getReview($trainerId);
    public function getAllCategories();
    public function deleteFavProduct($id);
    public function getAvailableTimeSlots($trainerId);
    public function getTrainerActivitiesTable($request);
    public function deleteActivity($id);
    public function getTrainerPlansTable($request);
    public function getTrainerFavProductsTable($request);
    public function addFavProducts($data);
    public function deleteFavProductByCondition($where);
    public function addRegActivities($data);
    public function deleteRegActivity($where);
    public function addRegCategories($data);
    public function deleteRegCategory($where);
    public function addRegMainCategories($data);
    public function deleteRegMainCategory($where);
    public function getTrainerSpecialitiesTable($request);
    public function deleteSpeciality($id);
    public function addRegSpecialities($data);
    public function getTrainerServiceTypesTable($request);
    public function addRegServiceTypes($data);
    public function deleteRegServiceType($where);
}