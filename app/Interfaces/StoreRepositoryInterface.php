<?php

namespace App\Interfaces;

interface StoreRepositoryInterface 
{
    public function getBanners();
    public function getCountries();
    public function getParentCategories();
    public function getSubCategories($catId);
    public function getBestSellers();
    public function getBestSellersAll();
    public function getNewArrivals();
    public function getNewArrivalsAll();
    public function getProducts();
    public function getProductsByCategory($catId);
    public function getProductDetail($id);
    public function getProductsByCategories($catId);
    public function isValidProduct($id);
    public function checkWishlist($data);
    public function addToWishlist($data);
    public function deleteWishlist($id);
    public function getWishlist($userId);
    public function updateProduct($where, $set);
    public function getProductsDataTable($data);
    public function getCategories();
    public function addProduct($data);
    public function addProductCategories($data);
    public function addProductGallery($data);
    public function deleteProductCategories($where);
    public function deleteProductGallery($where);
    public function deleteGalleryImage($id);
    public function deleteProduct($id);
}