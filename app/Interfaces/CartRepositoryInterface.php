<?php

namespace App\Interfaces;

interface CartRepositoryInterface 
{
    public function get($id);
    public function addToCart($data);
    public function getGuestCart($guest_user_id);
    public function getCart($userId);
    public function checkCartGuest($data);
    public function checkCart($data);
    public function updateCart($where, $set);
    public function deleteItem($where);
    public function clearCart($userId);
    public function clearCartGuest($guestId);
}