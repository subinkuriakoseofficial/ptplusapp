<?php

namespace App\Interfaces;

interface CommonRepositoryInterface 
{
    public function getCountries();
    public function getCitiesByCountry($countryId);
    public function getBlocksByCity($cityId);
    public function getActivities();
    public function getCategory($id);
    public function getMainCategories();
    public function getUserByToken($hashedToken);
    public function addToWishlist($data);
    public function deleteWishlist($id);
    public function getPlanWishlist($userId);
    public function checkPlanWishlist($data);
    public function getTrainingPlan($id);
    public function getCurrencies();
    public function getBanners();
    public function getCategories();
    public function getServiceTypes();
}