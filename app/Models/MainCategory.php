<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainCategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name_en', 'name_ar', 'thumbnail_img', 'slug'
    ];

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['name_ar']:$this->attributes['name_en'],
        );
    }

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function category()
    {
        return $this->hasMany(Category::class, 'main_category_id', 'id');
    }

}
