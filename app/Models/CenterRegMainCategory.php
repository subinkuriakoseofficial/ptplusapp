<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CenterRegMainCategory extends Model
{
    use HasFactory;

    public function center()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function mainCategory()
    {
        return $this->hasOne(MainCategory::class, 'id', 'main_category_id');
    }

}
