<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'booking_number', 'guest_user_id' ,'trainer_id', 'plan_id', 'center_id', 'service_type', 'appointment_date', 'appointment_time', 'payment_type', 'total_amount', 'currency'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id');
    }

    public function trainer()
    {
        return $this->hasOne(User::class, 'id', 'trainer_id');
    }

    public function plan()
    {
        return $this->hasOne(TrainingPlan::class, 'id', 'plan_id');
    }

    public function center()
    {
        return $this->hasOne(User::class, 'id', 'center_id');
    }
}
