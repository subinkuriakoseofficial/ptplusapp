<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name_en', 'name_ar', 'added_by' ,'brand_id', 'thumbnail_img', 'description_en', 'description_ar', 'unit_price', 'currency_id', 'current_stock', 'status', 'approved', 'slug'
    ];

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['name_ar']:$this->attributes['name_en'],
        );
    }

    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['description_ar']:$this->attributes['description_en'],
        );
    }

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function brand()
    {
        return $this->hasOne(Brand::class, 'id', 'brand_id');
    }

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->hasMany(ProductCategory::class, 'product_id', 'id');
    }

}
