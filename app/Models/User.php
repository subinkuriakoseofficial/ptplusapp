<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name', 'email', 'password', 'dob', 'user_type', 'phone_code', 'phone', 'email_verified_at', 'profile_picture', 'pswd_non_hashed'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function trainerRegCategories()
    {
        return $this->hasMany(TrainerRegCategory::class, 'user_id', 'id');
    }

    public function trainerRegMainCategories()
    {
        return $this->hasMany(TrainerRegMainCategory::class, 'user_id', 'id');
    }

    public function trainerRegServiceTypes()
    {
        return $this->hasMany(TrainerRegServiceType::class, 'user_id', 'id');
    }

    public function trainerRegActivities()
    {
        return $this->hasMany(TrainerRegActivity::class, 'user_id', 'id');
    }

    public function trainerRegSpecialities()
    {
        return $this->hasMany(TrainerRegSpeciality::class, 'user_id', 'id');
    }

    public function trainerDetails()
    {
        return $this->hasOne(TrainerDetail::class, 'user_id', 'id');
    }

    public function centerRegCategories()
    {
        return $this->hasMany(CenterRegCategory::class, 'user_id', 'id');
    }

    public function centerDetails()
    {
        return $this->hasOne(CenterDetail::class, 'user_id', 'id');
    }

    public function centerRegTypes()
    {
        return $this->hasMany(CenterRegType::class, 'user_id', 'id');
    }

    public function centerRegMainCategories()
    {
        return $this->hasMany(CenterRegMainCategory::class, 'user_id', 'id');
    }

    public function centerRegBanners()
    {
        return $this->hasMany(CenterRegBanner::class, 'center_id', 'id');
    }

    public function centerRegSpecialities()
    {
        return $this->hasMany(CenterRegSpeciality::class, 'user_id', 'id');
    }

    /*public function category()
    {
        return $this->hasManyThrough(Category::class, TrainerRegCategory::class, 'category_id', 'id', 'id', 'user_id');
    }*/
}
