<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Address extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'title', 'name' ,'email', 'phone_code', 'phone', 'country', 'state', 'pincode', 'street', 'area', 'block', 'building', 'type', 'gender'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
}
