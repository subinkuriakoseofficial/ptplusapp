<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id', 'name', 'email' ,'phone_code', 'phone', 'country', 'state', 'pincode', 'street', 'area', 'block', 'building', 'type', 'gender'
    ];

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }
}
