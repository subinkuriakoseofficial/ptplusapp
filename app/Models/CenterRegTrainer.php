<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CenterRegTrainer extends Model
{
    use HasFactory;

    public function trainer()
    {
        return $this->hasOne(User::class, 'id', 'trainer_id');
    }

    public function center()
    {
        return $this->hasOne(User::class, 'id', 'center_id');
    }
}
