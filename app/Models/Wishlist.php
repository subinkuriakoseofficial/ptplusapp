<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Wishlist extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'product_id', 'plan_id', 'trainer_id', 'center_id'];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function trainer()
    {
        return $this->hasOne(User::class, 'id', 'trainer_id');
    }

    public function center()
    {
        return $this->hasOne(User::class, 'id', 'center_id');
    }

    public function plan()
    {
        return $this->hasOne(TrainingPlan::class, 'id', 'plan_id');
    }
}
