<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Cart extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'guest_user_id',
        'product_id',
        'quantity',
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

}
