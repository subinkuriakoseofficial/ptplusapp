<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class StoreBanner extends Model
{
    use HasFactory;

    protected $fillable = [
        'title_en', 'title_ar', 'description_en', 'description_ar', 'image', 'status', 'product_id'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['title_ar']:$this->attributes['title_en'],
        );
    }

    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['description_ar']:$this->attributes['description_en'],
        );
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

}
