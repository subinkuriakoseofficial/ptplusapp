<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Casts\Attribute;

class ProductCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id', 'category_id'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    
}
