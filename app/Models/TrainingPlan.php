<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingPlan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'center_id', 'parent_id' ,'title_en', 'title_ar', 'sub_title_en', 'sub_title_ar', 'description_en', 'description_ar', 'num_of_trainers', 'thumbnail_img', 'slug', 'price', 'currency_id', 'status'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['title_ar']:$this->attributes['title_en'],
        );
    }

    protected function subTitle(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['sub_title_ar']:$this->attributes['sub_title_en'],
        );
    }

    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['description_ar']:$this->attributes['description_en'],
        );
    }

    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function banner()
    {
        return $this->hasMany(TrainingPlansBanner::class, 'training_plan_id', 'id');
    }

    public function review()
    {
        return $this->hasMany(Review::class, 'training_plan_id', 'id');
    }
    
}
