<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title_en', 'title_ar', 'thumbnail_img', 'status', 'slug'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['title_ar']:$this->attributes['title_en'],
        );
    }
}
