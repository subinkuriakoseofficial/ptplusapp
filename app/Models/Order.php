<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'delivery_date', 'delivery_type' ,'payment_type', 'sub_total', 'delivery_charge', 'total_amount', 'currency', 'order_number', 'guest_user_id'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function item()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function shippingAddress()
    {
        return $this->hasOne(ShippingAddress::class, 'order_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'order_id', 'id');
    }

}
