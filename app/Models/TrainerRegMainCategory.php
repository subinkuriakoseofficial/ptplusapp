<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainerRegMainCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'main_category_id'
    ];

    public function trainer()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function mainCategory()
    {
        return $this->hasOne(MainCategory::class, 'id', 'main_category_id');
    }
}
