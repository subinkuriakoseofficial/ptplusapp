<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'parent_id', 'main_category_id', 'name_en', 'name_ar', 'thumbnail_img', 'type', 'status', 'slug'
    ];

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (app()->getLocale()=='ar')?$this->attributes['name_ar']:$this->attributes['name_en'],
        );
    }

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }
}
