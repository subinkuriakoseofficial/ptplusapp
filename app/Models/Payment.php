<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id', 'booking_id', 'user_id' ,'guest_user_id', 'knet_payment_type', 'knet_payment_id', 'knet_result', 'knet_order_id', 'knet_post_date', 'knet_transaction_id', 'knet_reference_id', 'knet_track_id', 'knet_auth'
    ];

    protected function idEnc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Crypt::encryptString($this->attributes['id']),
        );
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function booking()
    {
        return $this->hasOne(Booking::class, 'id', 'booking_id');
    }

}
