<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class TrainingPlanBannerAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'plan_id' => 'required',
            'thumbnail' => 'required|mimes:jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'plan_id.required' => ':attribute is required',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
        ];
    }

    public function attributes()
    {
        return [
            'plan_id' => 'Plan id',
            'thumbnail' => 'Thumbnail Image',
        ];
    }
}
