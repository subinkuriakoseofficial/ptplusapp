<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TrainerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    public function rules()
    {
        try {
            $id = Crypt::decryptString($this->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'max:255', Rule::unique('users')->ignore($id)],
            'image' => 'mimes:jpg,png',
            'dob' => 'nullable|date_format:Y-m-d',
            'phone_code' => 'nullable|required_with:phone|numeric',
            'phone' => 'nullable|numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'email.required' => ':attribute is required',
            'email.unique' => ':attribute already registered',
            'image.mimes' => ':attribute must be in jpg or png format',
            'dob.date_format' => ':attribute must be in yyyy-mm-dd format',            
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'image' => 'Profile picture',
            'dob' => 'Date of Birth',
        ];
    }
}
