<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name_en' => 'required|max:255',
            'name_ar' => 'required|max:255',
            'category' => 'required',
            'stock' => 'required|numeric',
            'price' => 'required|numeric',
            'currency' => 'required',
            'thumbnail' => 'required|mimes:jpg,png',
            'images' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_en.required' => ':attribute is required',
            'name_ar.required' => ':attribute is required',
            'category.required' => ':attribute is required',
            'stock.required' => ':attribute is required',
            'stock.numeric' => ':attribute must be a numeric value',
            'price.required' => ':attribute is required',
            'price.numeric' => ':attribute must be a numeric value',
            'currency.required' => ':attribute is required',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'images.required' => ':attribute is required',
            'images.mimes' => ':attribute must be in jpg or png format',

        ];
    }

    public function attributes()
    {
        return [
            'name_en' => 'Name in English',
            'name_ar' => 'Name in Arabic',
            'category' => 'Category',
            'stock' => 'Stock',
            'price' => 'Price',
            'currency' => 'Currency',
            'thumbnail' => 'Thumbnail Image',
            'images' => 'Gallery Images',
        ];
    }
}
