<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TrainingPlanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->title_en, '-'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        try {
            $id = Crypt::decryptString($this->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        return [
            'behalf_of' => 'required',
            'trainer' => 'required_if:behalf_of,trainer',
            'center' => 'required_if:behalf_of,center',
            'title_en' => ['required', 'max:255', Rule::unique('training_plans')->ignore($id)],
            'title_ar' => 'required|max:255',
            'sub_title_en' => 'required|max:255',
            'sub_title_ar' => 'required|max:255',
            'price' => 'required|numeric',
            'currency' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'num_of_trainers' => 'required|numeric',
            'thumbnail' => 'mimes:jpg,png',
            'slug' => ['required', 'max:255', Rule::unique('training_plans')->ignore($id)],
        ];
    }

    public function messages()
    {
        return [
            'behalf_of.required' => ':attribute is required',
            'trainer.required_if' => ':attribute is required',
            'center.required_if' => ':attribute is required',
            'title_en.required' => ':attribute is required',
            'title_ar.required' => ':attribute is required',
            'sub_title_en.required' => ':attribute is required',
            'sub_title_ar.required' => ':attribute is required',
            'description_en.required' => ':attribute is required',
            'description_ar.required' => ':attribute is required',
            'num_of_trainers.required' => ':attribute is required',
            'num_of_trainers.numeric' => ':attribute must be a numeric value',
            'price.required' => ':attribute is required',
            'price.numeric' => ':attribute must be a numeric value',
            'currency.required' => ':attribute is required',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'slug.unique' => ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'behalf_of' => 'On behalf of',
            'trainer' => 'Trainer',
            'center' => 'Center',
            'title_en' => 'Title in English',
            'title_ar' => 'Title in Arabic',
            'sub_title_en' => 'Sub title in English',
            'sub_title_ar' => 'Sub title in Arabic',
            'description_en' => 'Description in English',
            'description_ar' => 'Description in Arabic',
            'num_of_trainers' => 'Number of trainers',
            'category' => 'Category',
            'stock' => 'Stock',
            'price' => 'Price',
            'currency' => 'Currency',
            'thumbnail' => 'Thumbnail Image',
            'slug' => 'Slug',
        ];
    }
}
