<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class CenterBannerAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    public function rules()
    {
        return [
            'title_en' => ['required', 'max:255', Rule::unique('center_banners')],
            'title_ar' => ['required', 'max:255', Rule::unique('center_banners')],
            'thumbnail' => 'required|mimes:jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'title_en.required' => ':attribute is required',
            'title_en.unique' => ':attribute must be unique',
            'title_ar.required' => ':attribute is required',
            'title_ar.unique' => ':attribute must be unique',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'slug.unique' => ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'title_en' => 'Name in English',
            'title_ar' => 'Name in Arabic',
            'thumbnail' => 'Thumbnail Image',
            'slug' => 'Slug',
        ];
    }
}
