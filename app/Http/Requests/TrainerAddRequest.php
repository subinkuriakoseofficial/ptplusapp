<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class TrainerAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'max:255', Rule::unique('users')],
            'image' => 'mimes:jpg,png',
            'dob' => 'nullable|date_format:Y-m-d',
            'phone_code' => 'nullable|required_with:phone|numeric',
            'phone' => 'nullable|numeric',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'email.required' => ':attribute is required',
            'email.unique' => ':attribute already registered',
            'image.mimes' => ':attribute must be in jpg or png format',
            'dob.date_format' => ':attribute must be in yyyy-mm-dd format',
            'password.required' => ':attribute is required',
            'password.min' => ':attribute must be minimum 6 characters',            
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'image' => 'Profile picture',
            'dob' => 'Date of Birth',
            'password' => 'Password',
        ];
    }
}
