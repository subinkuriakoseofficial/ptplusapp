<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class MainCategoryAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->name_en, '-'),
        ]);
    }

    public function rules()
    {
        return [
            'name_en' => ['required', 'max:255', Rule::unique('main_categories')],
            'name_ar' => ['required', 'max:255', Rule::unique('main_categories')],
            'thumbnail' => 'required|mimes:jpg,png',
            'slug' => ['required', 'max:255', Rule::unique('main_categories')],
        ];
    }

    public function messages()
    {
        return [
            'name_en.required' => ':attribute is required',
            'name_en.unique' => ':attribute must be unique',
            'name_ar.required' => ':attribute is required',
            'name_ar.unique' => ':attribute must be unique',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'slug.unique' => ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'name_en' => 'Name in English',
            'name_ar' => 'Name in Arabic',
            'thumbnail' => 'Thumbnail Image',
            'slug' => 'Slug',
        ];
    }
}
