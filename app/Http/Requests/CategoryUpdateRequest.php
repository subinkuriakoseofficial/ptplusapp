<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->name_en, '-'),
        ]);
    }

    public function rules()
    {
        try {
            $id = Crypt::decryptString($this->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        return [
            'main_category' => 'required',
            'name_en' => ['required', 'max:255', Rule::unique('categories')->ignore($id)],
            'name_ar' => ['required', 'max:255', Rule::unique('categories')->ignore($id)],
            'thumbnail' => 'mimes:jpg,png',
            'type' => 'required',
            'slug' => ['required', 'max:255', Rule::unique('categories')->ignore($id)],
        ];
    }

    public function messages()
    {
        return [
            'name_en.required' => ':attribute is required',
            'name_en.unique' => ':attribute must be unique',
            'name_ar.required' => ':attribute is required',
            'name_ar.unique' => ':attribute must be unique',
            'type.required' => ':attribute is required',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'slug.unique' => ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'name_en' => 'Name in English',
            'name_ar' => 'Name in Arabic',
            'thumbnail' => 'Thumbnail Image',
            'slug' => 'Slug',
            'type' => 'Type',
        ];
    }
}
