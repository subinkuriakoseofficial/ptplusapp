<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class StoreBannerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    public function rules()
    {
        try {
            $id = Crypt::decryptString($this->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_home_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        return [
            'title_en' => ['required', 'max:255', Rule::unique('store_banners')->ignore($id)],
            'title_ar' => ['required', 'max:255', Rule::unique('store_banners')->ignore($id)],
            'thumbnail' => 'mimes:jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'title_en.required' => ':attribute is required',
            'title_en.unique' => ':attribute must be unique',
            'title_ar.required' => ':attribute is required',
            'title_ar.unique' => ':attribute must be unique',
            'thumbnail.required' => ':attribute is required',
            'thumbnail.mimes' => ':attribute must be in jpg or png format',
            'slug.unique' => ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'title_en' => 'Name in English',
            'title_ar' => 'Name in Arabic',
            'thumbnail' => 'Thumbnail Image',
            'slug' => 'Slug',
        ];
    }
}
