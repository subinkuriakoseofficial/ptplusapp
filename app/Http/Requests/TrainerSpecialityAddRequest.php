<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TrainerSpecialityAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */

    public function rules()
    {
        return [
            'trainer_id' => ['required'],
            'title_en' => ['required', 'max:255'],
            'title_ar' => ['required', 'max:255'],
        ];

        try {
            $trainerId = Crypt::decryptString($this->trainer_id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to update the data');
        }
    }

    public function messages()
    {
        return [
            'title_en.required' => ':attribute is required',   
            'title_ar.required' => ':attribute is required',   
        ];
    }

    public function attributes()
    {
        return [
            'title_en' => 'Speciality title in english',
            'title_ar' => 'Speciality title in arabic',
        ];
    }
}
