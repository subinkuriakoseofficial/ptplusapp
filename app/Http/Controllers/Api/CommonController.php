<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\CommonRepositoryInterface;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CommonController extends Controller
{
    private CommonRepositoryInterface $commonRepository;

    public function __construct(CommonRepositoryInterface $commonRepository) 
    {
        $this->commonRepository = $commonRepository;
    }

    public function getBanners()
    {
        $banners = $this->commonRepository->getBanners();
        $banners = $banners->map(function ($bnr) {
            return array(
                'id' => $bnr->idEnc,
                'title' => $bnr->title,
                'description' => $bnr->description,
                'image' => Storage::disk('public')->url($bnr->image),
            );
        });
        return response()->json($banners);
    }

    public function getCountries()
    {
        $countries = $this->commonRepository->getCountries();
        $countries = $countries->map(function ($row) {
            return array(
                'id' => $row->id,
                'name' => $row->shortName,
                'phone_code' => $row->country_code,
            );
        });
        return response()->json($countries);
    }

    public function getCities(Request $request)
    {
        $rules = [
            'country_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $countryId = $request->country_id;
        $cities = $this->commonRepository->getCitiesByCountry($countryId);
        $cities = $cities->map(function ($row) {
            return array(
                'id' => $row->id,
                'name' => $row->name,
            );
        });
        return response()->json($cities);
    }

    public function getBlocks(Request $request)
    {
        $rules = [
            'city_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $cityId = $request->city_id;
        $cities = $this->commonRepository->getBlocksByCity($cityId);
        $cities = $cities->map(function ($row) {
            return array(
                'id' => $row->id,
                'name' => $row->name,
            );
        });
        return response()->json($cities);
    }

    public function getActivities()
    {
        $activities = $this->commonRepository->getActivities();
        $activities = $activities->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
            );
        });
        return response()->json($activities);
    }

    public function getMainCategories()
    {
        $categories = $this->commonRepository->getMainCategories();
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => $cat->idEnc,
                'name' => $cat->name,
                // 'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img), (storage/categories/barbells-plates.jpg)
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getWishlist()
    {
        $wishlist = $this->commonRepository->getPlanWishlist(Auth::id());
        $wishlist = $wishlist->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'plan_id' => Crypt::encryptString($row->plan_id),
                'name' => $row->plan->title,
                'thumbnail_img' => Storage::disk('public')->url($row->plan->thumbnail_img),
            );
        });
        return response()->json($wishlist);
    }

    public function addToWishlist(Request $request)
    {
        $rules = [
            'plan_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $planId = Crypt::decryptString($request->plan_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }

        // check product valid
        $validPlan = $this->commonRepository->getTrainingPlan($planId);
        if($validPlan == NULL) {
            return response()->json(['status' => false, 'message' => 'Invalid plan'], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'plan_id' => $planId,
            'type' => 'trainer'
        );

        // check if already exists
        $wishlistCount = $this->commonRepository->checkPlanWishlist($data);
        if($wishlistCount > 0) {
            return response()->json(['status' => false, 'message' => 'Item already exist.'], 201);
        }

        $wishlist = $this->commonRepository->addToWishlist($data);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Successfully added to the wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function deleteWishlist(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message, 201);
        }

        $wishlist = $this->commonRepository->deleteWishlist($id);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Deleted from wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    

}
