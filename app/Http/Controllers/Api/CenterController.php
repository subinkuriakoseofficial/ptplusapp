<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\TrainerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\CenterRepositoryInterface;

class CenterController extends Controller
{
    private TrainerRepositoryInterface $trainerRepository;
    private CommonRepositoryInterface $commonRepository;
    private CenterRepositoryInterface $centerRepository;

    public function __construct(TrainerRepositoryInterface $trainerRepository, CommonRepositoryInterface $commonRepository, CenterRepositoryInterface $centerRepository) 
    {
        $this->trainerRepository = $trainerRepository;
        $this->commonRepository = $commonRepository;
        $this->centerRepository = $centerRepository;
    }
    
    public function getBanners()
    {
        $banners = $this->centerRepository->getBanners();
        $banners = $banners->map(function ($bnr) {
            return array(
                'id' => $bnr->idEnc,
                'title' => $bnr->title,
                'description' => $bnr->description,
                'image' => Storage::disk('public')->url($bnr->image),
                'related_center_id' => Crypt::encryptString($bnr->user_id),
            );
        });
        return response()->json($banners);
    }

    public function getCategories()
    {
        $categories = $this->centerRepository->getParentCategories();
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => $cat->idEnc,
                'name' => $cat->name,
                // 'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img), (storage/categories/barbells-plates.jpg)
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getSubCategories($catId)
    {
        try {
            $catId = Crypt::decryptString($catId);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message);
        }

        $categories = $this->centerRepository->getSubCategories($catId);
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => Crypt::encryptString($cat->id),
                'name' => $cat->name,
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getCentersByFilter(Request $request)
    {
        if(isset($request->main_category)) {
            try {
                $mainCategory = Crypt::decryptString($request->main_category);
            } catch (DecryptException $e) {
                $message = ['status' => false, 'message' => 'Invalid main category id'];
                return response()->json($message);
            }
        }
        else {
            $mainCategory = '';
        }

        if(isset($request->category)) {
            try {
                $category = Crypt::decryptString($request->category);
            } catch (DecryptException $e) {
                $message = ['status' => false, 'message' => 'Invalid category id'];
                return response()->json($message);
            }
        }
        else {
            $category = '';
        }

        $centers = $this->centerRepository->getCentersByFilter($mainCategory, $category);
        $centers = $centers->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'name' => $row->name,
                'thumbnail_img' => Storage::disk('public')->url($row->profile_picture),
            );
        });

        return response()->json($centers);
    }

    public function getCenterDetail(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        $specialities = $this->centerRepository->getCenterSpecialities($centerId);
        $specialities = $specialities->map(function ($row) {
            return $row->speciality;
        });



        $trainers = $this->centerRepository->getCenterTrainers($centerId, 4);
        $trainers = $trainers->map(function ($row) {
            $categoryName = '';
            if(isset($row->trainer->trainerRegCategories[0]->category_id)) {
                $categoryId = $row->trainer->trainerRegCategories[0]->category_id;
                $category = $this->commonRepository->getCategory($categoryId);
                $categoryName = $category->name;
            }
            
            return array(
                'id' => $row->trainer->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->trainer->thumbnail_img),
                'name' => $row->trainer->name,
                'category' => $categoryName,
            );
        });

        $plans = $this->centerRepository->getPlans($centerId, 4);
        $plans = $plans->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'sub_title' => $row->sub_title,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
            );
        });

        $products = $this->centerRepository->getCenterProducts($centerId, 4);
        $products = $products->map(function ($row) {
            return array(
                'product_id' => $row->idEnc,
                'name' => $row->name,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'price' => $row->unit_price,
                'currency' => $row->currency->code,
            );
        });

        $gallery = $this->centerRepository->getTrainerGallery($centerId, 4);
        $gallery = $gallery->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'video' => Storage::disk('public')->url($row->video),
                'type' => $row->type,
            );
        });

        $center = $this->centerRepository->getCenter($centerId);

        $banner = $center->centerRegBanners;
        $bannerNew = [];
        foreach($banner as $bnr) {
            $bannerNew[] = Storage::disk('public')->url($bnr->image);
        }
        if(count($bannerNew) == 0) {
            $bannerNew[] = Storage::disk('public')->url($center->profile_picture);
        }

        $centerData = array(
            'id' => $center->idEnc,
            'name' => $center->name,
            'banner' => $bannerNew,
            'rating' => $center->centerDetails->rating,
            'phone' => $center->centerDetails->phone,
            'about' => $center->centerDetails->about,
            'address' => $center->centerDetails->address,
            'latitude' => $center->centerDetails->latitude,
            'longitude' => $center->centerDetails->longitude,
            'specialities' => $specialities,
            'gallery' => $gallery,
            'plans' => $plans,
            'products' => $products,
            'trainers' => $trainers,
        );

        return response()->json($centerData);
    }

    public function getTrainers(Request $request)
    {
        $rules = [
            'center_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->center_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        $trainers = $this->centerRepository->getCenterTrainers($centerId);
        $trainers = $trainers->map(function ($row) {
            $categoryName = '';
            if(isset($row->trainer->trainerRegCategories[0]->category_id)) {
                $categoryId = $row->trainer->trainerRegCategories[0]->category_id;
                $category = $this->commonRepository->getCategory($categoryId);
                $categoryName = $category->name;
            }
            
            return array(
                'id' => $row->trainer->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->trainer->thumbnail_img),
                'name' => $row->trainer->name,
                'category' => $categoryName,
            );
        });
        return response()->json($trainers);
    }

    public function getPlans(Request $request)
    {
        $rules = [
            'center_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->center_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        if(isset($request->filter_rating)) {
            $filterRating = $request->filter_rating;
            if($filterRating != 'asc' && $filterRating != 'desc') {
                $message = ['status' => false, 'message' => 'Invalid filter value'];
                return response()->json($message, 201);
            }
        }
        else {
            $filterRating = '';
        }

        $plans = $this->centerRepository->getPlans($centerId, '', $filterRating);
        $plans = $plans->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'sub_title' => $row->sub_title,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
            );
        });
        return response()->json($plans);
    }

    public function getProducts(Request $request)
    {
        $rules = [
            'center_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->center_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        $products = $this->centerRepository->getCenterProducts($centerId);
        $products = $products->map(function ($row) {
            return array(
                'product_id' => $row->idEnc,
                'name' => $row->name,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'price' => $row->unit_price,
                'currency' => $row->currency->code,
            );
        });
        return response()->json($products);
    }

    public function getGallery(Request $request)
    {
        $rules = [
            'center_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->center_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        $gallery = $this->centerRepository->getTrainerGallery($centerId);
        $gallery = $gallery->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'video' => Storage::disk('public')->url($row->video),
                'type' => $row->type,
            );
        });
        return response()->json($gallery);
    }

    public function getWishlist()
    {
        $wishlist = $this->centerRepository->getWishlist(Auth::id());
        $wishlist = $wishlist->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'center_id' => Crypt::encryptString($row->center_id),
                'name' => $row->center->name,
                'thumbnail_img' => Storage::disk('public')->url($row->center->profile_picture),
                'about' => $row->center->centerDetails->about,
            );
        });
        return response()->json($wishlist);
    }

    public function addToWishlist(Request $request)
    {
        $rules = [
            'center_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $centerId = Crypt::decryptString($request->center_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid center id'];
            return response()->json($message, 201);
        }

        // check center valid
        $validCenter = $this->centerRepository->getCenter($centerId);
        if($validCenter == NULL) {
            return response()->json(['status' => false, 'message' => 'Invalid center'], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'center_id' => $centerId,
            'type' => 'center'
        );

        // check if already exists
        $wishlistCount = $this->centerRepository->checkWishlist($data);
        if($wishlistCount > 0) {
            return response()->json(['status' => false, 'message' => 'Already exist.'], 201);
        }

        $wishlist = $this->centerRepository->addToWishlist($data);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Successfully added to the wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function deleteWishlist(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message, 201);
        }

        $wishlist = $this->centerRepository->deleteWishlist($id);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Deleted from wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    


}
