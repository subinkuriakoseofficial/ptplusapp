<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\TrainerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;

use function PHPSTORM_META\map;

class TrainerController extends Controller
{
    private TrainerRepositoryInterface $trainerRepository;
    private CommonRepositoryInterface $commonRepository;
    private StoreRepositoryInterface $storeRepository;

    public function __construct(TrainerRepositoryInterface $trainerRepository, CommonRepositoryInterface $commonRepository, StoreRepositoryInterface $storeRepository) 
    {
        $this->trainerRepository = $trainerRepository;
        $this->commonRepository = $commonRepository;
        $this->storeRepository = $storeRepository;
    }
    
    public function getTrainer()
    {
        $trainers = $this->trainerRepository->getTrainers(4);
        $trainers = $trainers->map(function ($row) {
            $categoryId = $row->trainerRegCategories[0]->category_id;
            $category = $this->commonRepository->getCategory($categoryId);
            return array(
                'id' => $row->idEnc,
                'name' => $row->name,
                'profile_picture' => Storage::disk('public')->url($row->profile_picture),
                'category' => $category->name,
            );
        });
        return response()->json($trainers);
    }

    public function getTrainerAll()
    {
        $trainers = $this->trainerRepository->getTrainers();
        $trainers = $trainers->map(function ($row) {
            $categoryId = $row->trainerRegCategories[0]->category_id;
            $category = $this->commonRepository->getCategory($categoryId);
            return array(
                'id' => $row->idEnc,
                'name' => $row->name,
                'profile_picture' => Storage::disk('public')->url($row->profile_picture),
                'category' => $category->name,
            );
        });
        return response()->json($trainers);
    }

    public function getCategories()
    {
        $categories = $this->trainerRepository->getParentCategories();
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => $cat->idEnc,
                'name' => $cat->name,
                // 'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img), (storage/categories/barbells-plates.jpg)
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getSubCategories($catId)
    {
        try {
            $catId = Crypt::decryptString($catId);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message);
        }

        $categories = $this->trainerRepository->getSubCategories($catId);
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => Crypt::encryptString($cat->id),
                'name' => $cat->name,
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getFilterCategories()
    {
        $categories = $this->trainerRepository->getAllCategories();
        $categoriesData = $categories->map(function ($cat) use ($categories) {
            if($cat->parent_id == 0) {
                $parentId = $cat->id;
                $subCategoriesData = [];
                foreach($categories as $subCat) {
                    if($subCat->parent_id == $parentId) {
                        $subCategoriesData[] = array(
                            'id' => $subCat->idEnc,
                            'name' => $subCat->name,
                        );
                    }
                }
                return array(
                    'id' => $cat->idEnc,
                    'name' => $cat->name,
                    'sub_categories' => $subCategoriesData,
                );
            }
        })
        ->reject(function ($value) {
            return $value === null;
        });

        return response()->json($categoriesData);
    }

    // temp for testing
    public function getFilterCategoriesTest()
    {
        $categories = $this->trainerRepository->getAllCategories();

        foreach($categories as $key=>$cat) {
            if($cat->parent_id == 0) {
                $filterCategory[$cat->id] = array(
                    'id' => $cat->idEnc,
                    'name' => $cat->name,
                    'sub_categories' => [],
                    'id_t' => $cat->id,
                    'parent_id_t' => $cat->parent_id,
                    'key_t' => $key,
                );
                unset($categories[$key]);
            }
        }

        $categoryArr = $categories->toArray();
        $categoryArr = array_values($categoryArr);
        $countCategories = count($categoryArr);
        // dd($categoryArr[0]['id']);
        if($countCategories > 0) {
            $step = 1;
            for($i=0; $i<$countCategories; $i++) {
                $parentId = $categoryArr[$i]['parent_id'];

                $subCategories = array(
                    'id' => Crypt::encryptString($categoryArr[$i]['id']),
                    'name' => $categoryArr[$i]['name_en'],
                    'sub_categories' => [],
                    'id_t' => $categoryArr[$i]['id'],
                    'parent_id_t' => $categoryArr[$i]['parent_id'],
                    'key_t' => $i,
                );

                
                if(isset($filterCategory[$parentId])) {
                    array_push($filterCategory[$parentId]['sub_categories'], $subCategories);
                    unset($categoryArr[$i]);
                    if($i == $countCategories-1) {
                        if(count($categoryArr) > 0) {
                            $i=0;
                            $step++;
                        }
                    }
                }
            }
        }


        return response()->json($filterCategory);
    }


    public function getFilterServiceTypes()
    {
        $serviceTypes = $this->trainerRepository->getFilterServiceTypes();
        $serviceTypes = $serviceTypes->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'group' => $row->groupTitle,
            );
        });
        $serviceTypes = $serviceTypes->groupBy('group');
        foreach($serviceTypes as $key=>$val) {
            $data[] = array(
                'type' => $key,
                'data' => $val
            );
        }
        return response()->json($data);
    }

    public function getTrainerByFilters(Request $request)
    {
        
        // $dataFilter['category'] = (isset($request->category_id)) ? $request->category_id : '';
        // $dataFilter['service_type'] = (isset($request->service_type)) ? $request->service_type : '';
        $serviceTypesNew = [];
        $activitiesNew = [];
        $categoriesNew = [];
        $availableStatus = '';
        $experience = [];

        if(isset($request->service_types)) {
            if(!is_array($request->service_types)) {
                return response()->json(['status' => false, 'message' => 'Service type must be an array'], 201);
            }

            $serviceTypes = $request->service_types;
            foreach($serviceTypes as $type) {
                try {
                    $serviceTypesNew[] = Crypt::decryptString($type);
                } catch (DecryptException $e) {
                    $message = ['status' => false, 'message' => 'Invalid service type id'];
                    return response()->json($message);
                }
            }
        }

        if(isset($request->categories)) {
            if(!is_array($request->categories)) {
                return response()->json(['status' => false, 'message' => 'Categories must be an array'], 201);
            }

            $categories = $request->categories;
            foreach($categories as $category) {
                try {
                    $categoriesNew[] = Crypt::decryptString($category);
                } catch (DecryptException $e) {
                    $message = ['status' => false, 'message' => 'Invalid category id'];
                    return response()->json($message);
                }
            }
        }

        if(isset($request->activities)) {
            if(!is_array($request->activities)) {
                return response()->json(['status' => false, 'message' => 'Activities must be an array'], 201);
            }

            $activities = $request->activities;
            foreach($activities as $activity) {
                try {
                    $activitiesNew[] = Crypt::decryptString($activity);
                } catch (DecryptException $e) {
                    $message = ['status' => false, 'message' => 'Invalid activity id'];
                    return response()->json($message);
                }
            }
        }

        if(isset($request->availability)) {
            ($request->availability == 1) ? $availableStatus = 'Available' : $availableStatus = '';
        }

        if(isset($request->experience)) {
            if(!is_array($request->experience)) {
                return response()->json(['status' => false, 'message' => 'Experience must be an array'], 201);
            }
            $experience = $request->experience;
            if(count($experience) != 2) {
                return response()->json(['status' => false, 'message' => 'Experience must be an array with exactly two elements'], 201);
            }
        }

        $trainers = $this->trainerRepository->getTrainersByFilter($serviceTypesNew, $categoriesNew, $activitiesNew, $availableStatus, $experience);
        $trainers = $trainers->map(function ($row) {
            $categoryId = $row->trainerRegCategories[0]->category_id;
            $category = $this->commonRepository->getCategory($categoryId);
            return array(
                'id' => $row->idEnc,
                'name' => $row->name,
                'profile_picture' => Storage::disk('public')->url($row->profile_picture),
                'category' => $category->name,
                'experience' => $row->trainerDetails->experience.' Years',
            );
        });
        return response()->json($trainers);
    }

    public function getTrainerDetail(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        $mainCategories = $this->trainerRepository->getTrainerMainCategories($trainerId);
        $mainCategories = $mainCategories->map(function ($row) {
            return $row->mainCategory->name;
        });

        $specialities = $this->trainerRepository->getTrainerSpecialities($trainerId);
        $specialities = $specialities->map(function ($row) {
            return $row->speciality;
        });

        $gallery = $this->trainerRepository->getTrainerGallery($trainerId, 4);
        $gallery = $gallery->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'video' => Storage::disk('public')->url($row->video),
                'type' => $row->type,
            );
        });

        $plans = $this->trainerRepository->getTrainerPlans($trainerId, 4);
        $plans = $plans->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'sub_title' => $row->sub_title,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
            );
        });

        $favProducts = $this->trainerRepository->getTrainerFavProducts($trainerId, 4);
        $favProducts = $favProducts->map(function ($row) {
            return array(
                'product_id' => $row->product->idEnc,
                'name' => $row->product->name,
                'thumbnail_img' => Storage::disk('public')->url($row->product->thumbnail_img),
                'price' => $row->product->unit_price,
                'currency' => $row->product->currency->code,
            );
        });

        $trainer = $this->trainerRepository->getTrainer($trainerId);

        // get trainer reviews
        $reviews = $this->trainerRepository->getReview($trainerId);
        $reviewData = [];
        foreach($reviews as $review) {
            $reviewData[] = array(
                'user_dp' => ($review->user->profile_picture != NULL) ? Storage::disk('public')->url($review->user->profile_picture): Storage::disk('public')->url('user/profile-picture-default.png'),
                'user_name' => $review->user->name,
                'review' => $review->review,
            );
        }

        // get available time slots for current date
        $timeSlots = $this->trainerRepository->getAvailableTimeSlots($trainerId);
        $timeSlotData = [];
        $timeSlots = $timeSlots->map(function($timeSlot){
            return array(
                        'time_slot' => $timeSlot->time_slot,
                        'part' => $timeSlot->part,
                    );
        });
        $timeSlots = $timeSlots->groupBy('part');
        foreach($timeSlots as $key=>$val) {
            $slots = [];
            foreach($timeSlots[$key] as $slot) {
                $slots[] = $slot['time_slot'];
            }
            $timeSlotData[] = array(
                'part' => $key,
                'slots' => $slots
            );
        }

        $categoryId = $trainer->trainerRegCategories[0]->category_id;
        $category = $this->commonRepository->getCategory($categoryId);
        $trainerData = array(
            'id' => $trainer->idEnc,
            'name' => $trainer->name,
            'profile_picture' => Storage::disk('public')->url($trainer->profile_picture),
            'category' => $category->name,
            'experience' => $trainer->trainerDetails->experience.' Years',
            'rating' => $trainer->trainerDetails->rating,
            'phone' => $trainer->trainerDetails->phone,
            'status' => $trainer->trainerDetails->status,
            'about' => $trainer->trainerDetails->about,
            'address' => $trainer->trainerDetails->address,
            'latitude' => $trainer->trainerDetails->latitude,
            'longitude' => $trainer->trainerDetails->longitude,
            'main_categories' => $mainCategories,
            'specialities' => $specialities,
            'gallery' => $gallery,
            'plans' => $plans,
            'favourite_products' => $favProducts,
            'review' => $reviewData,
            'time_slots' => $timeSlotData,
        );

        return response()->json($trainerData);
    }

    public function getTrainerTimeSlotsAvailable(Request $request)
    {
        $rules = [
            'trainer_id' => 'required',
            'date' => 'required|date_format:Y-m-d',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        $timeSlots = $this->trainerRepository->getAvailableTimeSlots($trainerId);
        $timeSlotData = [];
        $timeSlots = $timeSlots->map(function($timeSlot){
            return array(
                        'time_slot' => $timeSlot->time_slot,
                        'part' => $timeSlot->part,
                    );
        });
        $timeSlots = $timeSlots->groupBy('part');
        foreach($timeSlots as $key=>$val) {
            $slots = [];
            foreach($timeSlots[$key] as $slot) {
                $slots[] = $slot['time_slot'];
            }
            $timeSlotData[] = array(
                'part' => $key,
                'slots' => $slots
            );
        }

        return response()->json($timeSlotData);
    }

    public function getPlans(Request $request)
    {
        $rules = [
            'trainer_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        if(isset($request->filter_rating)) {
            $filterRating = $request->filter_rating;
            if($filterRating != 'asc' && $filterRating != 'desc') {
                $message = ['status' => false, 'message' => 'Invalid filter value'];
                return response()->json($message, 201);
            }
        }
        else {
            $filterRating = '';
        }

        $plans = $this->trainerRepository->getTrainerPlans($trainerId, '', $filterRating);
        $plans = $plans->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => $row->title,
                'sub_title' => $row->sub_title,
                'num_of_trainers' => $row->num_of_trainers,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
            );
        });
        return response()->json($plans);
    }

    public function getGallery(Request $request)
    {
        $rules = [
            'trainer_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        $gallery = $this->trainerRepository->getTrainerGallery($trainerId);
        $gallery = $gallery->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'thumbnail_img' => Storage::disk('public')->url($row->thumbnail_img),
                'video' => Storage::disk('public')->url($row->video),
                'type' => $row->type,
            );
        });
        return response()->json($gallery);
    }

    public function getFavProducts(Request $request)
    {
        $rules = [
            'trainer_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $favProducts = $this->trainerRepository->getTrainerFavProducts($trainerId);
        $favProducts = $favProducts->map(function ($row) use ($wishlist) {
            return array(
                'product_id' => $row->product->idEnc,
                'name' => $row->product->name,
                'thumbnail_img' => Storage::disk('public')->url($row->product->thumbnail_img),
                'price' => $row->product->unit_price,
                'currency' => $row->product->currency->code,
                'wishlisted' => (in_array($row->product->id, $wishlist) ? true : false)
            );
        });

        return response()->json($favProducts);
    }

    public function getPlanDetail($id)
    {
        try {
            $planId = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid plan id'];
            return response()->json($message, 201);
        }

        $plan = $this->trainerRepository->getTrainerPlan($planId);
        $planData = [];
        if($plan) {
            $banner = $plan->banner;
            $bannerNew = [];
            foreach($banner as $bnr) {
                $bannerNew[] = Storage::disk('public')->url($bnr->image);
            }
            if(count($bannerNew) == 0) {
                $bannerNew[] = Storage::disk('public')->url($plan->thumbnail_img);
            }
            // Get sub plans
            $subSplans = $this->trainerRepository->getTrainerPlansSub($planId);
            $subSplans = $subSplans->map(function($row) {
                return array(
                    'id' => $row->idEnc,
                    'title' => $row->title,
                ); 
            });

            $reviewData = [];
            foreach($plan->review as $review) {
                $reviewData[] = array(
                    'user_dp' => ($review->user->profile_picture != NULL) ? Storage::disk('public')->url($review->user->profile_picture): Storage::disk('public')->url('user/profile-picture-default.png'),
                    'user_name' => $review->user->name,
                    'review' => $review->review,
                );
            }

            $planData = array(
                'id' => $plan->idEnc,
                'title' => $plan->title,
                'sub_title' => $plan->sub_title,
                'num_of_trainers' => $plan->num_of_trainers,
                'description' => $plan->description,
                'price' => $plan->price,
                'currency' => $plan->currency->code,
                // 'thumbnail_img' => Storage::disk('public')->url($plan->thumbnail_img),
                'banner' => $bannerNew,
                'sub_plans' => $subSplans,
                'review' => $reviewData,
            );
        }
        return response()->json($planData);
    }

    public function getWishlist()
    {
        $wishlist = $this->trainerRepository->getWishlist(Auth::id());
        $wishlist = $wishlist->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'trainer_id' => Crypt::encryptString($row->trainer_id),
                'name' => $row->trainer->name,
                'thumbnail_img' => Storage::disk('public')->url($row->trainer->profile_picture),
            );
        });
        return response()->json($wishlist);
    }

    public function addToWishlist(Request $request)
    {
        $rules = [
            'trainer_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }

        // check product valid
        $validTrainer = $this->trainerRepository->getTrainer($trainerId);
        if($validTrainer == NULL) {
            return response()->json(['status' => false, 'message' => 'Invalid traienr'], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'trainer_id' => $trainerId,
            'type' => 'trainer'
        );

        // check if already exists
        $wishlistCount = $this->trainerRepository->checkWishlist($data);
        if($wishlistCount > 0) {
            return response()->json(['status' => false, 'message' => 'Item already exist.'], 201);
        }

        $wishlist = $this->trainerRepository->addToWishlist($data);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Successfully added to the wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function deleteWishlist(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message, 201);
        }

        $wishlist = $this->trainerRepository->deleteWishlist($id);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Deleted from wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    


}
