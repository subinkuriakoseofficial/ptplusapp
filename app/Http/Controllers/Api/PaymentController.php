<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;

use App\Interfaces\PaymentRepositoryInterface;
use App\Interfaces\OrderRepositoryInterface;

class PaymentController extends Controller
{
    private PaymentRepositoryInterface $paymentRepository;
    private OrderRepositoryInterface $orderRepository;

    public function __construct(PaymentRepositoryInterface $paymentRepository, OrderRepositoryInterface $orderRepository) 
    {
        $this->paymentRepository = $paymentRepository;
        $this->orderRepository = $orderRepository;
    }

    public function paymentSuccess(Request $request)
    {
        $orderId = $request->OrderID;
        $result = $request->Result;

        $where = array('order_number' => $orderId);
        $order = $this->orderRepository->getByCondition($where);
        $userId = NULL;
        $guestUserId = NULL;
        $orderId = NULL;
        foreach($order as $odr) {
            $orderId = $odr->id;
            $userId = $odr->user_id;
            $guestUserId = $odr->guest_user_id;
        }
        if($orderId == null) {
            $message = ['status' => false, 'message' => 'An error occured with the order'];
            return response()->json($message, 201);
        }
        
        $dataPayment = array(
            'order_id' => $orderId,
            'user_id' => $userId,
            'guest_user_id' => $guestUserId,
            'knet_payment_type' => @$request->payment_type,
            'knet_payment_id' => $request->PaymentID,
            'knet_result' => $result,
            'knet_order_id' => $request->OrderID,
            'knet_post_date' => $request->PostDate,
            'knet_transaction_id' => $request->TranID,
            'knet_reference_id' => $request->Ref,
            'knet_track_id' => $request->TrackID,
            'knet_auth' => $request->Auth,
        );

        $payment = $this->paymentRepository->create($dataPayment);
        if($payment) {
            // update order payment status
            if($result == 'CAPTURED') {
                $where = array('id' => $orderId);
                $set = array('payment_status' => 1);
                $updateOrder = $this->orderRepository->updateOrder($where, $set);
                if($updateOrder) {
                    $message = ['status' => true, 'message' => 'Payment completed successfully', 'order_id' => $orderId];
                    return response()->json($message);
                }
            }
            else {
                $message = ['status' => false, 'message' => 'Payment failed'];
                return response()->json($message);
            }
        }

    }

    public function paymentError(Request $request)
    {
        $orderId = $request->OrderID;
        $result = $request->Result;

        $where = array('order_number' => $orderId);
        $order = $this->orderRepository->getByCondition($where);
        $userId = NULL;
        $guestUserId = NULL;
        $orderId = NULL;
        foreach($order as $odr) {
            $orderId = $odr->id;
            $userId = $odr->user_id;
            $guestUserId = $odr->guest_user_id;
        }
        if($orderId == null) {
            $message = ['status' => false, 'message' => 'An error occured with the order'];
            return response()->json($message, 201);
        }
        
        $dataPayment = array(
            'order_id' => $orderId,
            'user_id' => $userId,
            'guest_user_id' => $guestUserId,
            'knet_payment_type' => @$request->payment_type,
            'knet_payment_id' => $request->PaymentID,
            'knet_result' => $result,
            'knet_order_id' => $request->OrderID,
            'knet_post_date' => $request->PostDate,
            'knet_transaction_id' => $request->TranID,
            'knet_reference_id' => $request->Ref,
            'knet_track_id' => $request->TrackID,
            'knet_auth' => $request->Auth,
        );

        $payment = $this->paymentRepository->create($dataPayment);
        
        $message = ['status' => false, 'message' => 'Payment failed'];
        return response()->json($message);
    }

    

    

}
