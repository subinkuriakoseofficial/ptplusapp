<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\CartRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;

class CartController extends Controller
{
    private CartRepositoryInterface $cartRepository;
    private StoreRepositoryInterface $storeRepository;

    public function __construct(CartRepositoryInterface $cartRepository, StoreRepositoryInterface $storeRepository) 
    {
        $this->cartRepository = $cartRepository;
        $this->storeRepository = $storeRepository;
    }
    
    public function addToCart(Request $request)
    {
        $rules = [
            'product_id' => 'required',
            'quantity' => 'required|numeric',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }

        // check product valid
        $validProduct = $this->storeRepository->isValidProduct($productId);
        if($validProduct != 1) {
            return response()->json(['status' => false, 'message' => 'Invalid product'], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'product_id' => $productId,
            'quantity' => $request->quantity,
        );

        // check product already added
        $cartCount = $this->cartRepository->checkCart($data);
        if($cartCount > 0) {
            return response()->json(['status' => false, 'message' => 'Item already exist'], 201);
        }

        $cart = $this->cartRepository->addToCart($data);
        if($cart) {
            return response()->json(['status' => true, 'message' => 'Item successfully added to the cart.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getCart()
    {
        $userId = Auth::id();
        $cart = $this->cartRepository->getCart($userId);
        $cartCount = count($cart);
        if($cartCount == 0) {
            return response()->json(['status' => false, 'message' => 'There are no items in your cart.']);
        }
        $currency = $cart[0]->product->currency->code;
        $subTotal = 0;
        foreach($cart as $row){ 
            $cartFinal[] = array(
                'id' => $row->idEnc,
                'product_id' => $row->product->idEnc,
                'product_name' => $row->product->name,
                'quantity' => $row->quantity,
                'unit_price' => $row->product->unit_price,
                'currency' => $row->product->currency->code,
                'thumbnail_img' => Storage::disk('public')->url($row->product->thumbnail_img),
            );
            $subTotal += ($row->product->unit_price * $row->quantity);
        }
        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;
        $data = array(
            'sub_total' => $subTotal,
            'delivery_charge' => $deliveryCharge,
            'total' => $total,
            'currency' => $currency,
        );
        return response()->json(['status' => true, 'item' => $cartFinal, 'order_info' => $data]);
    }

    public function updateCart(Request $request)
    {
        $rules = [
            'cart_id' => 'required',
            'quantity' => 'required|numeric',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $cartId = Crypt::decryptString($request->cart_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid cart id'];
            return response()->json($message, 201);
        }

        $quantity = $request->quantity;

        $where = array(
            'user_id' => Auth::id(),
            'id' => $cartId,
        );

        // delete item if quantity is zero
        if($quantity <= 0) {
            $delete = $this->cartRepository->deleteItem($where);
            if($delete) {
                return response()->json(['status' => true, 'message' => 'Cart updated.']);
            }
            return response()->json(['status' => false, 'message' => 'Unable to update the cart.'], 201);
        }

        $set = array(
            'quantity' => $quantity
        );

        $update = $this->cartRepository->updateCart($where, $set);
        if($update) {
            return response()->json(['status' => true, 'message' => 'Cart updated.']);
        }
        return response()->json(['status' => false, 'message' => 'Unable to update the cart.'], 201);

    }

    


}
