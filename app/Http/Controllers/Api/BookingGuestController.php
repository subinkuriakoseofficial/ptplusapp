<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use \Validator;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\TrainerRepositoryInterface;
use App\Interfaces\BookingRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

class BookingGuestController extends Controller
{
    private TrainerRepositoryInterface $trainerRepository;
    private BookingRepositoryInterface $bookingRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(TrainerRepositoryInterface $trainerRepository, BookingRepositoryInterface $bookingRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->trainerRepository = $trainerRepository;
        $this->bookingRepository = $bookingRepository;
        $this->commonRepository = $commonRepository;
    }

    public function create(Request $request)
    {
        $rules = [
            'guest_id' => 'required',
            'appointment_date' => 'required|date_format:Y-m-d',
            'appointment_time' => 'required|string',
            'trainer_id' => 'required',
            'plan_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        try {
            $planId = Crypt::decryptString($request->plan_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid plan id'];
            return response()->json($message, 201);
        }
        
        $trainer = $this->trainerRepository->getTrainer($trainerId);
        if($trainer == null) {
            return response()->json(['status' => false, 'message' => 'The trainer you are trying to book is not available.'], 201);
        }
        $categoryId = $trainer->trainerRegCategories[0]->category_id;
        $category = $this->commonRepository->getCategory($categoryId);

        $plan = $this->trainerRepository->getTrainerPlan($planId);
        if($plan == null) {
            return response()->json(['status' => false, 'message' => 'The plan you are trying to book is not available.'], 201);
        }

        $orderNumber = Str::random(6);
        $bookingData = array(
            'booking_number' => $orderNumber,
            'guest_user_id' => $request->guest_id,
            'trainer_id' => $trainerId,
            'plan_id' => $planId,
            'service_type' => 'trainer',
            'appointment_date' => $request->appointment_date,
            'appointment_time' => $request->appointment_time,
            'payment_type' => 'cod',
            'total_amount' => $plan->price,
            'currency' => $plan->currency->code,
        );

        $order = $this->bookingRepository->create($bookingData);
        if($order) {
            // update booking number with unique string
            $orderNumber = $order->id . '_' . $orderNumber;
            $where = array('id' => $order->id);
            $set = array('booking_number' => $orderNumber);
            $updateOrder = $this->bookingRepository->updateBooking($where, $set);
            
            $response = array(
                'order_id' => $orderNumber,
                'trainer_name' => $trainer->name,
                'trainer_category' => $category->name,
                'trainer_image' => ($trainer->profile_picture != null) ? Storage::disk('public')->url($trainer->profile_picture) : '',
                'trainer_address' => $trainer->trainerDetails->address,
                'appointment_date' => $request->appointment_date,
                'appointment_time' => $request->appointment_time,

            );
            return response()->json(['status' => true, 'message' => 'Booking completed successfully.', 'data' => $response]);
        }
        return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
    }

    


}
