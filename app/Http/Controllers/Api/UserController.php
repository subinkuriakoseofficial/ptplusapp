<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Interfaces\UserRepositoryInterface;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Storage;

class UserController extends Controller
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository) 
    {
        $this->userRepository = $userRepository;
    }
    
    public function addAddress(Request $request)
    {
        $userId = Auth::id();
        $rules = [
            'title' => [Rule::unique('addresses')->where(fn ($query) => $query->where('user_id', $userId)), 'string'],
            'name' => 'required|string',
            'email' => 'email',
            'phone_code' => 'required',
            'phone' => 'required|numeric',
            'country' => 'required|string',
            'state' => 'string',
            'pincode' => 'required|numeric',
            'type' => 'in:home,work,other',
            'gender' => 'in:male,female,other',
        ];
        $messages = [
            'type' => 'The :attribute is invalid.',
        ];
        $attributes = [
            'type' => 'address type',
        ];
        $validator = Validator::make($request->all(), $rules, $messages, $attributes);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'title' => $request->title,
            'name' => $request->name,
            'email' => $request->email,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'state' => $request->state,
            'pincode' => $request->pincode,
            'street' => $request->street,
            'area' => $request->area,
            'block' => $request->block,
            'building' => $request->building,
            'type' => $request->type,
            'gender' => $request->gender,
        );
        $user = $this->userRepository->addAddress($data);
        if($user) {
            return response()->json(['status' => true, 'message' => 'Address added successfully.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getAddress()
    {
        $addresses = $this->userRepository->getAddressAll();
        $addresses = $addresses->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'title' => ($row->title) ? $row->title : '',
                'name' => $row->name,
                'email' => ($row->email) ? $row->email : '',
                'phone_code' => $row->phone_code,
                'phone' => $row->phone,
                'country' => $row->country,
                'state' => ($row->state) ? $row->state : '',
                'pincode' => $row->pincode,
                'street' => ($row->street) ? $row->street : '',
                'area' => ($row->area) ? $row->area : '',
                'block' => ($row->block) ? $row->block : '',
                'building' => ($row->building) ? $row->block : '',
                'gender' => ($row->gender) ? $row->gender : '',
                'type' => ($row->type) ? $row->type : ''
            );
        });
        return response()->json($addresses);
    }

    public function updateDp(Request $request)
    {
        // dd($request);
        $userId = Auth::id();
        if($request->hasFile('profile_picture')){
            $this->_removeDp($userId);
            $profilepicture = $request->file('profile_picture');
            $logoPath = 'user';
            $logoName = Storage::disk('public')->putFile($logoPath, $profilepicture);
            $set['profile_picture'] = $logoName;
        }
        else {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.'], 201);
        }
        $where = array(
            'id' => $userId
        );
        $user = $this->userRepository->updateUser($where, $set);
        if($user) {
            return response()->json(['status' => true, 'message' => 'Profile picture updated successfully.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    private function _removeDp($id){
        $user = $this->userRepository->getUser($id);
        if($user->profile_picture != NULL) {
            if(Storage::disk('public')->delete($user->profile_picture)){
                return true;
            }
        }
        return false;
    }

    public function updateProfile(Request $request)
    {
        $userId = Auth::id();
        $rules = [
            'name' => 'required|string',
            'email' => [Rule::unique('users')->ignore($userId), 'required', 'email'],
            'phone_code' => 'required',
            'phone' => 'required|numeric',
            'country' => 'string',
            'state' => 'string',
            'gender' => 'in:male,female,other',
            'dob' => 'date_format:Y-m-d',
        ];
        $messages = [
            'dob' => 'The :attribute must be in YYYY-MM-DD format.',
        ];
        $attributes = [
            'dob' => 'date of birth',
        ];
        $validator = Validator::make($request->all(), $rules, $messages, $attributes);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $userId = Auth::id();
        $set['name'] = $request->name;
        $set['email'] = $request->email;
        $set['phone_code'] = $request->phone_code;
        $set['phone'] = $request->phone;
        $set['country'] = $request->country;
        $set['gender'] = $request->gender;
        $set['dob'] = $request->dob;
        $where = array(
            'id' => $userId
        );
        $user = $this->userRepository->updateUser($where, $set);
        if($user) {
            return response()->json(['status' => true, 'message' => 'Profile updated successfully.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getProfile()
    {
        $userId = Auth::id();
        $user = $this->userRepository->getUser($userId);
        $user = array(
            'name' => $user->name,
            'email' => $user->email,
            'phone_code' => $user->phone_code,
            'phone' => $user->phone,
            'country' => ($user->country) ? $user->country : '',
            'gender' => ($user->gender) ? $user->gender : '',
            'dob' => ($user->dob) ? $user->dob : ''
        );
        return response()->json($user);
    }



}
