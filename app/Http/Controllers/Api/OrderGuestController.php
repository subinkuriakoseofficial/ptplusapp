<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\OrderRepositoryInterface;
use App\Interfaces\CartRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\PaymentRepositoryInterface;

class OrderGuestController extends Controller
{
    private OrderRepositoryInterface $orderRepository;
    private CartRepositoryInterface $cartRepository;
    private StoreRepositoryInterface $storeRepository;
    private UserRepositoryInterface $userRepository;
    private PaymentRepositoryInterface $paymentRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, CartRepositoryInterface $cartRepository, StoreRepositoryInterface $storeRepository, UserRepositoryInterface $userRepository, PaymentRepositoryInterface $paymentRepository) 
    {
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
        $this->storeRepository = $storeRepository;
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepository;
    }
    
    public function getDeliveryCharge(Request $request)
    {
        $rules = [
            'guest_id' => 'required',
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_pincode' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }
        
        $guestId = $request->guest_id;
        $cart = $this->cartRepository->getGuestCart($guestId);
        $cartCount = count($cart);
        if($cartCount == 0) {
            return response()->json(['status' => false, 'message' => 'There are no items in your cart.'], 201);
        }
        $currency = $cart[0]->product->currency->code;
        $subTotal = 0;
        foreach($cart as $row){ 
            // check is product valid
            if($row->product->status != 1 || $row->product->current_stock < $row->quantity) {
                return response()->json(['status' => false, 'message' => 'One or more products in your cart are out of stock.'], 201);
            }
            $subTotal += ($row->product->unit_price * $row->quantity);
        }

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;
        $response = array(
            'sub_total' => $subTotal,
            'delivery_charge' => $deliveryCharge,
            'total' => $total,
            'currency' => $currency,
        );
        
        return response()->json(['status' => true, 'data' => $response]);
    }

    public function create(Request $request)
    {
        $rules = [
            'guest_id' => 'required',
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_name' => 'required|string',
            'shipping_email' => 'email',
            'shipping_phone_code' => 'required',
            'shipping_phone' => 'required',
            'shipping_country' => 'required|string',
            'shipping_state' => 'nullable|string',
            'shipping_pincode' => 'required',
            'shipping_street' => 'nullable|string',
            'shipping_area' => 'nullable|string',
            'shipping_block' => 'nullable|string',
            'shipping_building' => 'nullable|string',
            'shipping_type' => 'in:home,work,other',
            'shipping_gender' => 'in:male,female,other',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }
        
        $guestId = $request->guest_id;
        $cart = $this->cartRepository->getGuestCart($guestId);
        $cartCount = count($cart);
        if($cartCount == 0) {
            return response()->json(['status' => false, 'message' => 'There are no items in your cart.'], 201);
        }
        $currency = $cart[0]->product->currency->code;
        $subTotal = 0;
        foreach($cart as $row){ 
            // check is product valid
            if($row->product->status != 1 || $row->product->current_stock < $row->quantity) {
                return response()->json(['status' => false, 'message' => 'One or more products in your cart are out of stock.'], 201);
            }
            else {
                $currentStock = $row->product->current_stock;
                $newStock = $currentStock - $row->quantity;
                $newStockData[] = array(
                    'id' => $row->product->id,
                    'current_stock' => $newStock
                );
            }

            $products[] = array(
                'name' => $row->product->name,
                'quantity' => $row->quantity,
            );

            $orderItemsData[] = array(
                'guest_user_id' => $guestId,
                'order_number' => rand(9999, 999999),
                'order_id' => '',
                'product_id' => $row->product->id,
                'unit_price' => $row->product->unit_price,
                'quantity' => $row->quantity,
                'created_at' => NOW(),
                'updated_at' => NOW(),
            );
            $subTotal += ($row->product->unit_price * $row->quantity);
        }

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $orderNumber = rand(9999, 999999);
        $orderData = array(
            'order_number' => $orderNumber,
            'guest_user_id' => $guestId,
            'delivery_date' => $request->delivery_date,
            'delivery_type' => $request->delivery_type,
            'payment_type' => 'cod',
            'sub_total' => $subTotal,
            'delivery_charge' => 0,
            'total_amount' => $total,
            'currency' => $currency,
        );

        $shippingAddressData = array(
            'order_id' => '',
            'name' => $request->shipping_name,
            'email' => $request->shipping_email,
            'phone_code' => $request->shipping_phone_code,
            'phone' => $request->shipping_phone,
            'country' => $request->shipping_country,
            'state' => $request->shipping_state,
            'pincode' => $request->shipping_pincode,
            'street' => $request->shipping_street,
            'area' => $request->shipping_area,
            'block' => $request->shipping_block,
            'building' => $request->shipping_building,
            'type' => $request->shipping_type,
            'gender' => $request->shipping_gender,
        );

        // reduce stock
        foreach($newStockData as $row) {
            $where = array('id' => $row['id']);
            $set = array('current_stock' => $row['current_stock']);
            $updateStock = $this->storeRepository->updateProduct($where, $set);
        }

        $order = $this->orderRepository->create($orderData);
        if($order) {
            // update order number with unique id
            $orderNumber = $order->id . '_' . Str::random(6);
            $where = array('id' => $order->id);
            $set = array('order_number' => $orderNumber);
            $updateOrder = $this->orderRepository->updateOrder($where, $set);
            // insert order items
            foreach($orderItemsData as $key => $value) {
                $orderItemsData[$key]['order_id'] = $order->id;
                $orderNumberItem = $order->id . '_tm_' . Str::random(6);
                $orderItemsData[$key]['order_number'] = $orderNumberItem;
            }
            $orderItemsInsert = $this->orderRepository->addOrderItems($orderItemsData);
            // insert shipping address
            $shippingAddressData['order_id'] = $order->id;
            $orderAddressInsert = $this->orderRepository->addShippingAddress($shippingAddressData);
            // clear cart
            $clearCart = $this->cartRepository->clearCartGuest($guestId);
            $response = array(
                'order_id' => $orderNumber,
                'total_amount' => $total,
                'currency' => $currency,
                'ordered_on' => $order->created_at,
                'products' => $products,
            );

            // Generate payment url
            $paymentRequest = $this->paymentRepository->generatePaymentUrl($response);

            return response()->json(['status' => true, 'message' => 'Order placed successfully.', 'data' => $response, 'payment_url' => $paymentRequest]);
        }
        else {
            // update stock back
            $cart = $this->cartRepository->getGuestCart($guestId);
            foreach($cart as $row){ 
                $currentStock = $row->product->current_stock;
                $newStock = $currentStock + $row->quantity;
                $where = array('id' => $row->product->id);
                $set = array('current_stock' => $newStock);
                $updateStock = $this->storeRepository->updateProduct($where, $set);
            }

            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function myOrders()
    {
        $orders = $this->orderRepository->myOrders();
        $orderResponse = [];
        foreach($orders as $order) {
            foreach($order->item as $item) {
                $orderResponse[] = array(
                    'id' => $item->idEnc,
                    'order_id' => Crypt::encryptString($order->id),
                    'product_id' => Crypt::encryptString($item->product->id),
                    'product_name' => $item->product->name,
                    'thumbnail_img' => Storage::disk('public')->url($item->product->thumbnail_img),
                    'amount' => $item->unit_price,
                    'currency' => $order->currency,
                    'quantity' => $item->quantity,
                    'status' => $order->status,
                    'ordered_on' => $item->created_at,
                );
            } 
        }
        return response()->json($orderResponse);
    }

    public function orderItemDetail(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $itemId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }
        $orders = $this->orderRepository->getOrderItemDetail($itemId);
        foreach($orders as $item) {
            $shippingAddress = array(
                'name' => $item->order->shippingAddress->name,
                'email' => ($item->order->shippingAddress->email) ? $item->order->shippingAddress->email : '',
                'phone_code' => $item->order->shippingAddress->phone_code,
                'phone' => $item->order->shippingAddress->phone,
                'country' => $item->order->shippingAddress->country,
                'state' => ($item->order->shippingAddress->state) ? $item->order->shippingAddress->state : '',
                'pincode' => $item->order->shippingAddress->pincode,
                'street' => ($item->order->shippingAddress->street) ? $item->order->shippingAddress->street : '',
                'area' => ($item->order->shippingAddress->area) ? $item->order->shippingAddress->area : '',
                'block' => ($item->order->shippingAddress->block) ? $item->order->shippingAddress->block : '',
                'building' => ($item->order->shippingAddress->building) ? $item->order->shippingAddress->building : '',
                'type' => $item->order->shippingAddress->type,
            );
            $orderResponse = array(
                'id' => $item->idEnc,
                'order_id' => Crypt::encryptString($item->order->id),
                'product_id' => Crypt::encryptString($item->product->id),
                'product_name' => $item->product->name,
                'thumbnail_img' => Storage::disk('public')->url($item->product->thumbnail_img),
                'amount' => $item->unit_price,
                'currency' => $item->order->currency,
                'quantity' => $item->quantity,
                'status' => $item->order->status,
                'ordered_on' => $item->created_at,
                'order_number' => $item->id,
                'payment_method' => $item->order->payment_type,
                'shipping_address' => $shippingAddress
            );
        } 
        return response()->json($orderResponse);
    }

    public function createSingleProduct(Request $request)
    {
        $rules = [
            'guest_id' => 'required',
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'product_id' => 'required',
            'quantity' => 'required',
            'shipping_name' => 'required|string',
            'shipping_email' => 'email',
            'shipping_phone_code' => 'required',
            'shipping_phone' => 'required',
            'shipping_country' => 'required|string',
            'shipping_state' => 'nullable|string',
            'shipping_pincode' => 'required',
            'shipping_street' => 'nullable|string',
            'shipping_area' => 'nullable|string',
            'shipping_block' => 'nullable|string',
            'shipping_building' => 'nullable|string',
            'shipping_type' => 'in:home,work,other',
            'shipping_gender' => 'in:male,female,other',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }
        
        $guestId = $request->guest_id;
        $quantity = $request->quantity;
        $product = $this->storeRepository->getProductDetail($productId);
        // check is product valid
        if($product == null || $product->status != 1 || $product->current_stock < $quantity) {
            return response()->json(['status' => false, 'message' => 'The product is out of stock or no longer available.'], 201);
        }
        else {
            $currentStock = $product->current_stock;
            $newStock = $currentStock - $quantity;
            $newStockData = array(
                'id' => $product->id,
                'current_stock' => $newStock
            );
            // reduce stock
            $where = array('id' => $newStockData['id']);
            $set = array('current_stock' => $newStockData['current_stock']);
            $updateStock = $this->storeRepository->updateProduct($where, $set);
        }

        $products[] = array(
            'name' => $product->name,
            'quantity' => $quantity,
        );

        $orderItemsData = array(
            'order_number' => rand(9999, 999999),
            'guest_user_id' => $guestId,
            'order_id' => '',
            'product_id' => $product->id,
            'unit_price' => $product->unit_price,
            'quantity' => $quantity,
            'created_at' => NOW(),
            'updated_at' => NOW(),
        );
        $subTotal = ($product->unit_price * $quantity);

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $orderData = array(
            'order_number' => rand(9999, 999999),
            'guest_user_id' => $guestId,
            'delivery_date' => $request->delivery_date,
            'delivery_type' => $request->delivery_type,
            'payment_type' => 'cod',
            'sub_total' => $subTotal,
            'delivery_charge' => 0,
            'total_amount' => $total,
            'currency' => $product->currency->code,
        );

        $shippingAddressData = array(
            'order_id' => '',
            'name' => $request->shipping_name,
            'email' => $request->shipping_email,
            'phone_code' => $request->shipping_phone_code,
            'phone' => $request->shipping_phone,
            'country' => $request->shipping_country,
            'state' => $request->shipping_state,
            'pincode' => $request->shipping_pincode,
            'street' => $request->shipping_street,
            'area' => $request->shipping_area,
            'block' => $request->shipping_block,
            'building' => $request->shipping_building,
            'type' => $request->shipping_type,
            'gender' => $request->shipping_gender,
        );

        $order = $this->orderRepository->create($orderData);
        if($order) {
            // update order number with unique string
            $orderNumber = $order->id . '_' . Str::random(6);
            $where = array('id' => $order->id);
            $set = array('order_number' => $orderNumber);
            $updateOrder = $this->orderRepository->updateOrder($where, $set);
            // insert order items
            $orderItemsData['order_id'] = $order->id;
            $orderNumberItem = $order->id . '_tm_' . Str::random(6);
            $orderItemsData['order_number'] = $orderNumberItem;
            $orderItemsInsert = $this->orderRepository->addOrderItems($orderItemsData);
            // insert shipping address
            $shippingAddressData['order_id'] = $order->id;
            $orderAddressInsert = $this->orderRepository->addShippingAddress($shippingAddressData);
            $response = array(
                'order_id' => $orderNumber,
                'total_amount' => $total,
                'currency' => $product->currency,
                'ordered_on' => $order->created_at,
                'products' => $products,
            );

            // Generate payment url
            $paymentRequest = $this->paymentRepository->generatePaymentUrl($response);

            return response()->json(['status' => true, 'message' => 'Order placed successfully.', 'data' => $response, 'payment_url' => $paymentRequest]);
        }
        else {
            // update stock back
            $product = $this->storeRepository->getProductDetail($productId);
            $currentStock = $product->current_stock;
            $newStock = $currentStock + $quantity;
            $where = array('id' => $product->id);
            $set = array('current_stock' => $newStock);
            $updateStock = $this->storeRepository->updateProduct($where, $set);

            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getDeliveryChargeSingleProduct(Request $request)
    {
        $rules = [
            'guest_id' => 'required',
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'product_id' => 'required',
            'quantity' => 'required',
            'shipping_pincode' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }
        
        $quantity = $request->quantity;
        $product = $this->storeRepository->getProductDetail($productId);
        // check is product valid
        if($product == null || $product->status != 1 || $product->current_stock < $quantity) {
            return response()->json(['status' => false, 'message' => 'The product is out of stock or no longer available.'], 201);
        }
        $subTotal = ($product->unit_price * $quantity);
        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;
        $response = array(
            'sub_total' => $subTotal,
            'delivery_charge' => $deliveryCharge,
            'total' => $total,
            'currency' => $product->currency->code,
        );
        return response()->json(['status' => true, 'data' => $response]);
    }

    


}
