<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\OrderRepositoryInterface;
use App\Interfaces\CartRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\PaymentRepositoryInterface;

class OrderController extends Controller
{
    private OrderRepositoryInterface $orderRepository;
    private CartRepositoryInterface $cartRepository;
    private StoreRepositoryInterface $storeRepository;
    private UserRepositoryInterface $userRepository;
    private PaymentRepositoryInterface $paymentRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, CartRepositoryInterface $cartRepository, StoreRepositoryInterface $storeRepository, UserRepositoryInterface $userRepository, PaymentRepositoryInterface $paymentRepository) 
    {
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
        $this->storeRepository = $storeRepository;
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepository;
    }
    
    public function getDeliveryCharge(Request $request)
    {
        $rules = [
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_address_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $addressId = Crypt::decryptString($request->shipping_address_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }
        
        $userId = Auth::id();
        $cart = $this->cartRepository->getCart($userId);
        $cartCount = count($cart);
        if($cartCount == 0) {
            return response()->json(['status' => false, 'message' => 'There are no items in your cart.'], 201);
        }
        $currency = $cart[0]->product->currency->code;
        $subTotal = 0;
        foreach($cart as $row){ 
            // check is product valid
            if($row->product->status != 1 || $row->product->current_stock < $row->quantity) {
                return response()->json(['status' => false, 'message' => 'One or more products in your cart are out of stock.'], 201);
            }
            $subTotal += ($row->product->unit_price * $row->quantity);
        }

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $address = $this->userRepository->getAddressForUser($addressId);
        if(!$address) {
            return response()->json(['status' => false, 'message' => 'Invalid shipping address.'], 201);
        }
        $response = array(
            'sub_total' => $subTotal,
            'delivery_charge' => $deliveryCharge,
            'total' => $total,
            'currency' => $currency,
        );
        return response()->json(['status' => true, 'data' => $response]);
    }

    public function create(Request $request)
    {
        $rules = [
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_address_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $addressId = Crypt::decryptString($request->shipping_address_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }
        
        $userId = Auth::id();
        $cart = $this->cartRepository->getCart($userId);
        $cartCount = count($cart);
        if($cartCount == 0) {
            return response()->json(['status' => false, 'message' => 'There are no items in your cart.'], 201);
        }
        $currency = $cart[0]->product->currency->code;
        $subTotal = 0;
        foreach($cart as $row){ 
            // check is product valid
            if($row->product->status != 1 || $row->product->current_stock < $row->quantity) {
                return response()->json(['status' => false, 'message' => 'One or more products in your cart are out of stock.'], 201);
            }
            else {
                $currentStock = $row->product->current_stock;
                $newStock = $currentStock - $row->quantity;
                $newStockData[] = array(
                    'id' => $row->product->id,
                    'current_stock' => $newStock
                );
            }

            $products[] = array(
                'name' => $row->product->name,
                'quantity' => $row->quantity,
            );

            $orderItemsData[] = array(
                'order_number' => rand(9999, 999999),
                'user_id' => Auth::id(),
                'order_id' => '',
                'product_id' => $row->product->id,
                'unit_price' => $row->product->unit_price,
                'quantity' => $row->quantity,
                'created_at' => NOW(),
                'updated_at' => NOW(),
            );
            $subTotal += ($row->product->unit_price * $row->quantity);
        }

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $orderNumber = rand(9999, 999999);
        $orderData = array(
            'order_number' => $orderNumber,
            'user_id' => Auth::id(),
            'delivery_date' => $request->delivery_date,
            'delivery_type' => $request->delivery_type,
            'payment_type' => 'cod',
            'sub_total' => $subTotal,
            'delivery_charge' => 0,
            'total_amount' => $total,
            'currency' => $currency,
        );

        $address = $this->userRepository->getAddressForUser($addressId);
        if(!$address) {
            return response()->json(['status' => false, 'message' => 'Invalid shipping address.'], 201);
        }
        $shippingAddressData = array(
            'order_id' => '',
            'name' => $address->name,
            'email' => $address->email,
            'phone_code' => $address->phone_code,
            'phone' => $address->phone,
            'country' => $address->country,
            'state' => $address->state,
            'pincode' => $address->pincode,
            'street' => $address->street,
            'area' => $address->area,
            'block' => $address->block,
            'building' => $address->building,
            'type' => $address->type,
            'gender' => $address->gender,
        );

        // reduce stock
        foreach($newStockData as $row) {
            $where = array('id' => $row['id']);
            $set = array('current_stock' => $row['current_stock']);
            $updateStock = $this->storeRepository->updateProduct($where, $set);
        }

        $order = $this->orderRepository->create($orderData);
        if($order) {
            // update order number with unique string
            $orderNumber = $order->id . '_' . Str::random(6);
            $where = array('id' => $order->id);
            $set = array('order_number' => $orderNumber);
            $updateOrder = $this->orderRepository->updateOrder($where, $set);
            // insert order items
            foreach($orderItemsData as $key => $value) {
                $orderItemsData[$key]['order_id'] = $order->id;
                $orderNumberItem = $order->id . '_tm_' . Str::random(6);
                $orderItemsData[$key]['order_number'] = $orderNumberItem;
            }
            $orderItemsInsert = $this->orderRepository->addOrderItems($orderItemsData);
            // insert shipping address
            $shippingAddressData['order_id'] = $order->id;
            $orderAddressInsert = $this->orderRepository->addShippingAddress($shippingAddressData);
            // clear cart
            $clearCart = $this->cartRepository->clearCart($userId);
            $response = array(
                'order_id' => $orderNumber,
                'total_amount' => $total,
                'currency' => $currency,
                'ordered_on' => $order->created_at,
                'products' => $products,
            );

            // Generate payment url
            $paymentRequest = $this->paymentRepository->generatePaymentUrl($response);

            return response()->json(['status' => true, 'message' => 'Order placed successfully.', 'data' => $response, 'payment_url' => $paymentRequest]);
        }
        else {
            // update stock back
            $cart = $this->cartRepository->getCart($userId);
            foreach($cart as $row) { 
                $currentStock = $row->product->current_stock;
                $newStock = $currentStock + $row->quantity;
                $where = array('id' => $row->product->id);
                $set = array('current_stock' => $newStock);
                $updateStock = $this->storeRepository->updateProduct($where, $set);
            }

            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function myOrders()
    {
        $orders = $this->orderRepository->myOrders();
        $orderResponse = [];
        foreach($orders as $order) {
            foreach($order->item as $item) {
                $orderResponse[] = array(
                    'id' => $item->idEnc,
                    'order_id' => Crypt::encryptString($order->id),
                    'product_id' => Crypt::encryptString($item->product->id),
                    'product_name' => $item->product->name,
                    'thumbnail_img' => Storage::disk('public')->url($item->product->thumbnail_img),
                    'amount' => $item->unit_price,
                    'currency' => $order->currency,
                    'quantity' => $item->quantity,
                    'status' => $order->status,
                    'ordered_on' => $item->created_at,
                );
            } 
        }
        return response()->json($orderResponse);
    }

    public function orderItemDetail(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $itemId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }
        $orders = $this->orderRepository->getOrderItemDetail($itemId);
        foreach($orders as $item) {
            $shippingAddress = array(
                'name' => $item->order->shippingAddress->name,
                'email' => ($item->order->shippingAddress->email) ? $item->order->shippingAddress->email : '',
                'phone_code' => $item->order->shippingAddress->phone_code,
                'phone' => $item->order->shippingAddress->phone,
                'country' => $item->order->shippingAddress->country,
                'state' => ($item->order->shippingAddress->state) ? $item->order->shippingAddress->state : '',
                'pincode' => $item->order->shippingAddress->pincode,
                'street' => ($item->order->shippingAddress->street) ? $item->order->shippingAddress->street : '',
                'area' => ($item->order->shippingAddress->area) ? $item->order->shippingAddress->area : '',
                'block' => ($item->order->shippingAddress->block) ? $item->order->shippingAddress->block : '',
                'building' => ($item->order->shippingAddress->building) ? $item->order->shippingAddress->building : '',
                'type' => $item->order->shippingAddress->type,
            );
            $orderResponse = array(
                'id' => $item->idEnc,
                'order_id' => Crypt::encryptString($item->order->id),
                'product_id' => Crypt::encryptString($item->product->id),
                'product_name' => $item->product->name,
                'thumbnail_img' => Storage::disk('public')->url($item->product->thumbnail_img),
                'amount' => $item->unit_price,
                'currency' => $item->order->currency,
                'quantity' => $item->quantity,
                'status' => $item->order->status,
                'ordered_on' => $item->created_at,
                'order_number' => $item->id,
                'payment_method' => $item->order->payment_type,
                'shipping_address' => $shippingAddress
            );
        } 
        return response()->json($orderResponse);
    }

    public function createSingleProduct(Request $request)
    {
        $rules = [
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_address_id' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $addressId = Crypt::decryptString($request->shipping_address_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }
        
        $userId = Auth::id();
        $quantity = $request->quantity;
        $product = $this->storeRepository->getProductDetail($productId);
        // check is product valid
        if($product == null || $product->status != 1 || $product->current_stock < $quantity) {
            return response()->json(['status' => false, 'message' => 'The product is out of stock or no longer available.'], 201);
        }
        else {
            $currentStock = $product->current_stock;
            $newStock = $currentStock - $quantity;
            $newStockData = array(
                'id' => $product->id,
                'current_stock' => $newStock
            );
        }

        $products[] = array(
            'name' => $product->name,
            'quantity' => $quantity,
        );

        $orderItemsData = array(
            'order_number' => rand(9999, 999999),
            'user_id' => Auth::id(),
            'order_id' => '',
            'product_id' => $product->id,
            'unit_price' => $product->unit_price,
            'quantity' => $quantity,
            'created_at' => NOW(),
            'updated_at' => NOW(),
        );
        $subTotal = ($product->unit_price * $quantity);

        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $orderData = array(
            'order_number' => rand(9999, 999999),
            'user_id' => Auth::id(),
            'delivery_date' => $request->delivery_date,
            'delivery_type' => $request->delivery_type,
            'payment_type' => 'cod',
            'sub_total' => $subTotal,
            'delivery_charge' => 0,
            'total_amount' => $total,
            'currency' => $product->currency->code,
        );

        $address = $this->userRepository->getAddressForUser($addressId);
        if(!$address) {
            return response()->json(['status' => false, 'message' => 'Invalid shipping address.'], 201);
        }
        $shippingAddressData = array(
            'order_id' => '',
            'name' => $address->name,
            'email' => $address->email,
            'phone_code' => $address->phone_code,
            'phone' => $address->phone,
            'country' => $address->country,
            'state' => $address->state,
            'pincode' => $address->pincode,
            'street' => $address->street,
            'area' => $address->area,
            'block' => $address->block,
            'building' => $address->building,
            'type' => $address->type,
            'gender' => $address->gender,
        );

        // reduce stock
        $where = array('id' => $newStockData['id']);
        $set = array('current_stock' => $newStockData['current_stock']);
        $updateStock = $this->storeRepository->updateProduct($where, $set);

        $order = $this->orderRepository->create($orderData);
        if($order) {
            // update order number with unique string
            $orderNumber = $order->id . '_' . Str::random(6);
            $where = array('id' => $order->id);
            $set = array('order_number' => $orderNumber);
            $updateOrder = $this->orderRepository->updateOrder($where, $set);
            // insert order items
            $orderItemsData['order_id'] = $order->id;
            $orderNumberItem = $order->id . '_tm_' . Str::random(6);
            $orderItemsData['order_number'] = $orderNumberItem;
            $orderItemsInsert = $this->orderRepository->addOrderItems($orderItemsData);
            // insert shipping address
            $shippingAddressData['order_id'] = $order->id;
            $orderAddressInsert = $this->orderRepository->addShippingAddress($shippingAddressData);
            $response = array(
                'order_id' => $orderNumber,
                'total_amount' => $total,
                'currency' => $product->currency,
                'ordered_on' => $order->created_at,
                'products' => $products,
            );

            // Generate payment url
            $paymentRequest = $this->paymentRepository->generatePaymentUrl($response);

            return response()->json(['status' => true, 'message' => 'Order placed successfully.', 'data' => $response, 'payment_url' => $paymentRequest]);
        }
        else {
            // update stock back
            $product = $this->storeRepository->getProductDetail($productId);
            $currentStock = $product->current_stock;
            $newStock = $currentStock + $quantity;
            $where = array('id' => $product->id);
            $set = array('current_stock' => $newStock);
            $updateStock = $this->storeRepository->updateProduct($where, $set);

            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getDeliveryChargeSingleProduct(Request $request)
    {
        $rules = [
            'delivery_date' => 'required|date_format:Y-m-d',
            'delivery_type' => 'required|in:normal,express',
            'shipping_address_id' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $addressId = Crypt::decryptString($request->shipping_address_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid address id'];
            return response()->json($message, 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }
        
        $quantity = $request->quantity;
        $product = $this->storeRepository->getProductDetail($productId);
        // check is product valid
        if($product->status != 1 || $product->current_stock < $quantity) {
            return response()->json(['status' => false, 'message' => 'The product is out of stock.'], 201);
        }

        $subTotal = ($product->unit_price * $quantity);
        $deliveryCharge = 0;
        $total = $subTotal + $deliveryCharge;

        $address = $this->userRepository->getAddressForUser($addressId);
        if(!$address) {
            return response()->json(['status' => false, 'message' => 'Invalid shipping address.'], 201);
        }

        $response = array(
            'sub_total' => $subTotal,
            'delivery_charge' => $deliveryCharge,
            'total' => $total,
            'currency' => $product->currency->code,
        );
        return response()->json(['status' => true, 'data' => $response]);
    }

    public function transactionDetail(Request $request)
    {
        $rules = [
            'order_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $where = array('order_number' => $request->order_id);
        $orders = $this->orderRepository->getByCondition($where);
        $products = [];
        foreach($orders as $row) {
            if(isset($row->payment->knet_payment_id)) {
                foreach($row->item as $item) {
                    $products[] = array(
                        'name' => $item->product->name,
                        'quantity' => $item->quantity,
                    );
                }
                $dataOrder = array(
                    'order_id' => $row->order_number,
                    'payment_status' => ($row->payment_status == 1) ? true : false,
                    'total_amount' => $row->total_amount,
                    'currency' => $row->currency,
                    'transaction_date' => $row->created_at,
                    'knet_payment_id' => $row->payment->knet_payment_id,
                    'products' => $products,
                );
            }
            
        }
        if(isset($dataOrder)) {
            return response()->json($dataOrder);
        }
        else {
            return response()->json(['status' => false, 'message' => 'No data found']);
        }
    }

    


}
