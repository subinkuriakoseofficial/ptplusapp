<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Interfaces\UserRepositoryInterface;
use \Validator;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository) 
    {
        $this->userRepository = $userRepository;
    }
    
    public function createUser(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
            'dob' => 'date_format:Y-m-d',
            'password' => ['required', 'confirmed', Password::min(6)->letters()->mixedCase()->numbers()->symbols()->uncompromised()],
        ];
        $messages = [
            'dob' => 'The :attribute must be in YYYY-MM-DD format.',
            'password.confirmed' => 'The :attribute and password confirmation does not match.',
            'password' => 'The :attribute must contain minimum of six characters with at least one uppercase one lowercase letter at least one number and at least one symbol.',
        ];
        $attributes = [
            'dob' => 'date of birth',
        ];
        $validator = Validator::make($request->all(), $rules, $messages, $attributes);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'dob' => $request->dob,
            'password' => Hash::make($request->password),
            'user_type' => 'customer',
            'email_verified_at' => NOW(),
        );
        $user = $this->userRepository->createUser($data);
        if($user) {
            return response()->json(['status' => true, 'message' => 'Registration completed successfully.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.']);
        }
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        // Sanctum
        $user = $this->userRepository->getByEmail($username);
 
        if (! $user || ! Hash::check($password, $user->password)) {
            return response()->json(['status' => false, 'message' => 'Invalid credentials'], 201);
        }
        $deviceName = 'ptplus user ' . rand(1, 100); 
        $token = $user->createToken($deviceName)->plainTextToken;
        return response()->json(['status' => true, 'token_type' => 'Bearer', 'access_token' => $token]);
    }



}
