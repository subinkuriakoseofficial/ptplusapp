<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

class StoreController extends Controller
{
    private StoreRepositoryInterface $storeRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(StoreRepositoryInterface $storeRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->storeRepository = $storeRepository;
    }
    
    public function getBanners()
    {
        $banners = $this->storeRepository->getBanners();
        $banners = $banners->map(function ($bnr) {
            return array(
                'id' => $bnr->idEnc,
                'title' => $bnr->title,
                'description' => $bnr->description,
                'image' => Storage::disk('public')->url($bnr->image),
                'related_product_id' => Crypt::encryptString($bnr->product_id),
            );
        });
        return response()->json($banners);
    }

    public function getCountries()
    {
        $countries = $this->storeRepository->getCountries();
        $countries = $countries->map(function ($row) {
            return array(
                'id' => $row->id,
                'name' => $row->shortName,
                'phone_code' => $row->country_code,
            );
        });
        return response()->json($countries);
    }

    public function getCategories()
    {
        $categories = $this->storeRepository->getParentCategories();
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => $cat->idEnc,
                'name' => $cat->name,
                // 'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img), (storage/categories/barbells-plates.jpg)
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getSubCategories($catId)
    {
        try {
            $catId = Crypt::decryptString($catId);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message);
        }

        $categories = $this->storeRepository->getSubCategories($catId);
        $categories = $categories->map(function ($cat) {
            return array(
                'id' => Crypt::encryptString($cat->id),
                'name' => $cat->name,
                'thumbnail_img' => Storage::disk('public')->url($cat->thumbnail_img),
            );
        });
        return response()->json($categories);
    }

    public function getBestSellers(Request $request)
    {
        $products = $this->storeRepository->getBestSellers();
        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $products = $products->map(function ($pro) use($wishlist) {
            return array(
                'id' => $pro->idEnc,
                'name' => $pro->name,
                'thumbnail_img' => Storage::disk('public')->url($pro->thumbnail_img),
                'price' => $pro->unit_price,
                'currency' => $pro->currency->code,
                'wishlisted' => (in_array($pro->id, $wishlist) ? true : false)
            );
        });
        return response()->json($products);
    }

    public function getBestSellersAll(Request $request)
    {
        $products = $this->storeRepository->getBestSellersAll();
        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $products = $products->map(function ($pro) use($wishlist) {
            return array(
                'id' => $pro->idEnc,
                'name' => $pro->name,
                'thumbnail_img' => Storage::disk('public')->url($pro->thumbnail_img),
                'price' => $pro->unit_price,
                'currency' => $pro->currency->code,
                'wishlisted' => (in_array($pro->id, $wishlist) ? true : false)
            );
        });
        return response()->json($products);
    }

    public function getNewArrivals(Request $request)
    {
        $products = $this->storeRepository->getNewArrivals();
        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $products = $products->map(function ($pro) use($wishlist) {
            return array(
                'id' => $pro->idEnc,
                'name' => $pro->name,
                'thumbnail_img' => Storage::disk('public')->url($pro->thumbnail_img),
                'price' => $pro->unit_price,
                'currency' => $pro->currency->code,
                'wishlisted' => (in_array($pro->id, $wishlist) ? true : false)
            );
        });
        return response()->json($products);
    }

    public function getNewArrivalsAll(Request $request)
    {
        $products = $this->storeRepository->getNewArrivalsAll();
        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $products = $products->map(function ($pro) use($wishlist) {
            return array(
                'id' => $pro->idEnc,
                'name' => $pro->name,
                'thumbnail_img' => Storage::disk('public')->url($pro->thumbnail_img),
                'price' => $pro->unit_price,
                'currency' => $pro->currency->code,
                'wishlisted' => (in_array($pro->id, $wishlist) ? true : false)
            );
        });
        return response()->json($products);
    }

    public function getProductsByCategory($catId, Request $request)
    {
        try {
            $catId = Crypt::decryptString($catId);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message);
        }

        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $products = $this->storeRepository->getProductsByCategory($catId);

        $response = [];
        foreach($products as $pro) {
            if($pro->product != null) {
                $response[] = array(
                    'id' => $pro->product->idEnc,
                    'name' => $pro->product->name,
                    'thumbnail_img' => Storage::disk('public')->url($pro->product->thumbnail_img),
                    'price' => $pro->product->unit_price,
                    'currency' => $pro->product->currency->code,
                    'wishlisted' => (in_array($pro->id, $wishlist) ? true : false)
                );
            }
        }
        return response()->json($response);
    }

    public function getProductDetail($id, Request $request)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message);
        }
        
        $product = $this->storeRepository->getProductDetail($id);
        if($product == null) {
            return response()->json(['status' => false, 'message' => 'Invalid product']);
        }
        // Gallery
        $gallery = [];
        if($product->gallery != null) {
            $gallery = $product->gallery->map(function ($gal) {
                return array(
                    'image' => Storage::disk('public')->url($gal->image)
                );
            });
        }

        // Related products
        if($product->category != null) {
            $productCategories = $product->category->map(function ($proCat) {
                return $proCat->category_id;
            });
            $relatedProducts = $this->storeRepository->getProductsByCategories($productCategories);
            $relatedProducts = $relatedProducts->map(function ($pro) use ($product) {
                return array(
                    'id' => $pro->product->idEnc,
                    'name' => $pro->product->name,
                    'thumbnail_img' => Storage::disk('public')->url($pro->product->thumbnail_img),
                    'price' => $pro->product->unit_price,
                    'currency' => $pro->product->currency->code,
                );
            })->reject($relatedProducts->map(function ($pro) use ($product) {
                return ($pro->product->id == $product->id);
            }));
        }
        
        // get user wishlist to set wishlist flag
        $wishlist = [];
        $token = $request->bearerToken();
        if ($token != null) {
            $token = $request->bearerToken();
            $user = $this->commonRepository->getUserByToken($token);
            $userId = $user->tokenable->id;
            $wishlist = $this->storeRepository->getwishlist($userId);
            foreach($wishlist as $row) {
                $wishlist = array(
                    $row->product_id
                );
            }
        }

        $product = array(
            'id' => $product->idEnc,
            'name' => $product->name,
            'thumbnail_img' => Storage::disk('public')->url($product->thumbnail_img),
            'price' => $product->unit_price,
            'currency' => $product->currency->code,
            'brand' => ($product->brand != null) ? $product->brand->name : '',
            'description' => $product->description,
            'stock' => $product->current_stock,
            'gallery' => $gallery,
            'related_products' => $relatedProducts,
            'wishlisted' => (in_array($product->id, $wishlist) ? true : false)
        );
        return response()->json($product);
    }

    public function addToWishlist(Request $request)
    {
        $rules = [
            'product_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $productId = Crypt::decryptString($request->product_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid product id'];
            return response()->json($message, 201);
        }

        // check product valid
        $validProduct = $this->storeRepository->isValidProduct($productId);
        if($validProduct != 1) {
            return response()->json(['status' => false, 'message' => 'Invalid product'], 201);
        }

        $data = array(
            'user_id' => Auth::id(),
            'product_id' => $productId,
            'type' => 'product'
        );

        // check if already exists
        $wishlistCount = $this->storeRepository->checkWishlist($data);
        if($wishlistCount > 0) {
            return response()->json(['status' => false, 'message' => 'Item already exist.'], 201);
        }

        $wishlist = $this->storeRepository->addToWishlist($data);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Item successfully added to the wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function deleteWishlist(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid id'];
            return response()->json($message, 201);
        }

        $wishlist = $this->storeRepository->deleteWishlist($id);
        if($wishlist) {
            return response()->json(['status' => true, 'message' => 'Item deleted from wishlist.']);
        }
        else {
            return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
        }
    }

    public function getWishlist()
    {
        $wishlist = $this->storeRepository->getWishlist(Auth::id());
        $wishlist = $wishlist->map(function ($row) {
            return array(
                'id' => $row->idEnc,
                'product_id' => Crypt::encryptString($row->product->id),
                'product_name' => $row->product->name,
                'thumbnail_img' => Storage::disk('public')->url($row->product->thumbnail_img),
            );
        });
        return response()->json($wishlist);
    }


}
