<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Interfaces\TrainerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\BookingRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;

class BookingController extends Controller
{
    private TrainerRepositoryInterface $trainerRepository;
    private CommonRepositoryInterface $commonRepository;
    private StoreRepositoryInterface $storeRepository;
    private BookingRepositoryInterface $bookingRepository;
    private UserRepositoryInterface $userRepository;

    public function __construct(TrainerRepositoryInterface $trainerRepository, CommonRepositoryInterface $commonRepository, StoreRepositoryInterface $storeRepository, BookingRepositoryInterface $bookingRepository, UserRepositoryInterface $userRepository) 
    {
        $this->trainerRepository = $trainerRepository;
        $this->commonRepository = $commonRepository;
        $this->storeRepository = $storeRepository;
        $this->bookingRepository = $bookingRepository;
        $this->userRepository = $userRepository;
    }

    public function create(Request $request)
    {
        $rules = [
            'appointment_date' => 'required|date_format:Y-m-d',
            'appointment_time' => 'required|string',
            'trainer_id' => 'required',
            'plan_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid trainer id'];
            return response()->json($message, 201);
        }

        try {
            $planId = Crypt::decryptString($request->plan_id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid plan id'];
            return response()->json($message, 201);
        }
        
        $trainer = $this->trainerRepository->getTrainer($trainerId);
        if($trainer == null) {
            return response()->json(['status' => false, 'message' => 'The trainer you are trying to book is not available.'], 201);
        }
        $categoryId = $trainer->trainerRegCategories[0]->category_id;
        $category = $this->commonRepository->getCategory($categoryId);

        $plan = $this->trainerRepository->getTrainerPlan($planId);
        if($plan == null) {
            return response()->json(['status' => false, 'message' => 'The plan you are trying to book is not available.'], 201);
        }

        $orderNumber = Str::random(6);
        $bookingData = array(
            'booking_number' => $orderNumber,
            'user_id' => Auth::id(),
            'trainer_id' => $trainerId,
            'plan_id' => $planId,
            'service_type' => 'trainer',
            'appointment_date' => $request->appointment_date,
            'appointment_time' => $request->appointment_time,
            'payment_type' => 'cod',
            'total_amount' => $plan->price,
            'currency' => $plan->currency->code,
        );

        $order = $this->bookingRepository->create($bookingData);
        if($order) {
            // update booking number with unique string
            $orderNumber = $order->id . '_' . $orderNumber;
            $where = array('id' => $order->id);
            $set = array('booking_number' => $orderNumber);
            $updateOrder = $this->bookingRepository->updateBooking($where, $set);
            
            $response = array(
                'order_id' => $orderNumber,
                'trainer_name' => $trainer->name,
                'trainer_category' => $category->name,
                'trainer_image' => ($trainer->profile_picture != null) ? Storage::disk('public')->url($trainer->profile_picture) : '',
                'trainer_address' => $trainer->trainerDetails->address,
                'appointment_date' => $request->appointment_date,
                'appointment_time' => $request->appointment_time,

            );
            return response()->json(['status' => true, 'message' => 'Booking completed successfully.', 'data' => $response]);
        }
        return response()->json(['status' => false, 'message' => 'An unknown error occurred.'], 201);
    }
    
    public function myBookings()
    {
        $bookings = $this->bookingRepository->myBookings();
        $bookings = $bookings->map(function($row){
            $categoryId = $row->trainer->trainerRegCategories[0]->category_id;
            $category = $this->commonRepository->getCategory($categoryId);
            return array(
                'id' => $row->idEnc,
                'trainer_name' => $row->trainer->name,
                'trainer_image' => ($row->trainer->profile_picture != null) ? Storage::disk('public')->url($row->trainer->profile_picture) : '',
                'trainer_category' => $category->name,
                'trainer_rating' => $row->trainer->trainerDetails->rating,
                'trainer_experience' => $row->trainer->trainerDetails->experience . ' Years experience',
                'plan_title' => $row->plan->title,
                'plan_sub_title' => $row->plan->sub_title,
                'plan_image' => ($row->plan->thumbnail_img != null) ? Storage::disk('public')->url($row->plan->thumbnail_img) : '',
                'plan_price' => $row->total_amount,
                'plan_currency' => $row->currency,
                'booked_on' => $row->created_at,
            );
        });

        return response()->json($bookings);
    }

    public function bookingDetail(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $bookingId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid booking id'];
            return response()->json($message, 201);
        }

        $booking = $this->bookingRepository->get($bookingId);
        $categoryId = $booking->trainer->trainerRegCategories[0]->category_id;
        $category = $this->commonRepository->getCategory($categoryId);
        $booking = array(
                    'id' => $booking->idEnc,
                    'trainer_name' => $booking->trainer->name,
                    'trainer_image' => ($booking->trainer->profile_picture != null) ? Storage::disk('public')->url($booking->trainer->profile_picture) : '',
                    'trainer_category' => $category->name,
                    'trainer_rating' => $booking->trainer->trainerDetails->rating,
                    'trainer_phone' => $booking->trainer->trainerDetails->phone,
                    'trainer_address' => $booking->trainer->trainerDetails->address,
                    'trainer_latitude' => $booking->trainer->trainerDetails->latitude,
                    'trainer_longitude' => $booking->trainer->trainerDetails->longitude,
                    'trainer_experience' => $booking->trainer->trainerDetails->experience . ' Years experience',
                    'plan_title' => $booking->plan->title,
                    'plan_sub_title' => $booking->plan->sub_title,
                    'plan_image' => ($booking->plan->thumbnail_img != null) ? Storage::disk('public')->url($booking->plan->thumbnail_img) : '',
                    'plan_price' => $booking->total_amount,
                    'plan_currency' => $booking->currency,
                    'booked_on' => $booking->created_at,
                    'booking_status' => $booking->status,
                );

        return response()->json($booking);
    }

    public function cancelBooking(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Validation error occurred.', 'errors' => $validator->errors()], 201);
        }

        try {
            $bookingId = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            $message = ['status' => false, 'message' => 'Invalid booking id'];
            return response()->json($message, 201);
        }

        $where = array(
                'id' => $bookingId,
                'user_id' => Auth::id(),
            );
        $set = array('status' => 'cancelled');
        $update = $this->bookingRepository->updateBooking($where, $set);
        if($update) {
            return response()->json(['status' => true, 'message' => 'Booking cancelled successfully']);
        }

        return response()->json(['status' => false, 'message' => 'Unable to cancel the booking'], 201);
    }

    


}
