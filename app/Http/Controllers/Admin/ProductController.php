<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\StoreRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

use App\Http\Requests\ProductAddRequest;
use App\Http\Requests\ProductUpdateRequest;

class ProductController extends Controller
{
    private StoreRepositoryInterface $storeRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(StoreRepositoryInterface $storeRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->storeRepository = $storeRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.product.index');
    }

    public function productListData(Request $request)
    {
        $products = $this->storeRepository->getProductsDataTable($request->all());
        $dataTableJSON = DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($product) {
                $thumbnail = $product->thumbnail_img;
                return view('admin.product.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($product){
                return $product->name;
            })
            ->addColumn('status', function ($product) use ($request) {
                $stock = $product->current_stock;
                return view('admin.product.status', compact('stock'));
            })
            ->addColumn('action', function ($product) use ($request) {
                $id = $product->idEnc;
                return view('admin.product.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->storeRepository->getCategories();
        $currencies = $this->commonRepository->getCurrencies();
        return view('admin.product.add', compact('categories', 'currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductAddRequest $request)
    {
        $uploadPath = 'products';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->name_en, '-');
        $dataProduct = array(
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'added_by' => 1,
            'thumbnail_img' => $thumbnail,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'unit_price' => $request->price,
            'currency_id' => $request->currency,
            'current_stock' => $request->stock,
            'status' => 1,
            'approved' => 1,
            'slug' => $slug,
        );

        $product = $this->storeRepository->addProduct($dataProduct);
        if($product) {
            $categories = $request->category;
            foreach($categories as $row) {
                $dataCategories[] = array(
                    'product_id' => $product->id,
                    'category_id' => $row,
                    'created_at' => now(),
                    'updated_at' => now(),
                );
            }
            if(isset($dataCategories)) {
                $createCategories = $this->storeRepository->addProductCategories($dataCategories);
            }

            if ($request->hasFile('images')) {
                foreach ($request->images as $image) {
                    $image = Storage::disk('public')->putFile($uploadPath, $image);
                    $dataGallery[] = array(
                        'product_id' => $product->id,
                        'image' => $image,
                        'created_at' => now(),
                        'updated_at' => now(),
                    );
                }
                if(isset($dataGallery)) {
                    $createGallery = $this->storeRepository->addProductGallery($dataGallery);
                }
            }

            return redirect()->route('admin_product_list')->with('messageSuccess', 'Product added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_product_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $product = $this->storeRepository->getProductDetail($id);
        $categoriesSelected = $product->category;
        $categoriesSelected = $categoriesSelected->map(function($category) {
            return $category->category_id;
        });
        $categoriesSelected = $categoriesSelected->toArray();
        $categories = $this->storeRepository->getCategories();
        $currencies = $this->commonRepository->getCurrencies();
        // dd($product->gallery);
        return view('admin.product.edit', compact('product', 'categories', 'currencies', 'categoriesSelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_product_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'products';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->name_en, '-');
        $dataProduct = array(
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'thumbnail_img' => $thumbnail,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'unit_price' => $request->price,
            'currency_id' => $request->currency,
            'current_stock' => $request->stock,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $product = $this->storeRepository->updateProduct($where, $dataProduct);

        if($product) {
            $where = array('product_id' => $id);
            $deleteCategories = $this->storeRepository->deleteProductCategories($where);
            $categories = $request->category;
            foreach($categories as $row) {
                $dataCategories[] = array(
                    'product_id' => $id,
                    'category_id' => $row,
                    'created_at' => now(),
                    'updated_at' => now(),
                );
            }
            if(isset($dataCategories)) {
                $createCategories = $this->storeRepository->addProductCategories($dataCategories);
            }

            if ($request->hasFile('images')) {
                foreach ($request->images as $image) {
                    $image = Storage::disk('public')->putFile($uploadPath, $image);
                    $dataGallery[] = array(
                        'product_id' => $id,
                        'image' => $image,
                        'created_at' => now(),
                        'updated_at' => now(),
                    );
                }
                if(isset($dataGallery)) {
                    $createGallery = $this->storeRepository->addProductGallery($dataGallery);
                }
            }

            return redirect()->route('admin_product_list')->with('messageSuccess', 'Product updated successfully');
        }

        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_product_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->storeRepository->deleteProduct($id);
        if($delete) {
            return redirect()->route('admin_product_list')->with('messageSuccess', 'Product deleted successfully');
        }

        return redirect()->route('admin_product_list')->with('messageError', 'An error occurred');
    }

    public function deleteGalleryImage($id, $productId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_product_edit', ['id' => $productId])->with('messageError', 'An error occurred, unable to delete the image');
        }

        $deletedGallery = $this->storeRepository->deleteGalleryImage($id);
        if($deletedGallery) {
            Storage::disk('public')->delete($deletedGallery->image);
            return redirect()->route('admin_product_edit', ['id' => $productId])->with('messageSuccess', 'Image deleted successfully');
        }

        return redirect()->route('admin_product_edit', ['id' => $productId])->with('messageError', 'An error occurred, unable to delete the image');
    }
}
