<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\CenterBannerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;

use App\Http\Requests\CenterBannerAddRequest;
use App\Http\Requests\CenterBannerUpdateRequest;

class CenterBannerController extends Controller
{
    private CenterBannerRepositoryInterface $centerBannerRepository;
    private CommonRepositoryInterface $commonRepository;
    private UserRepositoryInterface $userRepository;

    public function __construct(CenterBannerRepositoryInterface $centerBannerRepository, CommonRepositoryInterface $commonRepository, UserRepositoryInterface $userRepository) 
    {
        $this->centerBannerRepository = $centerBannerRepository;
        $this->commonRepository = $commonRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.centerbanner.index');
    }

    public function listData(Request $request)
    {
        $banners = $this->centerBannerRepository->getForDataTable($request->all());
        $dataTableJSON = DataTables::of($banners)
            ->addIndexColumn()
            ->addColumn('image', function ($banner) {
                $thumbnail = $banner->image;
                return view('admin.centerbanner.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($banner){
                return $banner->title;
            })
            ->addColumn('action', function ($banner) use ($request) {
                $id = $banner->idEnc;
                return view('admin.centerbanner.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centers = $this->userRepository->getByType('center');
        return view('admin.centerbanner.add', compact('centers'));
    }

    /**
     * Center a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CenterBannerAddRequest $request)
    {
        $uploadPath = 'banner';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'user_id' => $request->center,
            'image' => $thumbnail,
        );

        $banner = $this->centerBannerRepository->create($dataBanner);
        if($banner) {
            return redirect()->route('admin_banner_center_list')->with('messageSuccess', 'Banner added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $centers = $this->userRepository->getByType('center');

        $banner = $this->centerBannerRepository->find($id);
        return view('admin.centerbanner.edit', compact('banner', 'centers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CenterBannerUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_center_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'banner';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'user_id' => $request->center,
            'image' => $thumbnail,
        );

        $where = array('id' => $id);
        $centerbanner = $this->centerBannerRepository->update($where, $dataBanner);
        if($centerbanner) {
            return redirect()->route('admin_banner_center_list')->with('messageSuccess', 'Banner updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->centerBannerRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_banner_center_list')->with('messageSuccess', 'Banner deleted successfully');
        }

        return redirect()->route('admin_banner_center_list')->with('messageError', 'An error occurred');
    }
}
