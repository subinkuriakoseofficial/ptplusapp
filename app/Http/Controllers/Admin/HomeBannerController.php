<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\HomeBannerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

use App\Http\Requests\HomeBannerAddRequest;
use App\Http\Requests\HomeBannerUpdateRequest;

class HomeBannerController extends Controller
{
    private HomeBannerRepositoryInterface $homeBannerRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(HomeBannerRepositoryInterface $homeBannerRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->homeBannerRepository = $homeBannerRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.homebanner.index');
    }

    public function listData(Request $request)
    {
        $banners = $this->homeBannerRepository->getForDataTable($request->all());
        $dataTableJSON = DataTables::of($banners)
            ->addIndexColumn()
            ->addColumn('image', function ($banner) {
                $thumbnail = $banner->image;
                return view('admin.homebanner.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($banner){
                return $banner->title;
            })
            ->addColumn('action', function ($banner) use ($request) {
                $id = $banner->idEnc;
                return view('admin.homebanner.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.homebanner.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HomeBannerAddRequest $request)
    {
        $uploadPath = 'banner';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'image' => $thumbnail,
            'status' => 1,
        );

        $banner = $this->homeBannerRepository->create($dataBanner);
        if($banner) {
            return redirect()->route('admin_banner_home_list')->with('messageSuccess', 'Banner added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_home_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $banner = $this->homeBannerRepository->find($id);

        return view('admin.homebanner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomeBannerUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_home_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'banner';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'image' => $thumbnail,
            'status' => 1,
        );

        $where = array('id' => $id);
        $homebanner = $this->homeBannerRepository->update($where, $dataBanner);
        if($homebanner) {
            return redirect()->route('admin_banner_home_list')->with('messageSuccess', 'Banner updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_home_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->homeBannerRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_banner_home_list')->with('messageSuccess', 'Banner deleted successfully');
        }

        return redirect()->route('admin_banner_home_list')->with('messageError', 'An error occurred');
    }
}
