<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\TrainerRepositoryInterface;

use App\Http\Requests\ActivityAddRequest;
use App\Http\Requests\ActivityUpdateRequest;

class CustomerController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;
    private TrainerRepositoryInterface $trainerRepository;

    public function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository, TrainerRepositoryInterface $trainerRepository) 
    {
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
        $this->trainerRepository = $trainerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customer.index');
    }

    public function listData(Request $request)
    {
        $users = $this->userRepository->getUserDataTable('customer');
        $dataTableJSON = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('name', function ($user){
                return $user->name;
            })
            ->addColumn('email', function ($user){
                return $user->email;
            })
            ->addColumn('phone', function ($user){
                return $user->phone_code . $user->phone;
            })
            ->addColumn('registered_on', function ($user){
                return $user->created_at;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                return view('admin.customer.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityAddRequest $request)
    {
        $uploadPath = 'activities';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->title_en, '-');
        $dataActivity = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'thumbnail_img' => $thumbnail,
            'status' => 1,
            'slug' => $slug,
        );

        $plan = $this->userRepository->create($dataActivity);
        if($plan) {
            return redirect()->route('admin_user_customer_list')->with('messageSuccess', 'Activity added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_customer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $favProducts = $this->trainerRepository->getTrainerFavProducts($id);

        return view('admin.customer.view', compact('user', 'favProducts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_customer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $activity = $this->userRepository->find($id);

        return view('admin.customer.edit', compact('activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActivityUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_customer_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'activities';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->title_en, '-');
        $dataActivity = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'thumbnail_img' => $thumbnail,
            'status' => 1,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $activity = $this->userRepository->update($where, $dataActivity);
        if($activity) {
            return redirect()->route('admin_user_customer_list')->with('messageSuccess', 'Activity updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_customer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->userRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_user_customer_list')->with('messageSuccess', 'User deleted successfully');
        }

        return redirect()->route('admin_user_customer_list')->with('messageError', 'An error occurred');
    }

}
