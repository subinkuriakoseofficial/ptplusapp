<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\TrainingPlanRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

use App\Http\Requests\TrainingPlanAddRequest;
use App\Http\Requests\TrainingPlanUpdateRequest;

class TrainingPlanController extends Controller
{
    private TrainingPlanRepositoryInterface $trainingPlanRepository;
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(TrainingPlanRepositoryInterface $trainingPlanRepository, UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->trainingPlanRepository = $trainingPlanRepository;
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.trainingplan.index');
    }

    public function listData(Request $request)
    {
        $plans = $this->trainingPlanRepository->getForDataTable($request->all());
        $dataTableJSON = DataTables::of($plans)
            ->addIndexColumn()
            ->addColumn('image', function ($plan) {
                $thumbnail = $plan->thumbnail_img;
                return view('admin.trainingplan.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($plan){
                return ucfirst($plan->title);
            })
            ->addColumn('description', function ($plan){
                return Str::limit($plan->description, 100);
            })
            ->addColumn('price', function ($plan){
                return $plan->price . ' ' . $plan->currency->code;
            })
            ->addColumn('action', function ($plan) use ($request) {
                $id = $plan->idEnc;
                return view('admin.trainingplan.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($trainerId = '', $centerId = '')
    {
        if($trainerId != '') {
            try {
                $trainerId = Crypt::decryptString($trainerId);
            } catch (DecryptException $e) {
                return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
            }
            $whereParentPlan = array('parent_id' => NULL, 'user_id' => $trainerId);
        }
        if($centerId != '') {
            try {
                $centerId = Crypt::decryptString($centerId);
            } catch (DecryptException $e) {
                return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
            }
            $whereParentPlan = array('parent_id' => NULL, 'center_id' => $centerId);
        }
        if(!isset($whereParentPlan)) {
            $whereParentPlan = array('parent_id' => NULL);
        }
        $parentPlans = $this->trainingPlanRepository->getByCondition($whereParentPlan);
        $trainers = $this->userRepository->getByType('trainer');
        $centers = $this->userRepository->getByType('center');
        $currencies = $this->commonRepository->getCurrencies();

        return view('admin.trainingplan.add', compact('parentPlans', 'trainers', 'centers', 'currencies', 'trainerId', 'centerId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainingPlanAddRequest $request)
    {
        $uploadPath = 'plans';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->title_en, '-');
        $trainer = NULL;
        $center = NULL;
        if($request->behalf_of == 'trainer') {
            $trainer = $request->trainer;
        }
        else if($request->behalf_of == 'center') {
            $center = $request->center;
        }
        $dataPlan = array(
            'user_id' => $trainer,
            'center_id' => $center,
            'parent_id' => $request->parent_plan,
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'sub_title_en' => $request->sub_title_en,
            'sub_title_ar' => $request->sub_title_ar,
            'price' => $request->price,
            'currency_id' => $request->currency,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'num_of_trainers' => $request->num_of_trainers,
            'thumbnail_img' => $thumbnail,
            'status' => 0,
            'slug' => $slug,
        );

        $plan = $this->trainingPlanRepository->create($dataPlan);
        if($plan) {
            return redirect()->route('admin_training_plan_list')->with('messageSuccess', 'Plan added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $plan = $this->trainingPlanRepository->find($id);
        $where = array('parent_id' => NULL);
        $parentPlans = $this->trainingPlanRepository->getByCondition($where);
        $trainers = $this->userRepository->getByType('trainer');
        $centers = $this->userRepository->getByType('center');
        $currencies = $this->commonRepository->getCurrencies();

        return view('admin.trainingplan.edit', compact('plan', 'parentPlans', 'trainers', 'centers', 'currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TrainingPlanUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'plans';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->title_en, '-');
        $trainer = NULL;
        $center = NULL;
        if($request->behalf_of == 'trainer') {
            $trainer = $request->trainer;
        }
        else if($request->behalf_of == 'center') {
            $center = $request->center;
        }
        $dataPlan = array(
            'user_id' => $trainer,
            'center_id' => $center,
            'parent_id' => $request->parent_plan,
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'sub_title_en' => $request->sub_title_en,
            'sub_title_ar' => $request->sub_title_ar,
            'price' => $request->price,
            'currency_id' => $request->currency,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'num_of_trainers' => $request->num_of_trainers,
            'thumbnail_img' => $thumbnail,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $plan = $this->trainingPlanRepository->update($where, $dataPlan);
        if($plan) {
            return redirect()->route('admin_training_plan_list')->with('messageSuccess', 'Plan updated successfully');
        }

        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->trainingPlanRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_training_plan_list')->with('messageSuccess', 'Plan deleted successfully');
        }

        return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred');
    }
}
