<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\VendorRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;

use App\Http\Requests\VendorAddRequest;
use App\Http\Requests\VendorUpdateRequest;

class VendorController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;
    private VendorRepositoryInterface $vendorRepository;
    private StoreRepositoryInterface $storeRepository;

    public function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository, VendorRepositoryInterface $vendorRepository, StoreRepositoryInterface $storeRepository) 
    {
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
        $this->vendorRepository = $vendorRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.vendor.index');
    }

    public function listData(Request $request)
    {
        $users = $this->userRepository->getUserDataTable('vendor');
        $dataTableJSON = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('name', function ($user){
                return $user->name;
            })
            ->addColumn('email', function ($user){
                return $user->email;
            })
            ->addColumn('phone', function ($user){
                return $user->phone_code . $user->phone;
            })
            ->addColumn('registered_on', function ($user){
                return $user->created_at;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                return view('admin.vendor.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryCodes = $this->commonRepository->getCountries();
        return view('admin.vendor.add', compact('countryCodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorAddRequest $request)
    {
        $uploadPath = 'user';
        $thumbnail = '';
        if ($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile($uploadPath, $request->image);
        }

        $passwordEnc = Crypt::encryptString($request->password);
        $dataUser = array(
            'name' => $request->name,
            'email' => $request->email,
            'dob' => $request->dob,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'gender' => $request->gender,            
            'profile_picture' => $image,
            'password' => Hash::make($request->password),
            'pswd_non_hashed' => $passwordEnc,
            'user_type' => 'vendor',
        );

        $user = $this->userRepository->createUser($dataUser);
        if($user) {
            return redirect()->route('admin_user_vendor_list')->with('messageSuccess', 'User added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        try {
            $pswdNonHashed = Crypt::decryptString($user->pswd_non_hashed);
        } catch (DecryptException $e) {
            $pswdNonHashed = '';
        }

        return view('admin.vendor.view', compact('user', 'pswdNonHashed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $countryCodes = $this->commonRepository->getCountries();

        return view('admin.vendor.edit', compact('user', 'countryCodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VendorUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'activities';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->title_en, '-');
        $dataActivity = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'thumbnail_img' => $thumbnail,
            'status' => 1,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $activity = $this->userRepository->update($where, $dataActivity);
        if($activity) {
            return redirect()->route('admin_user_vendor_list')->with('messageSuccess', 'Activity updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->userRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_user_vendor_list')->with('messageSuccess', 'User deleted successfully');
        }

        return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred');
    }

    public function productlistData(Request $request)
    {
        $products = $this->vendorRepository->getVendorProductsTable($request->all());
        $dataTableJSON = DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($product) {
                $thumbnail = $product->thumbnail_img;
                return view('admin.vendor.products.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($product){
                return $product->name;
            })
            ->addColumn('status', function ($product) use ($request) {
                $stock = $product->current_stock;
                return view('admin.vendor.products.status', compact('stock'));
            })
            ->addColumn('action', function ($product) use ($request) {
                $id = $product->idEnc;
                $vendorId = $request->vendorId;
                return view('admin.vendor.products.action', compact('id', 'vendorId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function deleteProduct($id, $vendorId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->storeRepository->deleteProduct($id);
        if($delete) {
            return redirect()->route('admin_user_vendor_view', ['id' => $vendorId])->with('messageSuccess', 'Product deleted successfully');
        }

        return redirect()->route('admin_user_vendor_view', ['id' => $vendorId])->with('messageError', 'An error occurred');
    }

}
