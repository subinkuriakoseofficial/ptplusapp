<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\CenterRepositoryInterface;

use App\Http\Requests\CenterAddRequest;
use App\Http\Requests\CenterUpdateRequest;

class CenterController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;
    private CenterRepositoryInterface $centerRepository;

    public function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository, CenterRepositoryInterface $centerRepository) 
    {
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
        $this->centerRepository = $centerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.center.index');
    }

    public function listData(Request $request)
    {
        $users = $this->userRepository->getUserDataTable('center');
        $dataTableJSON = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('name', function ($user){
                return $user->name;
            })
            ->addColumn('email', function ($user){
                return $user->email;
            })
            ->addColumn('phone', function ($user){
                return $user->phone_code . $user->phone;
            })
            ->addColumn('registered_on', function ($user){
                return $user->created_at;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                return view('admin.center.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryCodes = $this->commonRepository->getCountries();
        return view('admin.center.add', compact('countryCodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CenterAddRequest $request)
    {
        $uploadPath = 'user';
        $thumbnail = '';
        if ($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile($uploadPath, $request->image);
        }

        $passwordEnc = Crypt::encryptString($request->password);
        $dataUser = array(
            'name' => $request->name,
            'email' => $request->email,
            'dob' => $request->dob,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'gender' => $request->gender,            
            'profile_picture' => $image,
            'password' => Hash::make($request->password),
            'pswd_non_hashed' => $passwordEnc,
            'user_type' => 'center',
        );

        $user = $this->userRepository->createUser($dataUser);
        if($user) {
            return redirect()->route('admin_user_center_list')->with('messageSuccess', 'User added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $categories = $user->centerRegCategories;
        $mainCategories = $user->centerRegMainCategories;
        $types = $user->centerRegTypes;
        $centerType = [];
        foreach($types as $row) {
            $centerType[] = $row->type->name;
        }
        $centerTypesStr = implode(', ', $centerType);

        $specialities = $user->centerRegSpecialities;
        try {
            $pswdNonHashed = Crypt::decryptString($user->pswd_non_hashed);
        } catch (DecryptException $e) {
            $pswdNonHashed = '';
        }

        return view('admin.center.view', compact('user', 'categories', 'mainCategories', 'types', 'specialities', 'centerTypesStr', 'pswdNonHashed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $countryCodes = $this->commonRepository->getCountries();

        return view('admin.center.edit', compact('user', 'countryCodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CenterUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'user';
        if ($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile($uploadPath, $request->image);
            if($image) {
                if($request->Ximage != NULL) {
                    Storage::disk('public')->delete($request->Ximage);
                }
            }
        }
        else {
            $image = $request->Ximage;
        }

        $slug = Str::slug($request->title_en, '-');
        $dataUser = array(
            'name' => $request->name,
            'dob' => $request->dob,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'gender' => $request->gender,            
            'profile_picture' => $image,
        );

        $where = array('id' => $id);
        $user = $this->userRepository->updateUser($where, $dataUser);
        if($user) {
            return redirect()->route('admin_user_center_list')->with('messageSuccess', 'User updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->userRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_user_center_list')->with('messageSuccess', 'User deleted successfully');
        }

        return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred');
    }

    public function bannerlistData(Request $request)
    {
        $banners = $this->centerRepository->getBannerDataTable($request->all());
        $dataTableJSON = DataTables::of($banners)
            ->addIndexColumn()
            ->addColumn('image', function ($banner) {
                $thumbnail = $banner->image;
                return view('admin.center.banners.thumbnail', compact('thumbnail'));
            })
            ->addColumn('action', function ($banner) use ($request) {
                $id = $banner->idEnc;
                $centerId = $request->centerId;
                return view('admin.center.banners.action', compact('id', 'centerId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function deleteBanner($id, $centerId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->centerRepository->deleteBanner($id);
        if($delete) {
            return redirect()->route('admin_user_center_view', ['id' => $centerId])->with('messageSuccess', 'Banner deleted successfully');
        }

        return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred');
    }

    public function planlistData(Request $request)
    {
        $plans = $this->centerRepository->getCenterPlansTable($request->all());
        $dataTableJSON = DataTables::of($plans)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($plan) {
                $thumbnail = $plan->thumbnail_img;
                return view('admin.center.plans.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($plan){
                return $plan->title;
            })
            ->addColumn('description', function ($plan){
                return Str::limit($plan->description, 100);
            })
            ->addColumn('price', function ($plan){
                return $plan->price . ' ' . $plan->currency->code;
            })
            ->addColumn('action', function ($plan) use ($request) {
                $id = $plan->idEnc;
                $centerId = $request->centerId;
                return view('admin.center.plans.action', compact('id', 'centerId'));
            })
            ->make();
        return $dataTableJSON;
    }

}
