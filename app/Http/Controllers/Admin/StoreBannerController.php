<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\StoreBannerRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;

use App\Http\Requests\StoreBannerAddRequest;
use App\Http\Requests\StoreBannerUpdateRequest;

class StoreBannerController extends Controller
{
    private StoreBannerRepositoryInterface $storeBannerRepository;
    private CommonRepositoryInterface $commonRepository;
    private StoreRepositoryInterface $storeRepository;

    public function __construct(StoreBannerRepositoryInterface $storeBannerRepository, CommonRepositoryInterface $commonRepository, StoreRepositoryInterface $storeRepository) 
    {
        $this->storeBannerRepository = $storeBannerRepository;
        $this->commonRepository = $commonRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.storebanner.index');
    }

    public function listData(Request $request)
    {
        $banners = $this->storeBannerRepository->getForDataTable($request->all());
        $dataTableJSON = DataTables::of($banners)
            ->addIndexColumn()
            ->addColumn('image', function ($banner) {
                $thumbnail = $banner->image;
                return view('admin.storebanner.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($banner){
                return $banner->title;
            })
            ->addColumn('action', function ($banner) use ($request) {
                $id = $banner->idEnc;
                return view('admin.storebanner.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = $this->storeRepository->getProducts();
        return view('admin.storebanner.add', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBannerAddRequest $request)
    {
        $uploadPath = 'banner';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'product_id' => $request->product,
            'image' => $thumbnail,
        );

        $banner = $this->storeBannerRepository->create($dataBanner);
        if($banner) {
            return redirect()->route('admin_banner_store_list')->with('messageSuccess', 'Banner added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_store_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $products = $this->storeRepository->getProducts();

        $banner = $this->storeBannerRepository->find($id);
        return view('admin.storebanner.edit', compact('banner', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBannerUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_store_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'banner';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $dataBanner = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'product_id' => $request->product,
            'image' => $thumbnail,
        );

        $where = array('id' => $id);
        $storebanner = $this->storeBannerRepository->update($where, $dataBanner);
        if($storebanner) {
            return redirect()->route('admin_banner_store_list')->with('messageSuccess', 'Banner updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_banner_store_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->storeBannerRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_banner_store_list')->with('messageSuccess', 'Banner deleted successfully');
        }

        return redirect()->route('admin_banner_store_list')->with('messageError', 'An error occurred');
    }
}
