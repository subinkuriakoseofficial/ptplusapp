<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\ActivityRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

use App\Http\Requests\ActivityAddRequest;
use App\Http\Requests\ActivityUpdateRequest;

class ActivityController extends Controller
{
    private ActivityRepositoryInterface $activityRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(ActivityRepositoryInterface $activityRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->activityRepository = $activityRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.activity.index');
    }

    public function listData(Request $request)
    {
        $activities = $this->activityRepository->getForDataTable($request->all());
        $dataTableJSON = DataTables::of($activities)
            ->addIndexColumn()
            ->addColumn('image', function ($activity) {
                $thumbnail = $activity->thumbnail_img;
                return view('admin.activity.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($activity){
                return $activity->title;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                return view('admin.activity.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.activity.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityAddRequest $request)
    {
        $uploadPath = 'activities';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->title_en, '-');
        $dataActivity = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'thumbnail_img' => $thumbnail,
            'status' => 1,
            'slug' => $slug,
        );

        $plan = $this->activityRepository->create($dataActivity);
        if($plan) {
            return redirect()->route('admin_activity_list')->with('messageSuccess', 'Activity added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_activity_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $activity = $this->activityRepository->find($id);

        return view('admin.activity.edit', compact('activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActivityUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_activity_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'activities';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->title_en, '-');
        $dataActivity = array(
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'thumbnail_img' => $thumbnail,
            'status' => 1,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $activity = $this->activityRepository->update($where, $dataActivity);
        if($activity) {
            return redirect()->route('admin_activity_list')->with('messageSuccess', 'Activity updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_activity_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->activityRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_activity_list')->with('messageSuccess', 'Activity deleted successfully');
        }

        return redirect()->route('admin_activity_list')->with('messageError', 'An error occurred');
    }
}
