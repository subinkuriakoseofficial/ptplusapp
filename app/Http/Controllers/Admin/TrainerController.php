<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;
use App\Interfaces\TrainerRepositoryInterface;
use App\Interfaces\StoreRepositoryInterface;

use App\Http\Requests\TrainerAddRequest;
use App\Http\Requests\TrainerUpdateRequest;
use App\Http\Requests\TrainerSpecialityAddRequest;

class TrainerController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;
    private TrainerRepositoryInterface $trainerRepository;
    private StoreRepositoryInterface $storeRepository;

    public function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository, TrainerRepositoryInterface $trainerRepository, StoreRepositoryInterface $storeRepository) 
    {
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
        $this->trainerRepository = $trainerRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.trainer.index');
    }

    public function listData(Request $request)
    {
        $users = $this->userRepository->getUserDataTable('trainer');
        $dataTableJSON = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('name', function ($user){
                return $user->name;
            })
            ->addColumn('email', function ($user){
                return $user->email;
            })
            ->addColumn('phone', function ($user){
                return $user->phone_code . $user->phone;
            })
            ->addColumn('gender', function ($user){
                return $user->gender;
            })
            ->addColumn('registered_on', function ($user){
                return $user->created_at;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                return view('admin.trainer.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryCodes = $this->commonRepository->getCountries();
        return view('admin.trainer.add', compact('countryCodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainerAddRequest $request)
    {
        $uploadPath = 'user';
        $thumbnail = '';
        if ($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile($uploadPath, $request->image);
        }

        $passwordEnc = Crypt::encryptString($request->password);
        $dataUser = array(
            'name' => $request->name,
            'email' => $request->email,
            'dob' => $request->dob,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'gender' => $request->gender,            
            'profile_picture' => $image,
            'password' => Hash::make($request->password),
            'pswd_non_hashed' => $passwordEnc,
            'user_type' => 'trainer',
        );

        $user = $this->userRepository->createUser($dataUser);
        if($user) {
            return redirect()->route('admin_user_trainer_list')->with('messageSuccess', 'User added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $categories = $user->trainerRegCategories;
        $mainCategories = $user->trainerRegMainCategories;
        $serviceTypes = $user->trainerRegServiceTypes;
        $specialities = $user->trainerRegSpecialities;

        try {
            $pswdNonHashed = Crypt::decryptString($user->pswd_non_hashed);
        } catch (DecryptException $e) {
            $pswdNonHashed = '';
        }

        return view('admin.trainer.view', compact('user', 'categories', 'mainCategories', 'serviceTypes', 'specialities', 'pswdNonHashed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($id);
        $countryCodes = $this->commonRepository->getCountries();

        return view('admin.trainer.edit', compact('user', 'countryCodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TrainerUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'user';
        if ($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile($uploadPath, $request->image);
            if($image) {
                if($request->Ximage != NULL) {
                    Storage::disk('public')->delete($request->Ximage);
                }
            }
        }
        else {
            $image = $request->Ximage;
        }

        $slug = Str::slug($request->title_en, '-');
        $dataUser = array(
            'name' => $request->name,
            'dob' => $request->dob,
            'phone_code' => $request->phone_code,
            'phone' => $request->phone,
            'country' => $request->country,
            'gender' => $request->gender,            
            'profile_picture' => $image,
        );

        $where = array('id' => $id);
        $user = $this->userRepository->updateUser($where, $dataUser);
        if($user) {
            return redirect()->route('admin_user_trainer_list')->with('messageSuccess', 'User updated successfully');
        }

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->userRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_user_trainer_list')->with('messageSuccess', 'User deleted successfully');
        }

        return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred');
    }

    public function favProductlistData(Request $request)
    {
        $favProducts = $this->trainerRepository->getTrainerFavProductsTable($request->all());
        $dataTableJSON = DataTables::of($favProducts)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($favProduct) {
                $thumbnail = $favProduct->product->thumbnail_img;
                return view('admin.trainer.favproducts.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($favProduct){
                return $favProduct->product->name;
            })
            ->addColumn('status', function ($favProduct) use ($request) {
                $stock = $favProduct->product->current_stock;
                return view('admin.trainer.favproducts.status', compact('stock'));
            })
            ->addColumn('action', function ($favProduct) use ($request) {
                $id = $favProduct->idEnc;
                $trainerId = $request->trainerId;
                return view('admin.trainer.favproducts.action', compact('id', 'trainerId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function removeFavProduct($id, $trainerId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->trainerRepository->deleteFavProduct($id);
        if($delete) {
            return redirect()->route('admin_user_trainer_view', ['id' => $trainerId])->with('messageSuccess', 'Favourite product removed successfully');
        }

        return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred');
    }

    public function activityListData(Request $request)
    {
        $activities = $this->trainerRepository->getTrainerActivitiesTable($request->all());
        $dataTableJSON = DataTables::of($activities)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($activity) {
                $thumbnail = $activity->activity->thumbnail_img;
                return view('admin.trainer.activities.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($activity){
                return $activity->activity->title;
            })
            ->addColumn('action', function ($activity) use ($request) {
                $id = $activity->idEnc;
                $trainerId = $request->trainerId;
                return view('admin.trainer.activities.action', compact('id', 'trainerId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function removeActivity($id, $trainerId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->trainerRepository->deleteActivity($id);
        if($delete) {
            return redirect()->route('admin_user_trainer_view', ['id' => $trainerId])->with('messageSuccess', 'Activity removed successfully');
        }

        return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred');
    }

    public function planlistData(Request $request)
    {
        $plans = $this->trainerRepository->getTrainerPlansTable($request->all());
        $dataTableJSON = DataTables::of($plans)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($plan) {
                $thumbnail = $plan->thumbnail_img;
                return view('admin.trainer.plans.thumbnail', compact('thumbnail'));
            })
            ->addColumn('title', function ($plan){
                return $plan->title;
            })
            ->addColumn('description', function ($plan){
                return Str::limit($plan->description, 100);
            })
            ->addColumn('price', function ($plan){
                return $plan->price . ' ' . $plan->currency->code;
            })
            ->addColumn('action', function ($plan) use ($request) {
                $id = $plan->idEnc;
                $trainerId = $request->trainerId;
                return view('admin.trainer.plans.action', compact('id', 'trainerId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function createfavProduct($trainerId)
    {
        try {
            $trainerId = Crypt::decryptString($trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($trainerId);
        return view('admin.trainer.favproducts.add', compact('user'));
    }

    public function createfavProductListdata(Request $request)
    {
        $favProducts = $this->trainerRepository->getTrainerFavProductsTable($request->all());

        $products = $this->storeRepository->getProductsDataTable($request->all());
        $dataTableJSON = DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('thumbnail', function ($product) {
                $thumbnail = $product->thumbnail_img;
                return view('admin.trainer.favproducts.manage.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($product){
                return $product->name;
            })
            ->addColumn('select', function ($product) use ($request, $favProducts) {
                $productId = $product->idEnc;
                $selected = $favProducts->contains('product_id', $product->id);
                return view('admin.trainer.favproducts.manage.action', compact('productId', 'selected'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function updateFavProduct(Request $request)
    {
        try {
            $trainerId = Crypt::decryptString($request->trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        try {
            $productId = Crypt::decryptString($request->productId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $data = array(
            'user_id' => $trainerId,
            'product_id' => $productId,
        );

        if($request->checked == 1) {
            $action = $this->trainerRepository->addFavProducts($data);
        }
        else {
            $action = $this->trainerRepository->deleteFavProductByCondition($data);
        }

        if($action) {
            return true;
        } else {
            return false;
        }        
    }

    public function createActivity($trainerId)
    {
        $data['trainerId'] = $trainerId;
        try {
            $trainerId = Crypt::decryptString($trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $regActivities = $this->trainerRepository->getTrainerActivitiesTable($data);
        $activities = $this->commonRepository->getActivities();
        $user = $this->userRepository->getUser($trainerId);
        return view('admin.trainer.activities.add', compact('regActivities', 'activities', 'user'));
    }

    public function updateRegActivity(Request $request)
    {
        try {
            $trainerId = Crypt::decryptString($request->trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        try {
            $activityId = Crypt::decryptString($request->activityId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $data = array(
            'user_id' => $trainerId,
            'activity_id' => $activityId,
        );

        if($request->checked == 1) {
            $action = $this->trainerRepository->addRegActivities($data);
        }
        else {
            $action = $this->trainerRepository->deleteRegActivity($data);
        }

        if($action) {
            return true;
        } else {
            return false;
        }        
    }

    public function createRegCategory($trainerId)
    {
        try {
            $trainerId = Crypt::decryptString($trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($trainerId);
        $regCategories = $user->trainerRegCategories;
        $regMainCategories = $user->trainerRegMainCategories;
        $categories = $this->commonRepository->getCategories();
        $mainCategories = $this->commonRepository->getMainCategories();

        return view('admin.trainer.categories.add', compact('regMainCategories', 'regCategories', 'user', 'categories', 'mainCategories'));
    }

    public function updateRegCategory(Request $request)
    {
        try {
            $trainerId = Crypt::decryptString($request->trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        try {
            $categoryId = Crypt::decryptString($request->categoryId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        try {
            $mainCategoryId = Crypt::decryptString($request->mainCategoryId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $dataCategory = array(
            'user_id' => $trainerId,
            'category_id' => $categoryId,
        );
        $dataMainCategory = array(
            'user_id' => $trainerId,
            'main_category_id' => $mainCategoryId,
        );

        if($request->checked == 1) {
            $action = $this->trainerRepository->addRegCategories($dataCategory);
            $action = $this->trainerRepository->addRegMainCategories($dataMainCategory);
        }
        else {
            $action = $this->trainerRepository->deleteRegCategory($dataCategory);
            $action = $this->trainerRepository->deleteRegMainCategory($dataMainCategory);
        }

        if($action) {
            return true;
        } else {
            return false;
        }        
    }

    public function specialityListData(Request $request)
    {
        $specialities = $this->trainerRepository->getTrainerSpecialitiesTable($request->all());
        $dataTableJSON = DataTables::of($specialities)
            ->addIndexColumn()
            ->addColumn('speciality', function ($speciality){
                return $speciality->speciality;
            })
            ->addColumn('action', function ($speciality) use ($request) {
                $id = $speciality->idEnc;
                $trainerId = $request->trainerId;
                return view('admin.trainer.specialities.action', compact('id', 'trainerId'));
            })
            ->make();
        return $dataTableJSON;
    }

    public function removeSpeciality($id, $trainerId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->trainerRepository->deleteSpeciality($id);
        if($delete) {
            return redirect()->route('admin_user_trainer_view', ['id' => $trainerId])->with('messageSuccess', 'Activity removed successfully');
        }

        return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred');
    }

    public function createSpeciality($trainerId)
    {
        try {
            $trainerId = Crypt::decryptString($trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $user = $this->userRepository->getUser($trainerId);
        return view('admin.trainer.specialities.add', compact('user'));
    }

    public function storeSpeciality(TrainerSpecialityAddRequest $request)
    {
        try {
            $trainerId = Crypt::decryptString($request->trainer_id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $dataSpeciality = array(
            'user_id' => $trainerId,
            'speciality_en' => $request->title_en,
            'speciality_ar' => $request->title_ar,
        );

        $user = $this->trainerRepository->addRegSpecialities($dataSpeciality);
        if($user) {
            return redirect()->route('admin_user_trainer_view', ['id' => $request->trainer_id])->with('messageSuccess', 'Speciality added successfully');
        }

        return back()->withInput();
    }

    public function createServiceType($trainerId)
    {
        $data['trainerId'] = $trainerId;
        try {
            $trainerId = Crypt::decryptString($trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $regServiceTypes = $this->trainerRepository->getTrainerServiceTypesTable($data);
        $serviceTypes = $this->commonRepository->getServiceTypes();
        $user = $this->userRepository->getUser($trainerId);
        return view('admin.trainer.servicetypes.add', compact('regServiceTypes', 'serviceTypes', 'user'));
    }

    public function updateRegServiceType(Request $request)
    {
        try {
            $trainerId = Crypt::decryptString($request->trainerId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        try {
            $serviceTypeId = Crypt::decryptString($request->serviceTypeId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $data = array(
            'user_id' => $trainerId,
            'service_type_id' => $serviceTypeId,
        );

        if($request->checked == 1) {
            $action = $this->trainerRepository->addRegServiceTypes($data);
        }
        else {
            $action = $this->trainerRepository->deleteRegServiceType($data);
        }

        if($action) {
            return true;
        } else {
            return false;
        }        
    }

}
