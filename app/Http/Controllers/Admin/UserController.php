<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

class UserController extends Controller
{

    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    public function listData()
    {
        $users = $this->userRepository->getUserDataTable();
        $dataTableJSON = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('user_type', function ($user){
                return ucfirst($user->user_type);
            })
            ->addColumn('name', function ($user){
                return $user->name;
            })
            ->addColumn('email', function ($user){
                return $user->email;
            })
            ->addColumn('phone', function ($user){
                return $user->phone_code . $user->phone;
            })
            ->addColumn('gender', function ($user){
                return $user->gender;
            })
            ->addColumn('registered_on', function ($user){
                return $user->created_at;
            })
            
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
