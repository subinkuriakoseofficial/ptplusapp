<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\CategoryRepositoryInterface;

use App\Http\Requests\CategoryAddRequest;
use App\Http\Requests\CategoryUpdateRequest;

class CategoryController extends Controller
{
    private CategoryRepositoryInterface $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository) 
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    public function listData(Request $request)
    {
        $categories = $this->categoryRepository->getCategoryDataTable($request->all());
        $dataTableJSON = DataTables::of($categories)
            ->addIndexColumn()
            ->addColumn('image', function ($category) {
                $thumbnail = $category->thumbnail_img;
                return view('admin.category.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($category){
                return $category->name;
            })
            ->addColumn('type', function ($category){
                return ucfirst($category->type);
            })
            ->addColumn('action', function ($category) use ($request) {
                $id = $category->idEnc;
                return view('admin.category.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainCategories = $this->categoryRepository->getMainCategories();
        $types = array('store', 'trainer', 'center', 'vendor');
        return view('admin.category.add', compact('mainCategories', 'types'));
    }

    public function ajaxListByMainCategory(Request $request)
    {
        $mainCategoryId = $request->mainCategoryId;
        $where = array('main_category_id' => $mainCategoryId);
        $categories = $this->categoryRepository->getByCondition($where);
        return (string)view('admin.category.ajax_categories_by_main_category', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryAddRequest $request)
    {
        $uploadPath = 'categories';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->name_en, '-');
        $parentId = ($request->category != '') ? $request->category : 0;
        $dataCategory = array(
            'parent_id' => $parentId,
            'main_category_id' => $request->main_category,
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'thumbnail_img' => $thumbnail,
            'type' => $request->type,
            'status' => 1,
            'slug' => $slug,
        );

        $category = $this->categoryRepository->create($dataCategory);
        if($category) {
            return redirect()->route('admin_category_list')->with('messageSuccess', 'Category added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $category = $this->categoryRepository->get($id);
        $mainCategories = $this->categoryRepository->getMainCategories();
        $types = array('store', 'trainer', 'center', 'vendor');
        $where = array('main_category_id' => $category->main_category_id);
        $parentCategories = $this->categoryRepository->getByCondition($where);

        return view('admin.category.edit', compact('category', 'mainCategories', 'types', 'parentCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'categories';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->name_en, '-');
        $parentId = ($request->category != '') ? $request->category : 0;
        $dataCategory = array(
            'parent_id' => $parentId,
            'main_category_id' => $request->main_category,
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'thumbnail_img' => $thumbnail,
            'type' => $request->type,
            'status' => 1,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $category = $this->categoryRepository->update($where, $dataCategory);
        if($category) {
            return redirect()->route('admin_category_list')->with('messageSuccess', 'Category updated successfully');
        }

        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->categoryRepository->delete($id);
        if($delete) {
            return redirect()->route('admin_category_list')->with('messageSuccess', 'Category deleted successfully');
        }

        return redirect()->route('admin_category_list')->with('messageError', 'An error occurred');
    }
}
