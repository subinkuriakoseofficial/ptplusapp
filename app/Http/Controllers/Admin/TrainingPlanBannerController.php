<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\TrainingPlanRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

use App\Http\Requests\TrainingPlanBannerAddRequest;
use App\Http\Requests\TrainingPlanUpdateRequest;

class TrainingPlanBannerController extends Controller
{
    private TrainingPlanRepositoryInterface $trainingPlanRepository;
    private UserRepositoryInterface $userRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(TrainingPlanRepositoryInterface $trainingPlanRepository, UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->trainingPlanRepository = $trainingPlanRepository;
        $this->userRepository = $userRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($planId)
    {
        try {
            $planId = Crypt::decryptString($planId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $plan = $this->trainingPlanRepository->find($planId);
        return view('admin.trainingplanbanner.index', compact('plan'));
    }

    public function listData(Request $request)
    {
        $banners = $this->trainingPlanRepository->getBannerForDataTable($request->all());
        $dataTableJSON = DataTables::of($banners)
            ->addIndexColumn()
            ->addColumn('image', function ($banner) {
                $thumbnail = $banner->thumbnail_img;
                return view('admin.trainingplanbanner.thumbnail', compact('thumbnail'));
            })
            ->addColumn('action', function ($banner) use ($request) {
                $id = $banner->idEnc;
                $planId = $request->planId;
                return view('admin.trainingplanbanner.action', compact('id', 'planId'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($planId)
    {
        try {
            $planId = Crypt::decryptString($planId);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $plan = $this->trainingPlanRepository->find($planId);

        return view('admin.trainingplanbanner.add', compact('plan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $planId = Crypt::decryptString($request->plan_id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $uploadPath = 'plans';
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $image = Storage::disk('public')->putFile($uploadPath, $image);
                $dataGallery[] = array(
                    'training_plan_id' => $planId,
                    'image' => $image,
                );
            }
            // dd($dataGallery);
            if(isset($dataGallery)) {
                $createGallery = $this->trainingPlanRepository->createBanner($dataGallery);
                if($createGallery) {
                    return redirect()->route('admin_training_plan_banner_list', ['planId' => $request->plan_id])->with('messageSuccess', 'Banner added successfully');
                }
            }
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $plan = $this->trainingPlanRepository->find($id);
        $where = array('parent_id' => NULL);
        $parentPlans = $this->trainingPlanRepository->getByCondition($where);
        $trainers = $this->userRepository->getByType('trainer');
        $centers = $this->userRepository->getByType('center');
        $currencies = $this->commonRepository->getCurrencies();

        return view('admin.trainingplanbanner.edit', compact('plan', 'parentPlans', 'trainers', 'centers', 'currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TrainingPlanUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_category_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'plans';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->title_en, '-');
        $trainer = NULL;
        $center = NULL;
        if($request->behalf_of == 'trainer') {
            $trainer = $request->trainer;
        }
        else if($request->behalf_of == 'center') {
            $center = $request->center;
        }
        $dataPlan = array(
            'user_id' => $trainer,
            'center_id' => $center,
            'parent_id' => $request->parent_plan,
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'sub_title_en' => $request->sub_title_en,
            'sub_title_ar' => $request->sub_title_ar,
            'price' => $request->price,
            'currency_id' => $request->currency,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'num_of_trainers' => $request->num_of_trainers,
            'thumbnail_img' => $thumbnail,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $plan = $this->trainingPlanRepository->update($where, $dataPlan);
        if($plan) {
            return redirect()->route('admin_training_plan_list')->with('messageSuccess', 'Plan updated successfully');
        }

        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $planId)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_banner_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->trainingPlanRepository->deleteBanner($id);
        if($delete) {
            return redirect()->route('admin_training_plan_banner_list', ['planId' => $planId])->with('messageSuccess', 'Banner deleted successfully');
        }

        return redirect()->route('admin_training_plan_banner_list')->with('messageError', 'An error occurred');
    }
}
