<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\OrderRepositoryInterface;
use App\Interfaces\CommonRepositoryInterface;

class OrderController extends Controller
{
    private OrderRepositoryInterface $orderRepository;
    private CommonRepositoryInterface $commonRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, CommonRepositoryInterface $commonRepository) 
    {
        $this->orderRepository = $orderRepository;
        $this->commonRepository = $commonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.order.index');
    }

    public function listData()
    {
        $order = $this->orderRepository->getOrderDataTable();
        $dataTableJSON = DataTables::of($order)
            ->addIndexColumn()
            ->addColumn('order_number', function ($order){
                return $order->order_number;
            })
            ->addColumn('user', function ($order){
                return ($order->user_id != NULL) ? $order->user->name : 'Guest User';
            })
            ->addColumn('delivery_date', function ($order){
                return $order->delivery_date;
            })
            ->addColumn('delivery_type', function ($order){
                return ucfirst($order->delivery_type);
            })
            ->addColumn('total_amount', function ($order){
                return $order->total_amount;
            })
            ->addColumn('ordered_on', function ($order){
                return $order->created_at;
            })
            ->addColumn('status', function ($order){
                return ucfirst($order->status);
            })
            ->addColumn('action', function ($order) {
                $id = $order->idEnc;
                return view('admin.order.action', compact('id'));
            })
            
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_order_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $order = $this->orderRepository->get($id);
        return view('admin.order.view', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
