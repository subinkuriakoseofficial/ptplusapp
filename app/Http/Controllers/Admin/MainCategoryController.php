<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Interfaces\CategoryRepositoryInterface;

use App\Http\Requests\MainCategoryAddRequest;
use App\Http\Requests\MainCategoryUpdateRequest;

class MainCategoryController extends Controller
{
    private CategoryRepositoryInterface $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository) 
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.maincategory.index');
    }

    public function listData(Request $request)
    {
        $products = $this->categoryRepository->getMainCategoryDataTable($request->all());
        $dataTableJSON = DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('image', function ($product) {
                $thumbnail = $product->thumbnail_img;
                return view('admin.maincategory.thumbnail', compact('thumbnail'));
            })
            ->addColumn('name', function ($product){
                return $product->name;
            })
            ->addColumn('action', function ($product) use ($request) {
                $id = $product->idEnc;
                return view('admin.maincategory.action', compact('id'));
            })
            ->make();
        return $dataTableJSON;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.maincategory.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainCategoryAddRequest $request)
    {
        $uploadPath = 'categories';
        $thumbnail = '';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
        }

        $slug = Str::slug($request->name_en, '-');
        $dataProduct = array(
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'thumbnail_img' => $thumbnail,
            'slug' => $slug,
        );

        $category = $this->categoryRepository->createMainCategory($dataProduct);
        if($category) {
            return redirect()->route('admin_maincategory_list')->with('messageSuccess', 'Category added successfully');
        }

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_maincategory_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        $category = $this->categoryRepository->findMainCategory($id);
        
        return view('admin.maincategory.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainCategoryUpdateRequest $request)
    {
        try {
            $id = Crypt::decryptString($request->id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_maincategory_list')->with('messageError', 'An error occurred, unable to update the data');
        }

        $uploadPath = 'categories';
        if ($request->hasFile('thumbnail')) {
            $thumbnail = Storage::disk('public')->putFile($uploadPath, $request->thumbnail);
            if($thumbnail) {
                Storage::disk('public')->delete($request->Xthumbnail);
            }
        }
        else {
            $thumbnail = $request->Xthumbnail;
        }

        $slug = Str::slug($request->name_en, '-');
        $dataCategory = array(
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'thumbnail_img' => $thumbnail,
            'slug' => $slug,
        );

        $where = array('id' => $id);
        $product = $this->categoryRepository->updateMainCategory($where, $dataCategory);

        if($product) {
            return redirect()->route('admin_maincategory_list')->with('messageSuccess', 'Category updated successfully');
        }

        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            return redirect()->route('admin_maincategory_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }

        $delete = $this->categoryRepository->deleteMainCategory($id);
        if($delete) {
            return redirect()->route('admin_maincategory_list')->with('messageSuccess', 'Category deleted successfully');
        }

        return redirect()->route('admin_maincategory_list')->with('messageError', 'An error occurred');
    }
}
