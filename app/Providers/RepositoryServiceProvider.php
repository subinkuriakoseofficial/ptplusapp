<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Interfaces\CommonRepositoryInterface;
use App\Repositories\CommonRepository;
use App\Interfaces\StoreRepositoryInterface;
use App\Repositories\StoreRepository;
use App\Interfaces\UserRepositoryInterface;
use App\Repositories\UserRepository;
use App\Interfaces\CartRepositoryInterface;
use App\Repositories\CartRepository;
use App\Interfaces\OrderRepositoryInterface;
use App\Repositories\OrderRepository;
use App\Interfaces\TrainerRepositoryInterface;
use App\Repositories\TrainerRepository;
use App\Interfaces\BookingRepositoryInterface;
use App\Repositories\BookingRepository;
use App\Interfaces\CenterRepositoryInterface;
use App\Repositories\CenterRepository;
use App\Interfaces\CategoryRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Interfaces\TrainingPlanRepositoryInterface;
use App\Repositories\TrainingPlanRepository;
use App\Interfaces\ActivityRepositoryInterface;
use App\Repositories\ActivityRepository;
use App\Interfaces\HomeBannerRepositoryInterface;
use App\Repositories\HomeBannerRepository;
use App\Interfaces\StoreBannerRepositoryInterface;
use App\Repositories\StoreBannerRepository;
use App\Interfaces\CenterBannerRepositoryInterface;
use App\Repositories\CenterBannerRepository;
use App\Interfaces\VendorRepositoryInterface;
use App\Repositories\VendorRepository;
use App\Interfaces\PaymentRepositoryInterface;
use App\Repositories\PaymentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CommonRepositoryInterface::class, CommonRepository::class);
        $this->app->bind(StoreRepositoryInterface::class, StoreRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CartRepositoryInterface::class, CartRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(TrainerRepositoryInterface::class, TrainerRepository::class);
        $this->app->bind(BookingRepositoryInterface::class, BookingRepository::class);
        $this->app->bind(CenterRepositoryInterface::class, CenterRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(TrainingPlanRepositoryInterface::class, TrainingPlanRepository::class);
        $this->app->bind(ActivityRepositoryInterface::class, ActivityRepository::class);
        $this->app->bind(HomeBannerRepositoryInterface::class, HomeBannerRepository::class);
        $this->app->bind(StoreBannerRepositoryInterface::class, StoreBannerRepository::class);
        $this->app->bind(CenterBannerRepositoryInterface::class, CenterBannerRepository::class);
        $this->app->bind(VendorRepositoryInterface::class, VendorRepository::class);
        $this->app->bind(PaymentRepositoryInterface::class, PaymentRepository::class);
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
