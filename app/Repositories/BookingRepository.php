<?php

namespace App\Repositories;

use App\Interfaces\BookingRepositoryInterface;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth;

class BookingRepository implements BookingRepositoryInterface 
{
    public function get($id)
    {
        return Booking::find($id);
    }

    public function create($data)
    {
        return Booking::create($data);
    }

    public function updateBooking($where, $set)
    {
        return Booking::where($where)->update($set);
    }

    public function myBookings()
    {
        return Booking::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
    }

    
}
