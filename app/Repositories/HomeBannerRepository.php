<?php

namespace App\Repositories;

use App\Interfaces\HomeBannerRepositoryInterface;
use App\Models\Banner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class HomeBannerRepository implements HomeBannerRepositoryInterface 
{
    public function create($data)
    {
        return Banner::create($data);
    }

    public function find($id) 
    {
        return Banner::find($id);
    }

    public function get() 
    {
        return Banner::get();
    }

    public function getByCondition($where) 
    {
        return Banner::where($where)->get();
    }

    public function update($where, $set)
    {
        return Banner::where($where)->update($set);
    }

    public function getForDataTable() 
    {
        return Banner::get();
    }

    public function delete($id)
    {
        return Banner::destroy($id);
    }
    
}
