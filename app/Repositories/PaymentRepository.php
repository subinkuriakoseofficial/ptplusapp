<?php

namespace App\Repositories;

use App\Interfaces\PaymentRepositoryInterface;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;

class PaymentRepository implements PaymentRepositoryInterface 
{
    public function generatePaymentUrl($data)
    {
        if (! function_exists( 'curl_version' )) { 
            $message = ['status' => false, 'message' => 'Unable to process the payment'];
            return response()->json($message);
        }
        $fields = array( 
            'merchant_id'=>'1201',
            'username' => 'test',
            'password'=>stripslashes('test'), 
            'api_key'=>'jtest123', // in sandbox request
            //'api_key' =>password_hash('API_KEY',PASSWORD_BCRYPT), //In production mode, please pass API_KEY with BCRYPT function
            'order_id'=>$data['order_id'], // MIN 30 characters with strong unique function (like hashing function with time) 
            'total_price'=> $data['total_amount'],
            'CurrencyCode'=>'KWD',//only works in production mode
            'CstFName'=>'Test Name',
            'CstEmail'=>'test@test.com', 
            'CstMobile'=>'12345678', 
            'success_url' => route('api_store_payment_success'),
            'error_url'=> route('api_store_payment_error'),
            'test_mode'=>1, // test mode enabled
            'whitelabled'=>true, // only accept in live credentials (it will not work in test) 
            'payment_gateway'=>'knet', // only works in production mode 
            'ProductName'=>json_encode(['computer','television']), 
            'ProductQty'=>json_encode([2,1]), 
            'ProductPrice'=>json_encode([150,1500]),
            'reference'=>$data['order_id'],
            // 'trnUdf' => $userIdEnc
        );

        $fields_string = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://api.upayments.com/test-payment"); curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $server_output = curl_exec($ch);
        curl_close($ch);
        $server_output = json_decode($server_output,true);

        return $server_output;
    }

    public function create($data)
    {
        return Payment::create($data);
    }

    
}
