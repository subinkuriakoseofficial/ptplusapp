<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface 
{
    public function getUser($id) 
    {
        return User::find($id);
    }

    public function createUser($data)
    {
        return User::create($data);
    }

    public function getByEmail($email) 
    {
        return User::where('email', $email)->first();
    }

    public function getByType($type) 
    {
        return User::where('user_type', $type)->get();
    }

    public function addAddress($data)
    {
        return Address::create($data);
    }

    public function getAddressAll()
    {
        return Address::where('user_id', Auth::id())->get();
    }

    public function getAddress($id) 
    {
        return Address::find($id);
    }

    public function getAddressForUser($id) 
    {
        return Address::where('user_id', Auth::id())->where('id', $id)->first();
    }

    public function updateUser($where, $set)
    {
        return User::where($where)->update($set);
    }

    public function getUserDataTable($type='') 
    {
        if($type != '') {
            return User::where('user_type', $type);
        }
        else {
            return User::where('user_type', '!=', 'admin');
        }        
    }

    public function delete($id)
    {
        return User::destroy($id);
    }


    
}
