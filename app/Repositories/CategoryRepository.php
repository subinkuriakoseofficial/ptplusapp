<?php

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Models\MainCategory;
use App\Models\Category;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;

class CategoryRepository implements CategoryRepositoryInterface 
{
    public function create($data)
    {
        return Category::create($data);
    }

    public function createMainCategory($data)
    {
        return MainCategory::create($data);
    }

    public function get($id) 
    {
        return Category::find($id);
    }

    public function getCategories() 
    {
        return Category::get();
    }

    public function getByCondition($where) 
    {
        return Category::where($where)->get();
    }

    public function getMainCategories() 
    {
        return MainCategory::get();
    }

    public function update($where, $set)
    {
        return Category::where($where)->update($set);
    }

    public function findMainCategory($id) 
    {
        return MainCategory::find($id);
    }

    public function updateMainCategory($where, $set)
    {
        return MainCategory::where($where)->update($set);
    }

    public function getMainCategoryDataTable() 
    {
        return MainCategory::get();
    }

    public function getCategoryDataTable() 
    {
        return Category::get();
    }

    public function delete($id)
    {
        return Category::destroy($id);
    }

    public function deleteMainCategory($id)
    {
        return MainCategory::destroy($id);
    }


    
}
