<?php

namespace App\Repositories;

use App\Interfaces\TrainerRepositoryInterface;
use App\Models\Category;
use App\Models\User;
use App\Models\ServiceType;
use App\Models\TrainerFavProduct;
use App\Models\TrainerGallery;
use App\Models\TrainerRegMainCategory;
use App\Models\TrainerRegCategory;
use App\Models\TrainerRegSpeciality;
use App\Models\TrainingPlan;
use App\Models\Wishlist;
use App\Models\Review;
use App\Models\TrainerAvailableTimeSlots;
use App\Models\TrainerRegActivity;
use App\Models\TrainerRegServiceType;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TrainerRepository implements TrainerRepositoryInterface 
{
    public function getTrainers($limit = '')
    {
        $query = User::where('user_type', 'trainer')->with('trainerRegCategories');
        if($limit != '') {
            $query->limit($limit)->get();
        }
        return $query->get();
    }

    public function getParentCategories() 
    {
        return Category::where('type', 'trainer')->where('parent_id', 0)->get();
    }

    public function getSubCategories($catId) 
    {
        return Category::where('type', 'trainer')->where('parent_id', $catId)->get();
    }

    public function getAllCategories() 
    {
        return Category::where('type', 'trainer')->get();
    }

    public function getFilterServiceTypes() 
    {
        return ServiceType::get();
    }

    public function getTrainersByFilter($serviceTypes = [], $categories = [], $activities = [], $availableStatus = '', $experience = [])
    {
        $query = User::where('user_type', 'trainer');
        if($serviceTypes != []) {
            $query->whereHas('trainerRegServiceTypes', function($queryRow) use ($serviceTypes) {
                foreach($serviceTypes as $type) {
                    $queryRow->where('service_type_id', $type);
                }
            });
        }
        if($categories != []) {
            $query->whereHas('trainerRegCategories', function($queryRow) use ($categories) {
                $queryRow->whereIn('category_id', $categories);
            });
        }
        if($activities != []) {
            $query->whereHas('trainerRegActivities', function($queryRow) use ($activities) {
                $queryRow->whereIn('activity_id', $activities);
            });
        }
        if($availableStatus != '') {
            $query->whereHas('trainerDetails', function($queryRow)  use ($availableStatus) {
                $queryRow->where('status', $availableStatus);
            });
        }
        if($experience != []) {
            $query->whereHas('trainerDetails', function($queryRow)  use ($experience) {
                $queryRow->whereBetween('experience', $experience);
            });
        }
        return $query->get();
    }

    public function getTrainer($id)
    {
        return User::where('user_type', 'trainer')->where('id', $id)->first();
    }

    public function getTrainerMainCategories($trainerId)
    {
        return TrainerRegMainCategory::where('user_id', $trainerId)->get();
    }

    public function getTrainerSpecialities($trainerId)
    {
        return TrainerRegSpeciality::where('user_id', $trainerId)->get();
    }

    public function getTrainerGallery($trainerId, $limit = '')
    {
        $query = TrainerGallery::where('user_id', $trainerId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getTrainerPlans($trainerId, $limit = '', $filterRating = '')
    {
        $query = TrainingPlan::where('user_id', $trainerId)->where('status', 1)->where('parent_id', NULL);
        if($filterRating  != '') {
            $query->orderBy('rating', $filterRating);
        }
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getTrainerPlansSub($planId, $limit = '')
    {
        $query = TrainingPlan::where('status', 1)->where('parent_id', $planId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getTrainerFavProducts($trainerId, $limit = '')
    {
        $query = TrainerFavProduct::where('user_id', $trainerId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getTrainerPlan($id)
    {
        return TrainingPlan::where('status', 1)->find($id);
    }

    public function addToWishlist($data)
    {
        return Wishlist::create($data);
    }

    public function deleteWishlist($id)
    {
        return Wishlist::destroy($id);
    }

    public function getWishlist($userId)
    {
        return Wishlist::where('user_id', $userId)->where('trainer_id', '!=', NULL)->get();
    }

    public function checkWishlist($data)
    {
        return Wishlist::where('user_id', $data['user_id'])->where('trainer_id', $data['trainer_id'])->count();
    }

    public function getReview($trainerId) 
    {
        return Review::where('trainer_id', $trainerId)->get();
    }

    public function getAvailableTimeSlots($trainerId) 
    {
        return TrainerAvailableTimeSlots::where('trainer_id', $trainerId)->get();
    }

    public function addFavProducts($data)
    {
        return TrainerFavProduct::create($data);
    }

    public function getTrainerFavProductsTable($request)
    {
        try {
            $trainerId = Crypt::decryptString($request['trainerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainerFavProduct::where('user_id', $trainerId)->get();        
    }

    public function deleteFavProduct($id)
    {
        return TrainerFavProduct::destroy($id);
    }

    public function deleteFavProductByCondition($where)
    {
        return TrainerFavProduct::where($where)->delete();
    }

    public function getTrainerActivitiesTable($request)
    {
        try {
            $trainerId = Crypt::decryptString($request['trainerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainerRegActivity::where('user_id', $trainerId)->get();        
    }

    public function deleteActivity($id)
    {
        return TrainerRegActivity::destroy($id);
    }

    public function getTrainerPlansTable($request)
    {
        try {
            $trainerId = Crypt::decryptString($request['trainerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainingPlan::where('user_id', $trainerId)->get();        
    }

    public function addRegActivities($data)
    {
        return TrainerRegActivity::create($data);
    }

    public function deleteRegActivity($where)
    {
        return TrainerRegActivity::where($where)->delete();
    }

    public function addRegCategories($data)
    {
        return TrainerRegCategory::create($data);
    }

    public function deleteRegCategory($where)
    {
        return TrainerRegCategory::where($where)->delete();
    }

    public function addRegMainCategories($data)
    {
        $regMainCategoryCount = TrainerRegMainCategory::where($data)->get()->count();
        if($regMainCategoryCount == 0) {
            return TrainerRegMainCategory::create($data);
        }
        return true;
    }

    public function deleteRegMainCategory($where)
    {
        // check any reg subcategory exists
        $categories = Category::where('main_category_id', $where['main_category_id'])->select('id')->get();
        $categoryId = [];
        foreach($categories as $cat) {
            $categoryId[] = $cat->id;
        }
        $regCategoryCount = TrainerRegCategory::whereIn('category_id', $categoryId)->get()->count();
        if($regCategoryCount == 0) {
            return TrainerRegMainCategory::where($where)->delete();
        }
        return true;
    }

    public function getTrainerSpecialitiesTable($request)
    {
        try {
            $trainerId = Crypt::decryptString($request['trainerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainerRegSpeciality::where('user_id', $trainerId)->get();        
    }

    public function deleteSpeciality($id)
    {
        return TrainerRegSpeciality::destroy($id);
    }

    public function addRegSpecialities($data)
    {
        return TrainerRegSpeciality::create($data);
    }

    public function getTrainerServiceTypesTable($request)
    {
        try {
            $trainerId = Crypt::decryptString($request['trainerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainerRegServiceType::where('user_id', $trainerId)->get();        
    }

    public function addRegServiceTypes($data)
    {
        return TrainerRegServiceType::create($data);
    }

    public function deleteRegServiceType($where)
    {
        return TrainerRegServiceType::where($where)->delete();
    }

    
}
