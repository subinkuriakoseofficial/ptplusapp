<?php

namespace App\Repositories;

use App\Interfaces\CenterRepositoryInterface;
use App\Models\Category;
use App\Models\User;
use App\Models\ServiceType;
use App\Models\TrainerFavProduct;
use App\Models\TrainerGallery;
use App\Models\TrainerRegMainCategory;
use App\Models\TrainerRegSpeciality;
use App\Models\TrainingPlan;
use App\Models\Wishlist;
use App\Models\CenterBanner;
use App\Models\CenterRegBanner;
use App\Models\CenterRegSpeciality;
use App\Models\CenterRegTrainer;
use App\Models\Product;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class CenterRepository implements CenterRepositoryInterface 
{
    public function getBanners() 
    {
        return CenterBanner::get();
    }

    public function getParentCategories() 
    {
        return Category::where('type', 'center')->where('parent_id', 0)->get();
    }

    public function getSubCategories($catId) 
    {
        return Category::where('type', 'center')->where('parent_id', $catId)->get();
    }

    public function getCenter($id)
    {
        return User::where('user_type', 'center')->where('id', $id)->first();
    }

    public function addToWishlist($data)
    {
        return Wishlist::create($data);
    }

    public function deleteWishlist($id)
    {
        return Wishlist::destroy($id);
    }

    public function getWishlist($userId)
    {
        return Wishlist::where('user_id', $userId)->where('center_id', '!=', NULL)->get();
    }

    public function checkWishlist($data)
    {
        return Wishlist::where('user_id', $data['user_id'])->where('center_id', $data['center_id'])->count();
    }

    public function getCentersByFilter($mainCategory = '', $category = '')
    {
        $query = User::where('user_type', 'center');
        if($mainCategory != '') {
            $query->whereHas('centerRegMainCategories', function($queryRow) use ($mainCategory) {
                $queryRow->where('main_category_id', $mainCategory);
            });
        }
        if($category != '') {
            $query->whereHas('centerRegCategories', function($queryRow) use ($category) {
                $queryRow->where('category_id', $category);
            });
        }
        return $query->get();
    }

    public function getCenterSpecialities($centerId)
    {
        return CenterRegSpeciality::where('user_id', $centerId)->get();
    }

    public function getCenterTrainers($centerId, $limit = '')
    {
        $query = CenterRegTrainer::where('center_id', $centerId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getPlans($centerId, $limit = '', $filterRating = '')
    {
        $query = TrainingPlan::where('center_id', $centerId)->where('status', 1);
        if($filterRating  != '') {
            $query->orderBy('rating', $filterRating);
        }
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getCenterProducts($centerId, $limit = '')
    {
        $query = Product::where('added_by', $centerId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getTrainerGallery($centerId, $limit = '')
    {
        $query = TrainerGallery::where('user_id', $centerId);
        if($limit != '') {
            $query->limit($limit);
        }
        return $query->get();
    }

    public function getBannerDataTable($request)
    {
        try {
            $centerId = Crypt::decryptString($request['centerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_trainer_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return CenterRegBanner::where('center_id', $centerId)->get();        
    }

    public function deleteBanner($id)
    {
        return CenterRegBanner::destroy($id);
    }

    public function getCenterPlansTable($request)
    {
        try {
            $centerId = Crypt::decryptString($request['centerId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_center_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainingPlan::where('center_id', $centerId)->get();        
    }

    
}
