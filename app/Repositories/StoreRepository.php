<?php

namespace App\Repositories;

use App\Interfaces\StoreRepositoryInterface;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\StoreBanner;
use App\Models\Wishlist;
use App\Models\Country;
use App\Models\ProductGallery;

class StoreRepository implements StoreRepositoryInterface 
{
    public function getBanners() 
    {
        return StoreBanner::get();
    }

    public function getCountries() 
    {
        return Country::get();
    }

    public function getCategories() 
    {
        return Category::where('type', 'store')->get();
    }

    public function getParentCategories() 
    {
        return Category::where('type', 'store')->where('parent_id', 0)->get();
    }

    public function getSubCategories($catId) 
    {
        return Category::where('type', 'store')->where('parent_id', $catId)->get();
    }

    public function getBestSellers() 
    {
        return Product::orderByRaw('RAND()')->take(5)->get();
    }

    public function getBestSellersAll() 
    {
        return Product::orderByRaw('RAND()')->get();
    }

    public function getNewArrivals() 
    {
        return Product::orderByDesc('id')->take(5)->get();
    }

    public function getNewArrivalsAll() 
    {
        return Product::orderByDesc('id')->get();
    }

    public function getProducts() 
    {
        return Product::get();
    }

    public function getProductsByCategory($catId) 
    {
        return ProductCategory::where('category_id', $catId)->with('product')->get();
    }

    public function getProductDetail($id) 
    {
        return Product::find($id);
    }

    public function getProductsByCategories($catId) 
    {
        return ProductCategory::whereIn('category_id', $catId)->with('product', function($rowQuery){
            $rowQuery->whereNull('deleted_at');
        })->take(5)->get();
    }

    public function isValidProduct($id) 
    {
        return Product::where('id', $id)->count();
    }

    public function checkWishlist($data)
    {
        return Wishlist::where('user_id', $data['user_id'])->where('product_id', $data['product_id'])->count();
    }

    public function addToWishlist($data)
    {
        return Wishlist::create($data);
    }

    public function deleteWishlist($id)
    {
        return Wishlist::destroy($id);
    }

    public function getWishlist($userId)
    {
        return Wishlist::where('user_id', $userId)->where('product_id', '!=', NULL)->get();
    }

    public function updateProduct($where, $set)
    {
        return Product::where($where)->update($set);
    }

    public function getProductsDataTable($data)
    {
        $query = Product::where('deleted_at', NULL);
        // if ($data['status'] != "") {
        //     $query->where('status', $data['status']);
        // }
        return $query;
    }

    public function addProduct($data)
    {
        return Product::create($data);
    }

    public function addProductCategories($data)
    {
        return ProductCategory::insert($data);
    }

    public function addProductGallery($data)
    {
        return ProductGallery::insert($data);
    }

    public function deleteProductCategories($where)
    {
        return ProductCategory::where($where)->delete();
    }

    public function deleteProductGallery($where)
    {
        $gallery = ProductGallery::where($where)->get();
        $delete = ProductGallery::where($where)->delete();
        if($delete) {
            return $gallery;
        }
        else {
            return false;
        }
    }

    public function deleteGalleryImage($id)
    {
        $gallery = ProductGallery::find($id);
        $gallery->delete();
        return $gallery;
    }

    public function deleteProduct($id)
    {
        return Product::destroy($id);
    }

    
}
