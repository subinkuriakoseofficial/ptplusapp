<?php

namespace App\Repositories;

use App\Interfaces\ActivityRepositoryInterface;
use App\Models\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class ActivityRepository implements ActivityRepositoryInterface 
{
    public function create($data)
    {
        return Activity::create($data);
    }

    public function find($id) 
    {
        return Activity::find($id);
    }

    public function get() 
    {
        return Activity::get();
    }

    public function getByCondition($where) 
    {
        return Activity::where($where)->get();
    }

    public function update($where, $set)
    {
        return Activity::where($where)->update($set);
    }

    public function getForDataTable() 
    {
        return Activity::get();
    }

    public function delete($id)
    {
        return Activity::destroy($id);
    }
    
}
