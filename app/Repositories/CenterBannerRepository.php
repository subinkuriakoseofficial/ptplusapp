<?php

namespace App\Repositories;

use App\Interfaces\CenterBannerRepositoryInterface;
use App\Models\CenterBanner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class CenterBannerRepository implements CenterBannerRepositoryInterface 
{
    public function create($data)
    {
        return CenterBanner::create($data);
    }

    public function find($id) 
    {
        return CenterBanner::find($id);
    }

    public function get() 
    {
        return CenterBanner::get();
    }

    public function getByCondition($where) 
    {
        return CenterBanner::where($where)->get();
    }

    public function update($where, $set)
    {
        return CenterBanner::where($where)->update($set);
    }

    public function getForDataTable() 
    {
        return CenterBanner::get();
    }

    public function delete($id)
    {
        return CenterBanner::destroy($id);
    }
    
}
