<?php

namespace App\Repositories;

use App\Interfaces\CommonRepositoryInterface;
use App\Models\Country;
use App\Models\City;
use App\Models\Block;
use App\Models\Activity;
use App\Models\Category;
use App\Models\MainCategory;
use App\Models\TrainingPlan;
use App\Models\Wishlist;
use App\Models\Currency;
use App\Models\Banner;
use App\Models\ServiceType;
use Laravel\Sanctum\PersonalAccessToken;

class CommonRepository implements CommonRepositoryInterface 
{
    public function getCountries() 
    {
        return Country::get();
    }

    public function getCitiesByCountry($countryId) 
    {
        return City::where('country_id', $countryId)->get();
    }

    public function getBlocksByCity($cityId) 
    {
        return Block::where('city_id', $cityId)->get();
    }

    public function getActivities() 
    {
        return Activity::get();
    }

    public function getServiceTypes() 
    {
        return ServiceType::get();
    }

    public function getCategory($id) 
    {
        return Category::find($id);
    }

    public function getMainCategories() 
    {
        return MainCategory::get();
    }

    public function getCategories() 
    {
        return Category::get();
    }

    public function getUserByToken($token) 
    {
        return PersonalAccessToken::findToken($token);
    }

    public function addToWishlist($data)
    {
        return Wishlist::create($data);
    }

    public function deleteWishlist($id)
    {
        return Wishlist::destroy($id);
    }

    public function getPlanWishlist($userId)
    {
        return Wishlist::where('user_id', $userId)->where('plan_id', '!=', NULL)->get();
    }

    public function checkPlanWishlist($data)
    {
        return Wishlist::where('user_id', $data['user_id'])->where('plan_id', $data['plan_id'])->count();
    }

    public function getTrainingPlan($id) 
    {
        return TrainingPlan::find($id);
    }

    public function getCurrencies() 
    {
        return Currency::get();
    }

    public function getBanners() 
    {
        return Banner::get();
    }


    
}
