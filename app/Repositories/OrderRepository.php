<?php

namespace App\Repositories;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ShippingAddress;
use Illuminate\Support\Facades\Auth;

class OrderRepository implements OrderRepositoryInterface 
{
    public function get($id)
    {
        return Order::find($id);
    }

    public function getByCondition($where)
    {
        return Order::where($where)->get();
    }

    public function create($data)
    {
        return Order::create($data);
    }

    public function addOrderItems($data)
    {
        return OrderItem::insert($data);
    }

    public function addShippingAddress($data)
    {
        return ShippingAddress::create($data);
    }
    
    public function myOrders()
    {
        return Order::where('user_id', Auth::id())->with('item')->orderBy('id', 'DESC')->get();
    }

    public function getOrderItemDetail($id)
    {
        return OrderItem::where('id', $id)->with('product')->with('order')->get();
    }

    public function updateOrder($where, $set)
    {
        return Order::where($where)->update($set);
    }

    public function getOrderDataTable() 
    {
        return Order::get();
    }


    
}
