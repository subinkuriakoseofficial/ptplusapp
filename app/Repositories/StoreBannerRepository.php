<?php

namespace App\Repositories;

use App\Interfaces\StoreBannerRepositoryInterface;
use App\Models\StoreBanner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class StoreBannerRepository implements StoreBannerRepositoryInterface 
{
    public function create($data)
    {
        return StoreBanner::create($data);
    }

    public function find($id) 
    {
        return StoreBanner::find($id);
    }

    public function get() 
    {
        return StoreBanner::get();
    }

    public function getByCondition($where) 
    {
        return StoreBanner::where($where)->get();
    }

    public function update($where, $set)
    {
        return StoreBanner::where($where)->update($set);
    }

    public function getForDataTable() 
    {
        return StoreBanner::get();
    }

    public function delete($id)
    {
        return StoreBanner::destroy($id);
    }
    
}
