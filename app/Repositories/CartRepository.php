<?php

namespace App\Repositories;

use App\Interfaces\CartRepositoryInterface;
use App\Models\Cart;

class CartRepository implements CartRepositoryInterface 
{
    public function get($id)
    {
        return Cart::find($id);
    }

    public function addToCart($data)
    {
        return Cart::create($data);
    }
    
    public function getGuestCart($guest_user_id)
    {
        return Cart::where('guest_user_id', $guest_user_id)->get();
    }

    public function getCart($userId)
    {
        return Cart::where('user_id', $userId)->get();
    }

    public function checkCartGuest($data)
    {
        return Cart::where('guest_user_id', $data['guest_user_id'])->where('product_id', $data['product_id'])->count();
    }

    public function checkCart($data)
    {
        return Cart::where('user_id', $data['user_id'])->where('product_id', $data['product_id'])->count();
    }

    /*public function isValidCartGuest($data)
    {
        return Cart::where('guest_user_id', $data['guest_user_id'])->where('id', $data['id'])->count();
    }*/

    public function updateCart($where, $set)
    {
        return Cart::where($where)->update($set);
    }

    public function deleteItem($where)
    {
        return Cart::where($where)->delete();
    }

    public function clearCart($userId)
    {
        return Cart::where('user_id', $userId)->delete();
    }

    public function clearCartGuest($guestId)
    {
        return Cart::where('guest_user_id', $guestId)->delete();
    }


    
}
