<?php

namespace App\Repositories;

use App\Interfaces\TrainingPlanRepositoryInterface;
use App\Models\TrainingPlan;
use App\Models\TrainingPlansBanner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TrainingPlanRepository implements TrainingPlanRepositoryInterface 
{
    public function create($data)
    {
        return TrainingPlan::create($data);
    }

    public function find($id) 
    {
        return TrainingPlan::find($id);
    }

    public function get() 
    {
        return TrainingPlan::get();
    }

    public function getByCondition($where) 
    {
        return TrainingPlan::where($where)->get();
    }

    public function update($where, $set)
    {
        return TrainingPlan::where($where)->update($set);
    }

    public function getForDataTable() 
    {
        return TrainingPlan::get();
    }

    public function delete($id)
    {
        return TrainingPlan::destroy($id);
    }

    public function createBanner($data)
    {
        return TrainingPlansBanner::insert($data);
    }

    public function findBanner($id) 
    {
        return TrainingPlansBanner::find($id);
    }

    public function getBanner() 
    {
        return TrainingPlansBanner::get();
    }

    public function getBannerByCondition($where) 
    {
        return TrainingPlansBanner::where($where)->get();
    }

    public function updateBanner($where, $set)
    {
        return TrainingPlansBanner::where($where)->update($set);
    }

    public function getBannerForDataTable($request) 
    {
        try {
            $planId = Crypt::decryptString($request['planId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_training_plan_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return TrainingPlansBanner::where('training_plan_id', $planId)->get();
    }

    public function deleteBanner($id)
    {
        return TrainingPlansBanner::destroy($id);
    }


    
}
