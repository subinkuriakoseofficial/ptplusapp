<?php

namespace App\Repositories;

use App\Interfaces\VendorRepositoryInterface;
use App\Models\Category;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class VendorRepository implements VendorRepositoryInterface 
{
    public function getVendors($limit = '')
    {
        $query = User::where('user_type', 'vendor')->with('vendorRegCategories');
        if($limit != '') {
            $query->limit($limit)->get();
        }
        return $query->get();
    }

    public function getVendor($id)
    {
        return User::where('user_type', 'vendor')->where('id', $id)->first();
    }

    public function getParentCategories() 
    {
        return Category::where('type', 'vendor')->where('parent_id', 0)->get();
    }

    public function getSubCategories($catId) 
    {
        return Category::where('type', 'vendor')->where('parent_id', $catId)->get();
    }

    public function getAllCategories() 
    {
        return Category::where('type', 'vendor')->get();
    }

    public function getVendorProductsTable($request)
    {
        try {
            $vendorId = Crypt::decryptString($request['vendorId']);
        } catch (DecryptException $e) {
            return redirect()->route('admin_user_vendor_list')->with('messageError', 'An error occurred, unable to fetch the data');
        }
        return Product::where('added_by', $vendorId)->get();
    }


    
}
