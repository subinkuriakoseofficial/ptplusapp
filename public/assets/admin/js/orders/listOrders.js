$(document).ready(function() {
    var url = $('#order_list_datatable').data('url');
    var table = $('#order_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'order_number', name: 'order_number'},
            {data: 'user', name: 'user'},
            {data: 'delivery_date', name: 'delivery_date'},
            {data: 'delivery_type', name: 'delivery_type'},
            {data: 'total_amount', name: 'total_amount'},
            {data: 'ordered_on', name: 'ordered_on'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action'},
        ]
    });

});
