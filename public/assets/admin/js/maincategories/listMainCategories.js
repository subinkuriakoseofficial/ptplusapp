$(document).ready(function() {
    var url = $('#main_categories_list_datatable').data('url');
    var table = $('#main_categories_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'image', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});
