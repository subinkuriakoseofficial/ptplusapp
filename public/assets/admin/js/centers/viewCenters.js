$(document).ready(function() {
    var url = $('#banners_list_datatable').data('url');
    var centerId = $('#banners_list_datatable').data('id');
    var url = url+'?centerId='+centerId;
    var table = $('#banners_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'image', name: 'image'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    var urlPlan = $('#plans_list_datatable').data('url');
    var centerId = $('#plans_list_datatable').data('id');
    var urlPlan = urlPlan+'?centerId='+centerId;
    var tablePlan = $('#plans_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlPlan,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'price', name: 'price'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});

