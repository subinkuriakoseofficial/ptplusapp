$(document).ready(function() {
    var url = $('#users_list_datatable').data('url');
    var table = $('#users_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user_type', name: 'user_type'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'gender', name: 'gender'},
            {data: 'registered_on', name: 'created_at'},
        ]
    });

});
