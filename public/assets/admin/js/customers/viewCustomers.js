$(document).ready(function() {
    var url = $('#favproducts_list_datatable').data('url');
    var trainerId = $('#favproducts_list_datatable').data('id');
    var url = url+'?trainerId='+trainerId;
    var table = $('#favproducts_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});

