$(document).ready(function() {
    var url = $('#plans_banner_list_datatable').data('url');
    var planId = $('#plans_banner_list_datatable').data('plan_id');
    var url = url+'?planId='+planId;
    var table = $('#plans_banner_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'image', name: 'thumbnail'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});
