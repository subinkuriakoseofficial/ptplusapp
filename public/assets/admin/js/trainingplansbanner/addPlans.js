$('.behalf_of').change(function() {
    var behalfOf = $(this).val();
    if(behalfOf == 'trainer') {
        $('#sectionTrainer').show();
        $('#sectionCenter').hide();
    }
    else {
        $('#sectionCenter').show();
        $('#sectionTrainer').hide();
    }
});