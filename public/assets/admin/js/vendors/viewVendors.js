$(document).ready(function() {
    var url = $('#products_list_datatable').data('url');
    var vendorId = $('#products_list_datatable').data('id');
    var url = url+'?vendorId='+vendorId;
    var table = $('#products_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});

