$(document).ready(function() {
    var url = $('#categories_list_datatable').data('url');
    var table = $('#categories_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'image', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});
