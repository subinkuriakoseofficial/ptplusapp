$("#mainCategory").change(function() {
    var mainCategoryId = $(this).val();
    var token = $('[name="_token"]').val();
    formData = '_token='+token+'&mainCategoryId='+mainCategoryId;
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'get',
        data: formData,
        success: function (data) {
            $("#category").html(data);
            $("#category").selectpicker('refresh');
        },
        cache: false,
        contentType: false,
        processData: false
    });
});
