$(document).ready(function() {
    var url = $('#trainer_favproducts_list_datatable').data('url');
    var trainerId = $('#trainer_favproducts_list_datatable').data('trainer_id');
    var url = url+'?trainerId='+trainerId;
    var table = $('#trainer_favproducts_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'select', name: 'select', searchable: false},
        ]
    });

});

function updateList(chkId)
{
    var productId = $('#pro_'+chkId).val();
    var trainerId = $('#trainer_favproducts_list_datatable').data('trainer_id');
    if($('#pro_'+chkId).is(":checked")) {
        var checked = 1;
    }
    else {
        var checked = 0;
    }
    var url = $('#trainer_favproducts_list_datatable').data('update_url');
    formData = 'trainerId='+trainerId+'&productId='+productId+'&checked='+checked;
    $.ajax({
        url: url,
        type: 'get',
        data: formData,
        success: function (data) {
            $('#labelSuccess_'+chkId).show();
        },
        error: function (error) {
            alert(error);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
