$(document).ready(function() {
    var url = $('#trainer_list_datatable').data('url');
    var table = $('#trainer_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'gender', name: 'gender'},
            {data: 'registered_on', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});
