function updateList(chkId)
{   
    var categoryId = $('#row_'+chkId).val();
    var mainCategoryId = $('#row_'+chkId).data('maincat_id');
    var trainerId = $('#trainer_regcategory_list').data('trainer_id');
    if($('#row_'+chkId).is(":checked")) {
        var checked = 1;
    }
    else {
        var checked = 0;
    }
    var url = $('#trainer_regcategory_list').data('update_url');
    formData = 'trainerId='+trainerId+'&categoryId='+categoryId+'&checked='+checked+'&mainCategoryId='+mainCategoryId;
    $.ajax({
        url: url,
        type: 'get',
        data: formData,
        success: function (data) {
            $('#labelSuccess_'+chkId).show();
        },
        error: function (error) {
            alert(error);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
