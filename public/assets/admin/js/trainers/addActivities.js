function updateList(chkId)
{   
    var activityId = $('#row_'+chkId).val();
    var trainerId = $('#trainer_regactivity_list').data('trainer_id');
    if($('#row_'+chkId).is(":checked")) {
        var checked = 1;
    }
    else {
        var checked = 0;
    }
    var url = $('#trainer_regactivity_list').data('update_url');
    formData = 'trainerId='+trainerId+'&activityId='+activityId+'&checked='+checked;
    $.ajax({
        url: url,
        type: 'get',
        data: formData,
        success: function (data) {
            $('#labelSuccess_'+chkId).show();
        },
        error: function (error) {
            alert(error);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
