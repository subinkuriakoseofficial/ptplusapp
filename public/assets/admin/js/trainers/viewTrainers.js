$(document).ready(function() {
    var url = $('#favproducts_list_datatable').data('url');
    var trainerId = $('#favproducts_list_datatable').data('id');
    var url = url+'?trainerId='+trainerId;
    var table = $('#favproducts_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    var urlActivity = $('#activities_list_datatable').data('url');
    var trainerId = $('#activities_list_datatable').data('id');
    var urlActivity = urlActivity+'?trainerId='+trainerId;
    var tableActivity = $('#activities_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        bPaginate: false,
        ajax: urlActivity,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'title', name: 'title'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    var urlPlan = $('#plans_list_datatable').data('url');
    var trainerId = $('#plans_list_datatable').data('id');
    var urlPlan = urlPlan+'?trainerId='+trainerId;
    var tablePlan = $('#plans_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlPlan,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'thumbnail', name: 'thumbnail'},
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'price', name: 'price'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    var urlSpeciality = $('#speciality_list_datatable').data('url');
    var trainerId = $('#speciality_list_datatable').data('id');
    var urlSpeciality = urlSpeciality+'?trainerId='+trainerId;
    var tableActivity = $('#speciality_list_datatable').DataTable({
        processing: true,
        serverSide: true,
        bPaginate: false,
        ajax: urlSpeciality,
        type: "POST",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'speciality', name: 'speciality'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});

