function updateList(chkId)
{   
    var serviceTypeId = $('#row_'+chkId).val();
    var trainerId = $('#trainer_regservicetype_list').data('trainer_id');
    if($('#row_'+chkId).is(":checked")) {
        var checked = 1;
    }
    else {
        var checked = 0;
    }
    var url = $('#trainer_regservicetype_list').data('update_url');
    formData = 'trainerId='+trainerId+'&serviceTypeId='+serviceTypeId+'&checked='+checked;
    $.ajax({
        url: url,
        type: 'get',
        data: formData,
        success: function (data) {
            $('#labelSuccess_'+chkId).show();
        },
        error: function (error) {
            console.log(error);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
