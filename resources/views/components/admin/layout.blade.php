<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>PT PLUS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/admin/images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/chartist/css/chartist.min.css') }}">
    <link href="{{ asset('assets/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/icons/LineIcons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/vendor/owl-carousel/owl.carousel.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/admin/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">

</head>

<body>

    <x-admin.header />

    <x-admin.sidebar />

    {{ $slot }}

    <x-admin.footer />

</body>
</html>