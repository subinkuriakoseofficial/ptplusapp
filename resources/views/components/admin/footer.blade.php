
<!--**********************************
    Footer start
***********************************-->
<div class="footer">
    <div class="copyright">
        <p>Copyright © 2022 Designed & Developed by Chrisans Web Solutions</p>
    </div>
</div>
<!--**********************************
    Footer end
***********************************-->

<!--**********************************
    Support ticket button start
***********************************-->

<!--**********************************
    Support ticket button end
***********************************-->


</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<!-- Required vendors -->
<script src="{{ asset('assets/admin/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/admin/vendor/chart.js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/deznav-init.js') }}"></script>
<script src="{{ asset('assets/admin/vendor/owl-carousel/owl.carousel.js') }}"></script>


<!-- Chart piety plugin files -->
<script src="{{ asset('assets/admin/vendor/peity/jquery.peity.min.js') }}"></script>

<!-- Apex Chart -->
<script src="{{ asset('assets/admin/vendor/apexchart/apexchart.js') }}"></script>

<!-- Dashboard 1 -->
<script src="{{ asset('assets/admin/js/dashboard/dashboard-1.js') }}"></script>

<!-- Datatable -->
<script src="{{ asset('assets/admin/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/plugins-init/datatables.init.js') }}"></script>

<script>
    function featuredmenus() {

        /*  testimonial one function by = owl.carousel.js */
        jQuery('.featured-menus').owlCarousel({
            loop: false,
            margin: 30,
            nav: true,
            autoplaySpeed: 3000,
            navSpeed: 3000,
            paginationSpeed: 3000,
            slideSpeed: 3000,
            smartSpeed: 3000,
            autoplay: false,
            dots: false,
            navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 1
                },
                767: {
                    items: 1
                },
                991: {
                    items: 2
                },
                1200: {
                    items: 2
                },
                1600: {
                    items: 3
                }
            }
        })
    }

    jQuery(window).on('load', function() {
        setTimeout(function() {
            featuredmenus();
        }, 1000);
    });
</script>

@stack('script')
