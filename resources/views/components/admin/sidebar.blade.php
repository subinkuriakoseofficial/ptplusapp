
<!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="mm-active active-no-child">
                        <a href="{{ route('admin_dashboard') }}" class="ai-icon mm-active">
                            <i class="flaticon-381-networking"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    {{-- <li>
                        <a class="ai-icon" href="{{ route('admin_user_list') }}" aria-expanded="false">
                            <i>
                             <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
								<g id="bx-user" transform="translate(-4.5 -3)">
								  <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
								  <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
								</g>
							  </svg>
                            </i>
                            <span class="nav-text">Users</span>
                        </a>

                    </li> --}}

                    <li>
                      <a class="ai-icon" href="{{ route('admin_user_trainer_list') }}" aria-expanded="false">
                          <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
                            <g id="bx-user" transform="translate(-4.5 -3)">
                              <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
                              <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                            </g>
                            </svg>
                          </i>
                          <span class="nav-text">Trainers</span>
                      </a>
                    </li>
                    <li>
                      <a class="ai-icon" href="{{ route('admin_user_center_list') }}" aria-expanded="false">
                          <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
                            <g id="bx-user" transform="translate(-4.5 -3)">
                              <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
                              <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                            </g>
                            </svg>
                          </i>
                          <span class="nav-text">Centers</span>
                      </a>
                    </li>
                    <li>
                      <a class="ai-icon" href="{{ route('admin_user_vendor_list') }}" aria-expanded="false">
                          <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
                            <g id="bx-user" transform="translate(-4.5 -3)">
                              <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
                              <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                            </g>
                            </svg>
                          </i>
                          <span class="nav-text">Vendors</span>
                      </a>
                    </li>
                    <li>
                      <a class="ai-icon" href="{{ route('admin_user_customer_list') }}" aria-expanded="false">
                          <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
                            <g id="bx-user" transform="translate(-4.5 -3)">
                              <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
                              <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                            </g>
                            </svg>
                          </i>
                          <span class="nav-text">Customers</span>
                      </a>
                    </li>

                    {{-- <li>
                      <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                        <i>
                          <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 42.631 44.999">
                          <g id="bx-user" transform="translate(-4.5 -3)">
                            <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="#fff"/>
                            <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                          </g>
                          </svg>
                         </i>
                          <span class="nav-text">Users</span>
                      </a>
                      <ul aria-expanded="false">
                          <li><a href="{{ route('admin_user_trainer_list') }}">Trainers</a></li>
                          <li><a href="{{ route('admin_user_center_list') }}">Centers</a></li>
                          <li><a href="{{ route('admin_user_vendor_list') }}">Vendors</a></li>
                          <li><a href="{{ route('admin_user_customer_list') }}">Customers</a></li>
                      </ul>
                    </li> --}}

                    <li>
                      <a class="ai-icon" href="{{ route('admin_activity_list') }}" aria-expanded="false">
                          <i class="flaticon-381-networking"></i>
                          <span class="nav-text">Activities</span>
                      </a>
                    </li>

                    <li>
                      <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                          <i>
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                          <g id="vuesax_outline_bag-2" data-name="vuesax/outline/bag-2" transform="translate(-556 -188)">
                            <g id="bag-2">
                            <path id="Vector" d="M9.75,7.384A.755.755,0,0,1,9,6.634V5.254a3.78,3.78,0,0,0-1.23-2.78,3.711,3.711,0,0,0-2.89-.95A4.043,4.043,0,0,0,1.5,5.454v.97a.755.755,0,0,1-.75.75A.755.755,0,0,1,0,6.424v-.98A5.544,5.544,0,0,1,4.74.024a5.189,5.189,0,0,1,4.04,1.34,5.27,5.27,0,0,1,1.72,3.89v1.38A.755.755,0,0,1,9.75,7.384Z" transform="translate(562.75 189.246)" fill="#7d7d7d"/>
                            <path id="Vector-2" data-name="Vector" d="M12.5,15.5h-6c-4.62,0-5.48-2.15-5.7-4.24L.054,5.27a4.746,4.746,0,0,1,.9-3.79C1.854.48,3.344,0,5.5,0h8c2.17,0,3.66.49,4.55,1.48a4.756,4.756,0,0,1,.9,3.77l-.75,6.01C17.984,13.35,17.124,15.5,12.5,15.5Zm-7-14c-1.69,0-2.85.33-3.44.99a3.236,3.236,0,0,0-.52,2.61l.75,5.99c.17,1.6.61,2.92,4.21,2.92h6c3.6,0,4.04-1.31,4.21-2.9l.75-6.01a3.242,3.242,0,0,0-.52-2.6c-.59-.67-1.75-1-3.44-1Z" transform="translate(558.496 195.25)" fill="#7d7d7d"/>
                            <path id="Vector-3" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(570.41 199.15)" fill="#7d7d7d"/>
                            <path id="Vector-4" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(563.41 199.15)" fill="#7d7d7d"/>
                            <path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(556 188)" fill="none" opacity="0"/>
                            </g>
                          </g>
                          </svg>
                          </i>
                          <span class="nav-text">Banners</span>
                      </a>
                      <ul aria-expanded="false">
                          <li><a href="{{ route('admin_banner_home_list') }}">Home Banners</a></li>
                          <li><a href="{{ route('admin_banner_store_list') }}">Store Banners</a></li>
                          <li><a href="{{ route('admin_banner_center_list') }}">Center Banners</a></li>
                      </ul>
                    </li>

                    <li>
                        <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i>
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
								<g id="vuesax_outline_bag-2" data-name="vuesax/outline/bag-2" transform="translate(-556 -188)">
								  <g id="bag-2">
									<path id="Vector" d="M9.75,7.384A.755.755,0,0,1,9,6.634V5.254a3.78,3.78,0,0,0-1.23-2.78,3.711,3.711,0,0,0-2.89-.95A4.043,4.043,0,0,0,1.5,5.454v.97a.755.755,0,0,1-.75.75A.755.755,0,0,1,0,6.424v-.98A5.544,5.544,0,0,1,4.74.024a5.189,5.189,0,0,1,4.04,1.34,5.27,5.27,0,0,1,1.72,3.89v1.38A.755.755,0,0,1,9.75,7.384Z" transform="translate(562.75 189.246)" fill="#7d7d7d"/>
									<path id="Vector-2" data-name="Vector" d="M12.5,15.5h-6c-4.62,0-5.48-2.15-5.7-4.24L.054,5.27a4.746,4.746,0,0,1,.9-3.79C1.854.48,3.344,0,5.5,0h8c2.17,0,3.66.49,4.55,1.48a4.756,4.756,0,0,1,.9,3.77l-.75,6.01C17.984,13.35,17.124,15.5,12.5,15.5Zm-7-14c-1.69,0-2.85.33-3.44.99a3.236,3.236,0,0,0-.52,2.61l.75,5.99c.17,1.6.61,2.92,4.21,2.92h6c3.6,0,4.04-1.31,4.21-2.9l.75-6.01a3.242,3.242,0,0,0-.52-2.6c-.59-.67-1.75-1-3.44-1Z" transform="translate(558.496 195.25)" fill="#7d7d7d"/>
									<path id="Vector-3" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(570.41 199.15)" fill="#7d7d7d"/>
									<path id="Vector-4" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(563.41 199.15)" fill="#7d7d7d"/>
									<path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(556 188)" fill="none" opacity="0"/>
								  </g>
								</g>
							  </svg>
							  </i>
                            <span class="nav-text">Categories</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('admin_maincategory_list') }}">Main Categories</a></li>
                            <li><a href="{{ route('admin_category_list') }}">Categories</a></li>
                        </ul>
                    </li>


                  



                    <li>
                        <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i>
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
								<g id="vuesax_outline_bag-2" data-name="vuesax/outline/bag-2" transform="translate(-556 -188)">
								  <g id="bag-2">
									<path id="Vector" d="M9.75,7.384A.755.755,0,0,1,9,6.634V5.254a3.78,3.78,0,0,0-1.23-2.78,3.711,3.711,0,0,0-2.89-.95A4.043,4.043,0,0,0,1.5,5.454v.97a.755.755,0,0,1-.75.75A.755.755,0,0,1,0,6.424v-.98A5.544,5.544,0,0,1,4.74.024a5.189,5.189,0,0,1,4.04,1.34,5.27,5.27,0,0,1,1.72,3.89v1.38A.755.755,0,0,1,9.75,7.384Z" transform="translate(562.75 189.246)" fill="#7d7d7d"/>
									<path id="Vector-2" data-name="Vector" d="M12.5,15.5h-6c-4.62,0-5.48-2.15-5.7-4.24L.054,5.27a4.746,4.746,0,0,1,.9-3.79C1.854.48,3.344,0,5.5,0h8c2.17,0,3.66.49,4.55,1.48a4.756,4.756,0,0,1,.9,3.77l-.75,6.01C17.984,13.35,17.124,15.5,12.5,15.5Zm-7-14c-1.69,0-2.85.33-3.44.99a3.236,3.236,0,0,0-.52,2.61l.75,5.99c.17,1.6.61,2.92,4.21,2.92h6c3.6,0,4.04-1.31,4.21-2.9l.75-6.01a3.242,3.242,0,0,0-.52-2.6c-.59-.67-1.75-1-3.44-1Z" transform="translate(558.496 195.25)" fill="#7d7d7d"/>
									<path id="Vector-3" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(570.41 199.15)" fill="#7d7d7d"/>
									<path id="Vector-4" data-name="Vector" d="M1.01,2A1,1,0,1,1,2,1,.994.994,0,0,1,1.01,2Z" transform="translate(563.41 199.15)" fill="#7d7d7d"/>
									<path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(556 188)" fill="none" opacity="0"/>
								  </g>
								</g>
							  </svg>
							  </i>
                            <span class="nav-text">Store Products</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('admin_product_list') }}">All Products</a></li>
                            <li><a href="{{ route('admin_product_add') }}">Add Product</a></li>
                        </ul>
                    </li>


                    

                    


                    <li>
                        <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <g id="vuesax_outline_shopping-cart" data-name="vuesax/outline/shopping-cart" transform="translate(-622 -187)">
                                  <g id="shopping-cart" transform="translate(0 -1)">
                                    <path id="Vector" d="M16.94,16.5H6.29a3.563,3.563,0,0,1-3.54-3.85l.83-9.96A1.1,1.1,0,0,0,2.48,1.5H.75A.755.755,0,0,1,0,.75.755.755,0,0,1,.75,0H2.49A2.585,2.585,0,0,1,4.98,1.88H17.47A3.566,3.566,0,0,1,20.09,3a3.524,3.524,0,0,1,.93,2.69l-.54,7.5A3.588,3.588,0,0,1,16.94,16.5ZM5.03,3.37l-.78,9.4a2.041,2.041,0,0,0,2.04,2.22H16.94A2.133,2.133,0,0,0,19,13.07l.54-7.5a2.041,2.041,0,0,0-2.06-2.21H5.03Z" transform="translate(623.25 189.25)" fill="#7d7d7d"/>
                                    <path id="Vector-2" data-name="Vector" d="M2,4A2,2,0,1,1,4,2,2.006,2.006,0,0,1,2,4ZM2,1.5a.5.5,0,1,0,.5.5A.5.5,0,0,0,2,1.5Z" transform="translate(636.25 206.75)" fill="#7d7d7d"/>
                                    <path id="Vector-3" data-name="Vector" d="M2,4A2,2,0,1,1,4,2,2.006,2.006,0,0,1,2,4ZM2,1.5a.5.5,0,1,0,.5.5A.5.5,0,0,0,2,1.5Z" transform="translate(628.25 206.75)" fill="#7d7d7d"/>
                                    <path id="Vector-4" data-name="Vector" d="M12.75,1.5H.75A.755.755,0,0,1,0,.75.755.755,0,0,1,.75,0h12a.755.755,0,0,1,.75.75A.755.755,0,0,1,12.75,1.5Z" transform="translate(630.25 195.25)" fill="#7d7d7d"/>
                                    <path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(622 188)" fill="none" opacity="0"/>
                                  </g>
                                </g>
                              </svg>
                              </i>
                            <span class="nav-text">Orders</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('admin_order_list') }}">All Orders</a></li>
                            <!-- <li><a href="cancel-orders.html">Cancel Orders</a></li>
                            <li><a href="invoice.html">Invoice</a></li> -->
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <g id="vuesax_outline_shopping-cart" data-name="vuesax/outline/shopping-cart" transform="translate(-622 -187)">
                                  <g id="shopping-cart" transform="translate(0 -1)">
                                    <path id="Vector" d="M16.94,16.5H6.29a3.563,3.563,0,0,1-3.54-3.85l.83-9.96A1.1,1.1,0,0,0,2.48,1.5H.75A.755.755,0,0,1,0,.75.755.755,0,0,1,.75,0H2.49A2.585,2.585,0,0,1,4.98,1.88H17.47A3.566,3.566,0,0,1,20.09,3a3.524,3.524,0,0,1,.93,2.69l-.54,7.5A3.588,3.588,0,0,1,16.94,16.5ZM5.03,3.37l-.78,9.4a2.041,2.041,0,0,0,2.04,2.22H16.94A2.133,2.133,0,0,0,19,13.07l.54-7.5a2.041,2.041,0,0,0-2.06-2.21H5.03Z" transform="translate(623.25 189.25)" fill="#7d7d7d"/>
                                    <path id="Vector-2" data-name="Vector" d="M2,4A2,2,0,1,1,4,2,2.006,2.006,0,0,1,2,4ZM2,1.5a.5.5,0,1,0,.5.5A.5.5,0,0,0,2,1.5Z" transform="translate(636.25 206.75)" fill="#7d7d7d"/>
                                    <path id="Vector-3" data-name="Vector" d="M2,4A2,2,0,1,1,4,2,2.006,2.006,0,0,1,2,4ZM2,1.5a.5.5,0,1,0,.5.5A.5.5,0,0,0,2,1.5Z" transform="translate(628.25 206.75)" fill="#7d7d7d"/>
                                    <path id="Vector-4" data-name="Vector" d="M12.75,1.5H.75A.755.755,0,0,1,0,.75.755.755,0,0,1,.75,0h12a.755.755,0,0,1,.75.75A.755.755,0,0,1,12.75,1.5Z" transform="translate(630.25 195.25)" fill="#7d7d7d"/>
                                    <path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(622 188)" fill="none" opacity="0"/>
                                  </g>
                                </g>
                              </svg>
                              </i>
                            <span class="nav-text">Training Plans</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('admin_training_plan_list') }}">All Plans</a></li>
                            <!-- <li><a href="cancel-orders.html">Cancel Orders</a></li>
                            <li><a href="invoice.html">Invoice</a></li> -->
                        </ul>
                    </li>

                </ul>

                <div class="copyright">
                    <p>Copyright © 2022 Designed & Developed by Chrisans Web Solutions</p>
                </div>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->
