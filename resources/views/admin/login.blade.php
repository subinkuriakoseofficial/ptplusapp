<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>PT PLUS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/admin/images/favicon.png') }}">
    <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">
    <style>
        .form-error-msg{
            color: red;
        }
        .form-success-msg{
            color: green;
        }
    </style>
</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form text-center">
                                    <?php if(isset($_SESSION['ptplusAdminErrorMsg'])) { ?>
                                        <p class="form-error-msg"><?php echo $_SESSION['ptplusWebAdminErrorMsg']; unset($_SESSION["ptplusWebAdminErrorMsg"]); ?></p>
                                    <?php }
                                        if(isset($_SESSION['ptplusAdminSuccessMsg'])) { ?>
                                            <p class="form-success-msg"><?php echo $_SESSION['ptplusWebAdminSuccessMsg']; unset($_SESSION["ptplusWebAdminSuccessMsg"]); ?></p>
                                    <?php } ?>
                                    <img src="{{ asset('assets/admin/images/logo.png') }}" class="mb-4">
                                    <h4 class="text-center mb-4">Sign in to your account</h4>
                                    <form action="{{ route('admin_validate_login') }}" class="text-left" method="post">
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Email</strong></label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1"><strong>Password</strong></label>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <!-- <div class="form-group">
                                               <div class="custom-control custom-checkbox ml-1">
													<input type="checkbox" class="custom-control-input" id="basic_checkbox_1">
													<label class="custom-control-label" for="basic_checkbox_1">Remember my preference</label>
												</div>
                                            </div> -->
                                            <!-- <div class="form-group">
                                                <a href="page-forgot-password.html">Forgot Password?</a>
                                            </div> -->
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-dark btn-block">Sign Me In</button>
                                        </div>
                                    </form>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('assets/admin/vendor/global/global.min.js') }}"></script>
	<script src="{{ asset('assets/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/custom.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/deznav-init.js') }}"></script>

</body>


</html>