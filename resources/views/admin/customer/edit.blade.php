<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Edit Activity</h4>
                        <a href="{{ route('admin_activity_list') }}" class="btn btn-rounded btn-dark"> List Activities</a>
                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <form action="{{ route('admin_activity_update') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Title (English)</label>
                                    <input type="text" name="title_en" placeholder="Title" class="form-control" value="{{ old('title_en', $activity->title_en) }}">
                                    @error('title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Title (Arabic)</label>
                                    <input type="text" name="title_ar" placeholder="Title" class="form-control" value="{{ old('title_ar', $activity->title_ar) }}">
                                    @error('title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>                            

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div>
                                        <img src="{{ Storage::disk('public')->url($activity->thumbnail_img) }}" style="max-width: 300px; max-height: 300px;" />
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    <input type="hidden" name="Xthumbnail" value="{{ $activity->thumbnail_img }}">
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{ $activity->idEnc }}">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>