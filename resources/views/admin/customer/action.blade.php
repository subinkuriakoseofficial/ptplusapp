<div class="d-flex">
    <a href="{{ route('admin_user_customer_view', ['id' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-eye"></i></a>
    {{-- <a href="{{ route('admin_user_center_edit', ['id' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a> --}}
    <a href="{{ route('admin_user_customer_delete', ['id' => $id]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to delete the user?')"><i class="fa fa-trash"></i></a>
</div>