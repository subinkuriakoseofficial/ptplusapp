<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Add Banner</h4>
                        <a href="{{ route('admin_banner_home_list') }}" class="btn btn-rounded btn-dark"> List All</a>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('admin_banner_home_add_data') }}" method="post" enctype="multipart/form-data" id="categoryForm">
                            @csrf
                            

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Title (English)</label>
                                    <input type="text" name="title_en" placeholder="Title" class="form-control" value="{{ old('title_en') }}">
                                    @error('title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Title (Arabic)</label>
                                    <input type="text" name="title_ar" placeholder="Title" class="form-control" value="{{ old('title_ar') }}">
                                    @error('title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>    
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Description (English)</label>
                                    <textarea name="description_en" class="form-control">{{ old('description_en') }}</textarea>
                                    @error('description_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Description (Arabic)</label>
                                    <textarea name="description_ar" class="form-control">{{ old('description_ar') }}</textarea>
                                    @error('description_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>    

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>