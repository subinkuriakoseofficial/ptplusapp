<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainers/viewTrainers.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">

        <!-- row -->
        @if (session('messageSuccess'))
            <div class="alert alert-success">
                {{ session('messageSuccess') }}
            </div>
        @endif
        @if (session('messageError'))
            <div class="alert alert-warning">
                {{ session('messageError') }}
            </div>
        @endif
        
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-tab">
                            <div class="custom-tab-1">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a href="#profile" data-toggle="tab" class="nav-link active show">Profile</a></li>
                                    <li class="nav-item"><a href="#favProducts" data-toggle="tab" class="nav-link ">Favourite Products</a></li>
                                    <li class="nav-item"><a href="#activities" data-toggle="tab" class="nav-link">Activities</a></li>
                                    <li class="nav-item"><a href="#categories" data-toggle="tab" class="nav-link">Categories</a></li>
                                    <li class="nav-item"><a href="#serviceTypes" data-toggle="tab" class="nav-link">Service Types</a></li>
                                    <li class="nav-item"><a href="#specialities" data-toggle="tab" class="nav-link">Specialities</a></li>
                                    <li class="nav-item"><a href="#plans" data-toggle="tab" class="nav-link">Training Plans</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="profile" class="tab-pane fade active show">

                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Name
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6 mb-1">
                                                    <span>{{ $user->name }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Email
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->email }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Date of Birth
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ ucfirst($user->dob) }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Phone
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->phone_code . ' ' . $user->phone }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Country
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->country }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Gender
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->gender }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Registered On
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->created_at }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Rating
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->rating }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Phone
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->phone }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Experience
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->experience }} Years</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">About
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->about }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Address
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->address }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Latitude
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->latitude }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Longitude
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->trainerDetails->longitude }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Password
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $pswdNonHashed }}</span>
                                                </div>
                                            </div>
                                            

                                        </div>
                                    </div>
                                    <div id="favProducts" class="tab-pane fade ">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_user_trainer_favproduct_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Favourite</a>
                                        </div>
                                        <div class="my-post-content pt-3">

                                            <div class="table-responsive">
                                                <table class="display min-w850" id="favproducts_list_datatable" data-url="{{ route('admin_user_trainer_favproduct_table') }}" data-id="{{ $user->idEnc }}">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Thumbnail</th>
                                                            <th>Name</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="activities" class="tab-pane fade">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_user_trainer_activity_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Activity</a>
                                        </div>
                                        <div class="my-post-content pt-3">

                                            <div class="table-responsive">
                                                <table class="display min-w850" id="activities_list_datatable" data-url="{{ route('admin_user_trainer_activity_table') }}" data-id="{{ $user->idEnc }}">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Thumbnail</th>
                                                            <th>Title</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>                                            

                                        </div>
                                    </div>

                                    <div id="categories" class="tab-pane fade">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_user_trainer_category_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Category</a>
                                        </div>
                                        <h3>Main Categories: </h3>
                                        @foreach($mainCategories as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->mainCategory->name }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <h3>Categories: </h3>
                                        @foreach($categories as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->category->name }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div id="serviceTypes" class="tab-pane fade">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_user_trainer_servicetype_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Service Type</a>
                                        </div>
                                        @foreach($serviceTypes as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->serviceType->title }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div id="specialities" class="tab-pane fade">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_user_trainer_speciality_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Speciality</a>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="display min-w850" id="speciality_list_datatable" data-url="{{ route('admin_user_trainer_speciality_table') }}" data-id="{{ $user->idEnc }}">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Speciality</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="plans" class="tab-pane fade">
                                        <div class="card-header justify-content-between">
                                            <a href="{{ route('admin_training_plan_trainer_add', ['trainerId' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Add Plan</a>
                                        </div>
                                        <div class="my-post-content pt-3">

                                            <div class="table-responsive">
                                                <table class="display min-w850" id="plans_list_datatable" data-url="{{ route('admin_user_trainer_plan_table') }}" data-id="{{ $user->idEnc }}">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Image</th>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                            <th>Price</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/tabs-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layout>