<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainers/addCategories.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Manage Categories</h4>
                        <a href="{{ route('admin_user_trainer_view', ['id' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Back to View</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <div class="row">
                            {{-- filter --}}
                        </div>

                        <div class="card-body">
    
                            <div class="form-row" id="trainer_regcategory_list" data-trainer_id="{{ $user->idEnc }}" data-update_url="{{ route('admin_user_trainer_category_update') }}">
                                @foreach ($mainCategories as $mainCat)
                                <div class="form-group col-md-12">
                                    <h5>{{ $mainCat->name }}</h5>
                                    <div class="row">
                                        @foreach($mainCat->category as $category)
                                        @php $categoryId = $category->idEnc; @endphp
                                        <div class="form-group col-md-6">
                                            <input class="form-check-input regCategories" type="checkbox" id="row_{{ $categoryId }}" value="{{ $categoryId }}" @if($regCategories->contains('category_id', $category->id) == true) checked @endif onclick="updateList('{{ $categoryId }}')" data-maincat_id="{{ $mainCat->idEnc }}">
                                            <label for="row_{{ $categoryId }}">{{ $category->name }}</label>
                                            <label class="label label-success" id="labelSuccess_{{ $categoryId }}" style="display:none">Updated</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
    
                        </div>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>