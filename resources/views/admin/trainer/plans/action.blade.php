<div class="d-flex">
    <a href="{{ route('admin_training_plan_edit', ['id' => $id]) }}" target="_blank" class="btn btn-danger shadow btn-xs sharp" title="Edit this plan"><i class="fa fa-pencil"></i></a>
</div>