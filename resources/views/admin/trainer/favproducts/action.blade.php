<div class="d-flex">
    <a href="{{ route('admin_user_trainer_favproduct_delete', ['id' => $id, 'trainerId' => $trainerId]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to remove the product from favourite list?')" title="Remove from favourite list"><i class="fa fa-trash"></i></a>
</div>