<div class="d-flex">
    <input class="form-check-input favproducts" type="checkbox" id="pro_{{ $productId }}" value="{{ $productId }}" @if($selected == true) checked @endif onclick="updateList('{{ $productId }}')">
    <label class="label label-success" id="labelSuccess_{{ $productId }}" style="display:none">Updated</label>
</div>