<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainers/addFavProducts.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">




            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Manage Favourite Products</h4>
                        <a href="{{ route('admin_user_trainer_view', ['id' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Back to View</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <div class="row">
                            {{-- filter --}}
                        </div>

                        <div class="table-responsive">
                            <table class="display min-w850" id="trainer_favproducts_list_datatable" data-url="{{ route('admin_user_trainer_favproduct_add_table') }}" data-trainer_id="{{ $user->idEnc }}" data-update_url="{{ route('admin_user_trainer_favproduct_update') }}">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Thumbnail</th>
                                        <th>Name</th>
                                        <th>Select</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>