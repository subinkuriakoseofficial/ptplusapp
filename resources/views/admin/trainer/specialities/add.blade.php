<x-admin.layout>

    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
    
                <div class="col-12">
                    <div class="card">
                        <div class="card-header justify-content-between">
                            <h4 class="card-title">Add Speciality</h4>
                            <a href="{{ route('admin_user_trainer_view', ['id' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Back to View</a>
                        </div>
                        <div class="card-body">
    
                            <form action="{{ route('admin_user_trainer_speciality_add_data') }}" method="post" enctype="multipart/form-data" id="userForm">
                                @csrf
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Speciality (English)</label>
                                        <input type="text" name="title_en" placeholder="Speciality" class="form-control" value="{{ old('title_en') }}">
                                        @error('title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Speciality (Arabic)</label>
                                        <input type="text" name="title_ar" placeholder="Speciality" class="form-control" value="{{ old('title_ar') }}">
                                        @error('title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>
                                <input type="hidden" name="trainer_id" value="{{ $user->idEnc }}">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
    
                        </div>
                    </div>
                </div>
    
    
    
    
    
            </div>
        </div>
    </div>
    
    </x-layout>