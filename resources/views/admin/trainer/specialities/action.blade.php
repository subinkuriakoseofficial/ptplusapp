<div class="d-flex">
    <a href="{{ route('admin_user_trainer_speciality_delete', ['id' => $id, 'trainerId' => $trainerId]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to remove the speciality?')" title="Remove from list"><i class="fa fa-trash"></i></a>
</div>