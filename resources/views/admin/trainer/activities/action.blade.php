<div class="d-flex">
    <a href="{{ route('admin_user_trainer_activity_delete', ['id' => $id, 'trainerId' => $trainerId]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to remove the activity from the list?')" title="Remove from list"><i class="fa fa-trash"></i></a>
</div>