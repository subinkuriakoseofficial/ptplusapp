<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainers/addActivities.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">




            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Manage Activities</h4>
                        <a href="{{ route('admin_user_trainer_view', ['id' => $user->idEnc]) }}" class="btn btn-rounded btn-dark"> Back to View</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <div class="row">
                            {{-- filter --}}
                        </div>

                        <div class="card-body">
    
                            <div class="form-row" id="trainer_regactivity_list" data-trainer_id="{{ $user->idEnc }}" data-update_url="{{ route('admin_user_trainer_activity_update') }}">
                                @foreach($activities as $activity)
                                @php $activityId = $activity->idEnc; @endphp
                                <div class="form-group col-md-6">
                                    <input class="form-check-input regActivities" type="checkbox" id="row_{{ $activityId }}" value="{{ $activityId }}" @if($regActivities->contains('activity_id', $activity->id) == true) checked @endif onclick="updateList('{{ $activityId }}')">
                                    <label for="row_{{ $activityId }}">{{ $activity->title }}</label>
                                    <label class="label label-success" id="labelSuccess_{{ $activityId }}" style="display:none">Updated</label>
                                </div>
                                @endforeach
                                
                            </div>
    
                        </div>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>