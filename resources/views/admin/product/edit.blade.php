<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Edit Product</h4>
                        <a href="{{ route('admin_product_list') }}" class="btn btn-rounded btn-dark"> All Products</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <form action="{{ route('admin_product_update') }}" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Product Name (English)</label>
                                    <input type="text" name="name_en" placeholder="Name" class="form-control" value="{{ old('name_en', $product->name_en) }}">
                                    @error('name_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Product Name (Arabic)</label>
                                    <input type="text" name="name_ar" placeholder="Name" class="form-control" value="{{ old('name_ar', $product->name_ar) }}">
                                    @error('name_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Product Category</label>
                                    <select name="category[]" class="form-control" multiple>
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $row)
                                            <option value="{{ $row->id }}" @if(old('category')) {{ in_array($row->id, old('category')) ? 'selected' : '' }} @endif {{ in_array($row->id, $categoriesSelected) ? 'selected' : '' }} >{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Product Stock</label>
                                    <input type="number" name="stock" placeholder="Stock" class="form-control" value="{{ old('stock', $product->current_stock) }}">
                                    @error('stock')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Unit Price</label>
                                    <input type="number" name="price" placeholder="Price" class="form-control" value="{{ old('price', $product->unit_price) }}">
                                    @error('price')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Currency</label>
                                    <select name="currency" class="form-control" >
                                        <option value="">Choose Currency</option>
                                        @foreach($currencies as $row)
                                            <option value="{{ $row->id }}" {{ old('currency', $product->currency_id) == $row->id ? 'selected' : '' }} >{{ $row->code }}</option>
                                        @endforeach
                                    </select>
                                    @error('currency')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Description (English)</label>
                                    <textarea name="description_en" class="form-control" rows="4" >{{ old('description_en', $product->description_en) }}</textarea>
                                    @error('description_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Description (Arabic)</label>
                                    <textarea name="description_ar" class="form-control" rows="4" >{{ old('description_ar', $product->description_ar) }}</textarea>
                                    @error('description_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Thumbnail Image</label>
                                    <div>
                                        <img src="{{ Storage::disk('public')->url($product->thumbnail_img) }}" style="max-width: 300px; max-height: 300px;" />
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    <input type="hidden" name="Xthumbnail" value="{{ $product->thumbnail_img }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Gallery Images</label>
                                    <div>
                                        @foreach($product->gallery as $gallery)
                                            <img src="{{ Storage::disk('public')->url($gallery->image) }}" style="max-width: 300px; max-height: 300px;" />
                                            <a href="{{ route('admin_product_delete_gallery_image', ['id' => Crypt::encryptString($gallery->id), 'product_id' => $product->idEnc]) }}" style="color: red; position: relative; top: -81px; left: -17px; font-size: larger;" title="Delete image" onclick="return confirm('Want to delete the image?')">X</a>
                                        @endforeach
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="images[]" class="custom-file-input" multiple>
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('images')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 
                            <input type="hidden" name="id" value="{{ $product->idEnc }}">

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>