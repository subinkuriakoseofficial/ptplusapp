<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Add Product</h4>
                        <a href="{{ route('admin_product_list') }}" class="btn btn-rounded btn-dark"> All Products</a>

                    </div>
                    <div class="card-body">

                        <form action="{{ route('admin_product_add_data') }}" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Product Name (English)</label>
                                    <input type="text" name="name_en" placeholder="Name" class="form-control" value="{{ old('name_en') }}">
                                    @error('name_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Product Name (Arabic)</label>
                                    <input type="text" name="name_ar" placeholder="Name" class="form-control" value="{{ old('name_ar') }}">
                                    @error('name_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Product Category</label>
                                    <select name="category[]" class="form-control" multiple>
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $row)
                                            <option value="{{ $row->id }}" @if(old('category')) {{ in_array($row->id, old('category')) ? 'selected' : '' }} @endif>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Product Stock</label>
                                    <input type="number" name="stock" placeholder="Stock" class="form-control" value="{{ old('stock') }}">
                                    @error('stock')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Unit Price</label>
                                    <input type="number" name="price" placeholder="Price" class="form-control" value="{{ old('price') }}">
                                    @error('price')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Currency</label>
                                    <select name="currency" class="form-control" >
                                        <option value="">Choose Currency</option>
                                        @foreach($currencies as $row)
                                            <option value="{{ $row->id }}" {{ old('currency') == $row->id ? 'selected' : '' }} >{{ $row->code }}</option>
                                        @endforeach
                                    </select>
                                    @error('currency')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Description (English)</label>
                                    <textarea name="description_en" class="form-control" rows="4" >{{ old('description_ar') }}</textarea>
                                    @error('description_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Description (Arabic)</label>
                                    <textarea name="description_ar" class="form-control" rows="4" >{{ old('description_ar') }}</textarea>
                                    @error('description_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Thumbnail Image</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Gallery Images</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="images[]" class="custom-file-input" multiple>
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('images')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>