@if($stock > 0)
    <span class="btn btn-success btn-sm btn-rounded">In Stock</span>
@else 
    <span class="btn btn-warning btn-sm btn-rounded">Out of Stock</span>
@endif