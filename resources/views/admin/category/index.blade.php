<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/categories/listCategories.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">




            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Categories</h4>
                        <a href="{{ route('admin_category_add') }}" class="btn btn-rounded btn-dark"> Add Category</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <div class="row">

                            <div class="d-flex align-items-center flex-wrap search-job px-0 mb-4 w-100">
                                <div class="col-xl-3 col-xxl-3 col-lg-3 col-sm-6 col-12 search-dropdown d-flex align-items-center">
                                    <select class="form-control border-0 default-select style-1 h-75">
                                            <option value="">Choose Category</option>
                                            <optgroup>
                                            <option>Barbells & Plates</option>
                                            <option>Cardio Machines</option>
                                            <option>Gym Accessories</option>
                                            <option>Crossfit Equipment</option>
                                            <option>Home Gym Equipment</option>
                                            
                                            </optgroup>
                                        </select>
                                </div>
                                <div class="col-xl-3 col-xxl-3 col-lg-3 col-sm-6 col-12 search-dropdown d-flex align-items-center">
                                    <select class="form-control border-0 default-select style-1 wide auto-hieight">
                                            <option value="" selected="selected">Sorting</option>
                                            <option value="1">Sort by Latest</option>
                                            <option value="2">Sort by A-Z</option>
                                        </select>
                                </div>
                                <div class="col-xl-6 col-xxl-6 col-lg-6 col-12 d-md-flex job-title-search pe-0">


                                    <div class="input-group search-area w-100">
                                        <input type="text" class="form-control h-auto" placeholder="search users...">
                                        <span class="input-group-text"><a href="javascript:void(0)" class="btn btn-primary ">Search<i class="flaticon-381-search-2 ms-2"></i></a></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="table-responsive">
                            <table class="display min-w850" id="categories_list_datatable" data-url="{{ route('admin_category_table') }}">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>