<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/categories/addCategories.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Add Category</h4>
                        <a href="{{ route('admin_category_list') }}" class="btn btn-rounded btn-dark"> List All</a>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('admin_category_add_data') }}" method="post" enctype="multipart/form-data" id="categoryForm">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Main Category</label>
                                    <select name="main_category" class="form-control" id="mainCategory" data-url="{{ route('admin_category_ajax_list_by_main_category') }}">
                                        <option value="">Choose Category</option>
                                        @foreach($mainCategories as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('main_category')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Parent Category</label>
                                    <select name="category" class="form-control" id="category">
                                        <option value="">Choose Category</option>
                                    </select>
                                    @error('category')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Name (English)</label>
                                    <input type="text" name="name_en" placeholder="Name" class="form-control" value="{{ old('name_en') }}">
                                    @error('name_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Name (Arabic)</label>
                                    <input type="text" name="name_ar" placeholder="Name" class="form-control" value="{{ old('name_ar') }}">
                                    @error('name_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>                            

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Category Type</label>
                                    <select name="type" class="form-control">
                                        <option value="">Choose Type</option>
                                        @foreach($types as $type)
                                            <option value="{{ $type }}" @if(old('type')) {{ (old('type') == $type) ? 'selected' : '' }} @endif>{{ ucfirst($type) }}</option>
                                        @endforeach
                                    </select>
                                    @error('type')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                            <!-- @error('slug')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror -->
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>