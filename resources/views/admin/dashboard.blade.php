<x-admin.layout>

<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl col-md-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="d-inline-block mb-4 ml--12 position-relative donut-chart-sale">
                            <span class="donut1" data-peity='{ "fill": ["rgb(28, 28, 30)", "rgba(18, 18, 18, 1)"],   "innerRadius": 45, "radius": 10}'>4/8</span>
                            <small class="text-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="42.631" height="44.999" viewBox="0 0 42.631 44.999">
                                    <g id="bx-user" transform="translate(-4.5 -3)">
                                        <path id="Path_19" data-name="Path 19" d="M27.71,13.105A7.105,7.105,0,1,1,20.605,6,7.105,7.105,0,0,1,27.71,13.105Z" transform="translate(5.21 1.737)" fill="none"/>
                                        <path id="Path_20" data-name="Path 20" d="M25.815,3A11.842,11.842,0,1,0,37.657,14.842,11.855,11.855,0,0,0,25.815,3Zm0,18.947a7.105,7.105,0,1,1,7.105-7.105A7.114,7.114,0,0,1,25.815,21.947ZM47.131,48V45.631A16.6,16.6,0,0,0,30.552,29.052H21.079A16.6,16.6,0,0,0,4.5,45.631V48H9.237V45.631A11.854,11.854,0,0,1,21.079,33.789h9.473A11.854,11.854,0,0,1,42.394,45.631V48Z" transform="translate(0 0)" fill="#fff"/>
                                    </g>
                                    </svg>
                            </small>
                            <span class="circle"></span>
                        </div>
                        <h2 class="fs-24 text-white font-w600 mb-0">10K</h2>
                        <span class="fs-16">Users</span>
                    </div>
                </div>
            </div>
            <div class="col-xl col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="d-inline-block mb-4 ml--12 position-relative donut-chart-sale">
                            <span class="donut1" data-peity='{ "fill": ["rgb(28, 28, 30)", "rgba(18, 18, 18, 1)"],   "innerRadius": 45, "radius": 10}'>4/8</span>
                            <small class="text-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64" height="64" viewBox="0 0 64 64">
                                    <image id="icons8-fitness-64-white" width="64" height="64" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAGymlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDUgNzkuMTYzNDk5LCAyMDE4LzA4LzEzLTE2OjQwOjIyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIyLTA5LTMwVDEzOjMzOjQzKzA1OjMwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMi0wOS0zMFQxNDoxNzo0OSswNTozMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMi0wOS0zMFQxNDoxNzo0OSswNTozMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpkYjhhYjk1MC0zYTIxLTU4NDUtYTA0MC0wYjcwMTkwYjE1NTIiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDo1ZDVjMTE2OC1kNjEwLWYwNGEtYjY2NS0xMzgzYTY0ODM5ZTUiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjOWFlOTRkOS0wZGY3LWNkNGYtOTdlNC1jMDVlNjE3ZmE1ZWYiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmM5YWU5NGQ5LTBkZjctY2Q0Zi05N2U0LWMwNWU2MTdmYTVlZiIgc3RFdnQ6d2hlbj0iMjAyMi0wOS0zMFQxMzozMzo0MyswNTozMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpkNGI2MjdhMi00ZjQ3LWQxNDctYTI5Ni01MjAwZGY4YjY5MGEiIHN0RXZ0OndoZW49IjIwMjItMDktMzBUMTQ6MTc6NDkrMDU6MzAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZGI4YWI5NTAtM2EyMS01ODQ1LWEwNDAtMGI3MDE5MGIxNTUyIiBzdEV2dDp3aGVuPSIyMDIyLTA5LTMwVDE0OjE3OjQ5KzA1OjMwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOSAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ODS/bAAABHRJREFUeJztmr9vHEUUxz9nIKaIL4kiLJBtItGkwEIgkCKhIAqQKCAFBCiAFtFAXNDwH3Ap+AcQDT/coIgUJC5ANDgolgK5CBMIRSgcLkigQDi7gDRfinmHl83t3szu3KxR7is9zd7M25nvezuzM+/dtiSRGLuA14BXgPsBAReBZeBd4O+UZFqJHTAPnAYeKGjvAkeAXipCU6kGAu4ETuGM/xF4DmgDM8DzwBXgIeBT3CxJA0mpZEkOlyTtG9K+X9KG6byeilfKGfCSlW8Bfwxpvwa8adcvJ2FE2nfAJrAbN+03C3TawJ/W3k5BKqUDBgO1IulFQcolsCMxcUDTBJpGSgdsWTlTorPHyv6YufyLlA64aOUTJTpP5XTHjpQOWLayA+wb0r4feMeuP0rCCJKeBKclde2ktyHpBUltkxclXbG2byTtSsUrdTA0h4sHHixoP48Lhq6mIpTaAeACnVdxx91Fq1vHLZH3gBspyTThgB2FyTkgQh+34dbtJ8BlXEZHY5IbwE821hEbuxbqLoEDwPvA43WJVMQaLrV2uXIPNbaQeyX9ktnWliTdN+Yt7A4bYymzbV6VdKBqn3XInDMCn0vam2rfzsheG1uSzlftZwq3LR3H7b2DtdbDndjKcnNbwFngKHA9YNJNA8dw03fLZA14w9p8cd3GXgP+KtErt09SR8XoRH5qc5IulIzXNZ2YY5bah6Se/Xg0c9Nhq+tFJDKdMf6SpGclzUjaLemothOiXdONNW6pfWS8kb+xqL6qHMsYnzIrXGpf6DlgATiBi9f7wEngoOe9g0xvzKxwHT4OATNgQdI13YzfrW3Uk9g0/ZkSnbbp9D368+VTal+IA07Y71NyL6p5SStW97EH4aJxqur58im1ryWpKA2dr+/j0lkLwM+ZKbhhbXsoR+y0uC+fUvt8HJCHr14RfB1Qtb8g3pNo0EOnhd+/NK0REpIV3vToLwrvlDPgeytv2azwINPrkxVeHtI+HgRsg756ReKbFe7KL6SOwjtkG/TVK8OorPAF4Bn8PpGJwnuK7RT04UzjY1bGTk/3gEO40DcbDp+1ukPE/z6o3D6Vh4tvR1wCscWXT6l9yK23jrbDRtl1R/9di/9XB5TaF5IUDT0xFuFr4Gng11z9LLACPBzYX513UpRt8Eyg/iPAFziDB5i1ulDjVwP1b0aEKRcid0n61vr5QdI9Q+ruHtPSGCq3B/jqDOHTPY/fgCdxT3sR+MzqF4HvcKfE/NIIRRjPCh5ekIvF+yYnJR0M7GNW0nrmaa1bXWoewQ6omxXKO+GcSRXjo/AIdcAgC7NiA81JOm112SzMV5JWA/suk3x/vjxGSuh/g8OyMPO4D519sjBV4ZOdGsZjJEIdUPf8XRXjiEuA8HNAyrghCY9QB3xo5Srb/7N9aXUfBPZVB/F4BL6M6sYNVSXfny+P6C9Bb79aOa53QDRMssJNE2gaEwc0TaBpTBzQNIGmMXFA0wSaxi3vgJCUWBXs+E/RxzUDQjPFPqifAR6CfwBXgZji+T1hwQAAAABJRU5ErkJggg=="/>
                                    </svg>
                            </small>
                            <span class="circle"></span>
                        </div>
                        <h2 class="fs-24 text-white font-w600 mb-0">100</h2>
                        <span class="fs-16">Trainers</span>
                    </div>
                </div>
            </div>
            <div class="col-xl col-md-4 col-sm-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="d-inline-block mb-4 ml--12 position-relative donut-chart-sale">
                            <span class="donut1" data-peity='{ "fill": ["rgb(28, 28, 30)", "rgba(18, 18, 18, 1)"],   "innerRadius": 45, "radius": 10}'>4/8</span>
                            <small class="text-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="54" height="54" viewBox="0 0 24 24">
                                    <g id="vuesax_outline_element-3" data-name="vuesax/outline/element-3" transform="translate(-620 -252)">
                                        <g id="element-3">
                                        <path id="Vector" d="M7.02,10H2.98C.97,10,0,9.11,0,7.27V2.73C0,.89.98,0,2.98,0H7.02C9.03,0,10,.89,10,2.73V7.26C10,9.11,9.02,10,7.02,10ZM2.98,1.5c-1.34,0-1.48.38-1.48,1.23V7.26c0,.86.14,1.23,1.48,1.23H7.02c1.34,0,1.48-.38,1.48-1.23V2.73c0-.86-.14-1.23-1.48-1.23Z" transform="translate(632.75 253.25)" fill="#fff"/>
                                        <path id="Vector-2" data-name="Vector" d="M7.02,10H2.98C.97,10,0,9.02,0,7.02V2.98C0,.97.98,0,2.98,0H7.02C9.03,0,10,.98,10,2.98V7.02C10,9.02,9.02,10,7.02,10ZM2.98,1.5C1.8,1.5,1.5,1.8,1.5,2.98V7.02c0,1.18.3,1.48,1.48,1.48H7.02C8.2,8.5,8.5,8.2,8.5,7.02V2.98C8.5,1.8,8.2,1.5,7.02,1.5Z" transform="translate(632.75 264.75)" fill="#fff"/>
                                        <path id="Vector-3" data-name="Vector" d="M7.02,10H2.98C.97,10,0,9.11,0,7.27V2.73C0,.89.98,0,2.98,0H7.02C9.03,0,10,.89,10,2.73V7.26C10,9.11,9.02,10,7.02,10ZM2.98,1.5c-1.34,0-1.48.38-1.48,1.23V7.26c0,.86.14,1.23,1.48,1.23H7.02c1.34,0,1.48-.38,1.48-1.23V2.73c0-.86-.14-1.23-1.48-1.23Z" transform="translate(621.25 253.25)" fill="#fff"/>
                                        <path id="Vector-4" data-name="Vector" d="M7.02,10H2.98C.97,10,0,9.02,0,7.02V2.98C0,.97.98,0,2.98,0H7.02C9.03,0,10,.98,10,2.98V7.02C10,9.02,9.02,10,7.02,10ZM2.98,1.5C1.8,1.5,1.5,1.8,1.5,2.98V7.02c0,1.18.3,1.48,1.48,1.48H7.02C8.2,8.5,8.5,8.2,8.5,7.02V2.98C8.5,1.8,8.2,1.5,7.02,1.5Z" transform="translate(621.25 264.75)" fill="#fff"/>
                                        <path id="Vector-5" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(620 252)" fill="none" opacity="0"/>
                                        </g>
                                    </g>
                                    </svg>
                            </small>
                            <span class="circle"></span>
                        </div>
                        <h2 class="fs-24 text-white font-w600 mb-0">200</h2>
                        <span class="fs-16">Centers</span>
                    </div>
                </div>
            </div>
            <div class="col-xl col-md-4 col-sm-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="d-inline-block mb-4 ml--12 position-relative donut-chart-sale">
                            <span class="donut1" data-peity='{ "fill": ["rgb(28, 28, 30)", "rgba(18, 18, 18, 1)"],   "innerRadius": 45, "radius": 10}'>4/8</span>
                            <small class="text-primary">
                                <svg id="menu-board" xmlns="http://www.w3.org/2000/svg" width="52.667" height="52.667" viewBox="0 0 52.667 52.667">
                                    <path id="Vector" d="M35.416,42.9H6.581A6.6,6.6,0,0,1,.3,34.343L9.543,4.652A6.544,6.544,0,0,1,15.819,0H42.811a6.526,6.526,0,0,1,6.057,4.038A6.669,6.669,0,0,1,49.2,8.185L41.8,37.876A6.532,6.532,0,0,1,35.416,42.9ZM15.841,3.314A3.292,3.292,0,0,0,12.7,5.64L3.464,35.331a3.315,3.315,0,0,0,.527,2.984A3.233,3.233,0,0,0,6.6,39.632H35.437a3.265,3.265,0,0,0,3.182-2.5l7.4-29.713a3.211,3.211,0,0,0-.154-2.085,3.261,3.261,0,0,0-3.028-2.041H15.841Z" transform="translate(0.529 7.022)" fill="#fff"/>
                                    <path id="Vector-2" data-name="Vector" d="M12.135,38.4H1.646a1.646,1.646,0,1,1,0-3.292H12.135a3.231,3.231,0,0,0,3.2-3.467L13.167,1.758A1.644,1.644,0,0,1,14.681,0a1.659,1.659,0,0,1,1.756,1.514L18.609,31.4a6.544,6.544,0,0,1-1.734,4.916A6.38,6.38,0,0,1,12.135,38.4Z" transform="translate(33.465 11.519)" fill="#fff"/>
                                    <path id="Vector-3" data-name="Vector" d="M1.65,12.776a2.418,2.418,0,0,1-.395-.044,1.648,1.648,0,0,1-1.207-2l2.282-9.48a1.65,1.65,0,1,1,3.2.79l-2.282,9.48A1.642,1.642,0,0,1,1.65,12.776Z" transform="translate(19.591 2.87)" fill="#fff"/>
                                    <path id="Vector-4" data-name="Vector" d="M1.656,12.826a.966.966,0,0,1-.351-.044A1.68,1.68,0,0,1,.032,10.829L2.094,1.305a1.648,1.648,0,0,1,3.226.68L3.258,11.51A1.62,1.62,0,0,1,1.656,12.826Z" transform="translate(34.289 2.842)" fill="#fff"/>
                                    <path id="Vector-5" data-name="Vector" d="M19.2,3.292H1.646A1.658,1.658,0,0,1,0,1.646,1.658,1.658,0,0,1,1.646,0H19.2a1.658,1.658,0,0,1,1.646,1.646A1.658,1.658,0,0,1,19.2,3.292Z" transform="translate(15.252 24.687)" fill="#fff"/>
                                    <path id="Vector-6" data-name="Vector" d="M19.2,3.292H1.646A1.658,1.658,0,0,1,0,1.646,1.658,1.658,0,0,1,1.646,0H19.2a1.658,1.658,0,0,1,1.646,1.646A1.658,1.658,0,0,1,19.2,3.292Z" transform="translate(13.057 33.465)" fill="#fff"/>
                                    <path id="Vector-7" data-name="Vector" d="M0,0H52.667V52.667H0Z" fill="none" opacity="0"/>
                                    </svg>
                            </small>
                            <span class="circle"></span>
                        </div>
                        <h2 class="fs-24 text-white font-w600 mb-0">40</h2>
                        <span class="fs-16">Training Plan</span>
                    </div>
                </div>
            </div>
            <div class="col-xl col-md-4 col-sm-6">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="d-inline-block mb-4 ml--12 position-relative donut-chart-sale">
                            <span class="donut1" data-peity='{ "fill": ["rgb(28, 28, 30)", "rgba(18, 18, 18, 1)"],   "innerRadius": 45, "radius": 10}'>4/8</span>
                            <small class="text-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42">
                                    <g id="vuesax_outline_diagram" data-name="vuesax/outline/diagram" transform="translate(-300 -252)">
                                        <g id="diagram" transform="translate(300 252)">
                                        <path id="Vector" d="M36.313,37.625H6.563A6.565,6.565,0,0,1,0,31.063V1.313A1.322,1.322,0,0,1,1.313,0,1.322,1.322,0,0,1,2.625,1.313v29.75A3.943,3.943,0,0,0,6.563,35h29.75a1.313,1.313,0,0,1,0,2.625Z" transform="translate(2.188 2.188)" fill="#fff"/>
                                        <path id="Vector-2" data-name="Vector" d="M1.312,20.125a1.312,1.312,0,0,1-1-2.17l8.033-9.38a4.843,4.843,0,0,1,3.465-1.68,4.892,4.892,0,0,1,3.587,1.4l1.662,1.663a2.259,2.259,0,0,0,1.628.647,2.139,2.139,0,0,0,1.575-.77L28.3.455A1.315,1.315,0,1,1,30.292,2.17L22.26,11.55a4.843,4.843,0,0,1-3.465,1.68,4.892,4.892,0,0,1-3.587-1.4l-1.645-1.663a2.189,2.189,0,0,0-1.628-.647,2.139,2.139,0,0,0-1.575.77L2.327,19.67A1.4,1.4,0,0,1,1.312,20.125Z" transform="translate(7.438 10.938)" fill="#fff"/>
                                        <path id="Vector-3" data-name="Vector" d="M0,0H42V42H0Z" fill="none" opacity="0"/>
                                        </g>
                                    </g>
                                    </svg>
                            </small>
                            <span class="circle"></span>
                        </div>
                        <h2 class="fs-24 text-white font-w600 mb-0">40%</h2>
                        <span class="fs-16">Total Sale</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-xxl-6">
                <div class="card">
                    <div class="card-header flex-wrap pb-0 border-0">
                        <div class="mr-auto pr-3 mb-2">
                            <h4 class="text-white fs-20">Total Sale</h4>

                        </div>



                        <div class="dropdown mt-sm-0 mt-3 mb-0">
                            <button type="button" class="btn rounded border border-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Weekly
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);">Link 1</a>
                                <a class="dropdown-item" href="javascript:void(0);">Link 2</a>
                                <a class="dropdown-item" href="javascript:void(0);">Link 3</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-3">
                        <div id="chartBar"></div>
                    </div>
                </div>
            </div>


            <div class="col-xl-6 col-xxl-6">
                <div class="card">
                    <div class="card-header d-sm-flex d-block pb-0 border-0">
                        <div class="mr-auto pr-3">
                            <h4 class="text-white fs-20">Total Users</h4>

                        </div>
                        <div class="dropdown mt-sm-0 mt-3">
                            <button type="button" class="btn rounded border border-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Weekly
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);">Link 1</a>
                                <a class="dropdown-item" href="javascript:void(0);">Link 2</a>
                                <a class="dropdown-item" href="javascript:void(0);">Link 3</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="chartTimeline"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->

</x-layout>