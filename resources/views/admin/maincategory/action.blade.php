<div class="d-flex">
    <a href="{{ route('admin_maincategory_edit', ['id' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
    <a href="{{ route('admin_maincategory_delete', ['id' => $id]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to delete the category?')"><i class="fa fa-trash"></i></a>
</div>