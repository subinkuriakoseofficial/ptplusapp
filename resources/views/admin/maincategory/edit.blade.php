<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Edit Main Category</h4>
                        <a href="{{ route('admin_maincategory_list') }}" class="btn btn-rounded btn-dark"> List Main Categories</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                        <form action="{{ route('admin_maincategory_update') }}" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Name (English)</label>
                                    <input type="text" name="name_en" placeholder="Name" class="form-control" value="{{ old('name_en', $category->name_en) }}">
                                    @error('name_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Name (Arabic)</label>
                                    <input type="text" name="name_ar" placeholder="Name" class="form-control" value="{{ old('name_ar', $category->name_ar) }}">
                                    @error('name_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>                            

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div>
                                        <img src="{{ Storage::disk('public')->url($category->thumbnail_img) }}" style="max-width: 300px; max-height: 300px;" />
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    <input type="hidden" name="Xthumbnail" value="{{ $category->thumbnail_img }}">
                                </div>

                            </div> 
                            <input type="hidden" name="id" value="{{ $category->idEnc }}">
                            <!-- @error('slug')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror -->

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>