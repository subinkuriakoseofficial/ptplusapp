<div class="d-flex">
    <a href="{{ route('admin_product_edit', ['id' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1" target="_blank"><i class="fa fa-pencil"></i></a>
    <a href="{{ route('admin_user_vendor_product_delete', ['id' => $id, 'vendorId' => $vendorId]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to remove this product?')" title="Delete"><i class="fa fa-trash"></i></a>
</div>