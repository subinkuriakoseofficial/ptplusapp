<x-admin.layout>

    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
    
                <div class="col-12">
                    <div class="card">
                        <div class="card-header justify-content-between">
                            <h4 class="card-title">Edit Vendor</h4>
                            <a href="{{ route('admin_user_vendor_list') }}" class="btn btn-rounded btn-dark"> List Vendors</a>
                        </div>
                        @if (session('messageSuccess'))
                            <div class="alert alert-success">
                                {{ session('messageSuccess') }}
                            </div>
                        @endif
                        @if (session('messageError'))
                            <div class="alert alert-warning">
                                {{ session('messageError') }}
                            </div>
                        @endif
                        <div class="card-body">
    
                            <form action="{{ route('admin_user_vendor_update') }}" method="post" enctype="multipart/form-data" id="userForm">
                                @csrf
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Name</label>
                                        <input type="text" name="name" placeholder="Name" class="form-control" value="{{ old('name', $user->name) }}">
                                        @error('name')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email (Username)</label>
                                        <input type="email" name="email" readonly placeholder="Email" class="form-control" value="{{ old('email', $user->email) }}">
                                        @error('email')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>  
                                
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Phone Code</label>
                                        <select name="phone_code" class="form-control">
                                            <option value="">Select country code</option>
                                            @foreach ($countryCodes as $row)
                                                <option value="{{ $row->country_code }}" {{ old('phone_code', $user->phone_code) == $row->country_code ? 'selected' : '' }}>+{{ $row->country_code }}</option>
                                            @endforeach
                                        </select>
                                        @error('phone_code')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Phone</label>
                                        <input type="text" name="phone" placeholder="Mobile Phone Number" class="form-control" value="{{ old('phone', $user->phone) }}">
                                        @error('phone')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div> 
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Date of Birth</label>
                                        <input type="date" name="dob" placeholder="dob" class="form-control" value="{{ old('dob', $user->dob) }}">
                                        @error('dob')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Country</label>
                                        <select name="country" class="form-control">
                                            <option value="">Select country</option>
                                            @foreach ($countryCodes as $row)
                                                <option value="{{ $row->short_name_en }}" {{ old('country', $user->country) == $row->short_name_en ? 'selected' : '' }}>{{ $row->short_name_en }}</option>
                                            @endforeach
                                        </select>
                                        @error('country')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>  
                                 
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Profile Picture</label>
                                        <div>
                                            <img src="{{ Storage::disk('public')->url($user->profile_picture) }}" style="max-width: 300px; max-height: 300px;" />
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" name="image" class="custom-file-input">
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                        @error('image')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                        <input type="hidden" name="Ximage" value="{{ $user->profile_picture }}">
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{ $user->idEnc }}">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
    
                        </div>
                    </div>
                </div>
    
    
    
    
    
            </div>
        </div>
    </div>
    
    </x-layout>