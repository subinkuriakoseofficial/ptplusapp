<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainingplans/addPlans.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Add Plan</h4>
                        <a href="{{ route('admin_training_plan_list') }}" class="btn btn-rounded btn-dark"> List All</a>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('admin_training_plan_add_data') }}" method="post" enctype="multipart/form-data" id="categoryForm">
                            @csrf
                            @if($trainerId == '' && $centerId == '')
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Behalf of: </label>
                                        <label for="behalf_of_trainer">Trainer</label>
                                        <input type="radio" name="behalf_of" value="trainer" checked class="behalf_of" id="behalf_of_trainer">
                                        <label for="behalf_of_center">Center</label>
                                        <input type="radio" name="behalf_of" value="center" class="behalf_of" id="behalf_of_center" {{ old('behalf_of') == 'center' ? 'checked' : '' }}>
                                        @error('behalf_of')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>

                                </div>
                            @elseif($trainerId != '')
                                <input type="hidden" name="behalf_of" value="trainer">
                                <input type="hidden" name="trainer" value="{{ $trainerId }}">
                            @elseif($centerId != '')
                                <input type="hidden" name="behalf_of" value="center">
                                <input type="hidden" name="center" value="{{ $centerId }}">
                            @endif

                            <div class="form-row">
                                @if($trainerId == '' && $centerId == '')
                                    <div class="form-group col-md-6" id="sectionTrainer" style="{{ old('behalf_of') == 'center' ? 'display:none' : '' }}">
                                        <label>Trainer</label>
                                        <select name="trainer" class="form-control" id="trainer">
                                            <option value="">Choose Trainer</option>
                                            @foreach($trainers as $row)
                                                <option value="{{ $row->id }}" {{ old('trainer') == $row->id ? 'selected' : '' }}>{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('trainer')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6" id="sectionCenter" style="{{ old('behalf_of') != 'center' ? 'display:none' : '' }}">
                                        <label>Center</label>
                                        <select name="center" class="form-control" id="center">
                                            <option value="">Choose Center</option>
                                            @foreach($centers as $row)
                                                <option value="{{ $row->id }}" {{ old('center') == $row->id ? 'selected' : '' }}>{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('center')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                @endif

                                <div class="form-group col-md-6" id="sectionCenter">
                                    <label>Parent Plan</label>
                                    <select name="parent_plan" class="form-control" id="center">
                                        <option value="">Choose Plan</option>
                                        @foreach($parentPlans as $row)
                                            <option value="{{ $row->id }}" {{ old('parent_plan') == $row->id ? 'selected' : '' }}>{{ $row->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('parent_plan')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Title (English)</label>
                                    <input type="text" name="title_en" placeholder="Title" class="form-control" value="{{ old('title_en') }}">
                                    @error('title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Title (Arabic)</label>
                                    <input type="text" name="title_ar" placeholder="Title" class="form-control" value="{{ old('title_ar') }}">
                                    @error('title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Sub Title (English)</label>
                                    <input type="text" name="sub_title_en" placeholder="Sub Title" class="form-control" value="{{ old('sub_title_en') }}">
                                    @error('sub_title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Sub Title (Arabic)</label>
                                    <input type="text" name="sub_title_ar" placeholder="Sub Title" class="form-control" value="{{ old('sub_title_ar') }}">
                                    @error('sub_title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Price</label>
                                    <input type="number" min="0" name="price" placeholder="Price" class="form-control" value="{{ old('price') }}">
                                    @error('price')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6" id="sectionCenter">
                                    <label>Currency</label>
                                    <select name="currency" class="form-control" id="center">
                                        <option value="">Choose Currency</option>
                                        @foreach($currencies as $row)
                                            <option value="{{ $row->id }}" {{ old('currency') == $row->id ? 'selected' : '' }}>{{ $row->code }}</option>
                                        @endforeach
                                    </select>
                                    @error('currency')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Description (English)</label>
                                    <textarea name="description_en" class="form-control">{{ old('description_en') }}</textarea>
                                    @error('description_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Description (Arabic)</label>
                                    <textarea name="description_ar" class="form-control">{{ old('description_ar') }}</textarea>
                                    @error('description_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Number of trainers</label>
                                    <input type="number" min="1" name="num_of_trainers" placeholder="Number of trainers" class="form-control" value="{{ old('num_of_trainers') }}">
                                    @error('num_of_trainers')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                            <!-- @error('slug')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror -->
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>