<div class="d-flex">
    <a href="{{ route('admin_training_plan_banner_list', ['planId' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1" title="Banner"><i class="fa fa-image"></i></a>
    <a href="{{ route('admin_training_plan_edit', ['id' => $id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
    <a href="{{ route('admin_training_plan_delete', ['id' => $id]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to delete the plan?')"><i class="fa fa-trash"></i></a>
</div>