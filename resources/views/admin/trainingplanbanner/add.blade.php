<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainingplans/addPlans.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Add Banner - {{ $plan->title }}</h4>
                        <a href="{{ route('admin_training_plan_banner_list', ['planId' => $plan->idEnc]) }}" class="btn btn-rounded btn-dark"> List All</a>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('admin_training_plan_banner_add_data') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="images[]" multiple class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('images')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>
                            <input type="hidden" name="plan_id" value="{{ $plan->idEnc }}" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>