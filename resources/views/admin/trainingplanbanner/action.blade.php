<div class="d-flex">
    <a href="{{ route('admin_training_plan_banner_delete', ['id' => $id, 'planId' => $planId]) }}" class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Want to delete the plan image?')"><i class="fa fa-trash"></i></a>
</div>