<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/trainingplans/editPlans.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title">Edit  Plan</h4>
                        <a href="{{ route('admin_training_plan_list') }}" class="btn btn-rounded btn-dark"> List  Plans</a>

                    </div>
                    @if (session('messageSuccess'))
                        <div class="alert alert-success">
                            {{ session('messageSuccess') }}
                        </div>
                    @endif
                    @if (session('messageError'))
                        <div class="alert alert-warning">
                            {{ session('messageError') }}
                        </div>
                    @endif
                    <div class="card-body">

                    <form action="{{ route('admin_training_plan_update') }}" method="post" enctype="multipart/form-data" id="planForm">
                            @csrf
                            @php 
                                $displayTrainer = 'block'; 
                                $displayCenter = 'block';
                                
                                if(old('behalf_of') != NULL):
                                    if(old('behalf_of') == 'trainer'):
                                        $displayTrainer = 'block';
                                        $displayCenter = 'none';
                                    else:
                                        $displayTrainer = 'none';
                                        $displayCenter = 'block';
                                    endif;
                                else:
                                    if($plan->user_id == NULL):
                                        $displayTrainer = 'none';
                                        $displayCenter = 'block';
                                    else:
                                        $displayTrainer = 'block';
                                        $displayCenter = 'none';
                                    endif;
                                endif;
                            @endphp
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Behalf of: </label>
                                    <label for="behalf_of_trainer">Trainer</label>
                                    <input type="radio" name="behalf_of" value="trainer" checked class="behalf_of" id="behalf_of_trainer" @if($displayTrainer=='block') checked @endif>
                                    <label for="behalf_of_center">Center</label>
                                    <input type="radio" name="behalf_of" value="center" class="behalf_of" id="behalf_of_center" @if($displayCenter=='block') checked @endif>
                                    @error('behalf_of')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6" id="sectionTrainer" style="@if($displayTrainer == 'none') display:none @endif">
                                    <label>Trainer</label>
                                    <select name="trainer" class="form-control" id="trainer">
                                        <option value="">Choose Trainer</option>
                                        @foreach($trainers as $row)
                                            <option value="{{ $row->id }}" {{ old('trainer', $plan->user_id) == $row->id ? 'selected' : '' }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('trainer')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                
                                <div class="form-group col-md-6" id="sectionCenter" style="@if($displayCenter == 'none') display:none @endif">
                                    <label>Center</label>
                                    <select name="center" class="form-control" id="center">
                                        <option value="">Choose Center</option>
                                        @foreach($centers as $row)
                                            <option value="{{ $row->id }}" {{ old('center', $plan->center_id) == $row->id ? 'selected' : '' }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('center')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>

                                <div class="form-group col-md-6" id="sectionCenter">
                                    <label>Parent Plan</label>
                                    <select name="parent_plan" class="form-control" id="center">
                                        <option value="">Choose Plan</option>
                                        @foreach($parentPlans as $row)
                                            <option value="{{ $row->id }}" {{ old('parent_plan', $plan->parent_id) == $row->id ? 'selected' : '' }}>{{ $row->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('parent_plan')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Title (English)</label>
                                    <input type="text" name="title_en" placeholder="Title" class="form-control" value="{{ old('title_en', $plan->title_en) }}">
                                    @error('title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Title (Arabic)</label>
                                    <input type="text" name="title_ar" placeholder="Title" class="form-control" value="{{ old('title_ar', $plan->title_ar) }}">
                                    @error('title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Sub Title (English)</label>
                                    <input type="text" name="sub_title_en" placeholder="Sub Title" class="form-control" value="{{ old('sub_title_en', $plan->sub_title_en) }}">
                                    @error('sub_title_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Sub Title (Arabic)</label>
                                    <input type="text" name="sub_title_ar" placeholder="Sub Title" class="form-control" value="{{ old('sub_title_ar', $plan->sub_title_ar) }}">
                                    @error('sub_title_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Price</label>
                                    <input type="number" min="0" name="price" placeholder="Price" class="form-control" value="{{ old('price', $plan->price) }}">
                                    @error('price')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6" id="sectionCenter">
                                    <label>Currency</label>
                                    <select name="currency" class="form-control" id="center">
                                        <option value="">Choose Currency</option>
                                        @foreach($currencies as $row)
                                            <option value="{{ $row->id }}" {{ old('currency', $plan->currency_id) == $row->id ? 'selected' : '' }}>{{ $row->code }}</option>
                                        @endforeach
                                    </select>
                                    @error('currency')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Description (English)</label>
                                    <textarea name="description_en" class="form-control">{{ old('description_en', $plan->description_en) }}</textarea>
                                    @error('description_en')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Description (Arabic)</label>
                                    <textarea name="description_ar" class="form-control">{{ old('description_ar', $plan->description_ar) }}</textarea>
                                    @error('description_ar')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Number of trainers</label>
                                    <input type="number" min="1" name="num_of_trainers" placeholder="Number of trainers" class="form-control" value="{{ old('num_of_trainers', $plan->num_of_trainers) }}">
                                    @error('num_of_trainers')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <img src="{{ Storage::disk('public')->url($plan->thumbnail_img) }}" style="max-width: 300px; max-height: 300px;" />
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="thumbnail" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    @error('thumbnail')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    <input type="hidden" name="Xthumbnail" value="{{ $plan->thumbnail_img }}">
                                </div>
                            </div>
                            <!-- @error('slug')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror -->
                            <input type="hidden" name="id" value="{{ $plan->idEnc }}">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

</x-layout>