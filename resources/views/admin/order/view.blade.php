<x-admin.layout>

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">

        <!-- row -->
        
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-tab">
                            <div class="custom-tab-1">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a href="#about-me" data-toggle="tab" class="nav-link active show">Order Details</a>
                                    </li>
                                    <li class="nav-item"><a href="#my-posts" data-toggle="tab" class="nav-link ">Shipping Address</a>
                                    </li>
                                    <li class="nav-item"><a href="#profile-settings" data-toggle="tab" class="nav-link">Order Items</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="about-me" class="tab-pane fade active show">

                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Order Number
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6 mb-1">
                                                    <span>{{ $order->order_number }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Delivery Date
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->delivery_date }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Delivery Type
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ ucfirst($order->delivery_type) }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Sub Total
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->sub_total . ' ' . $order->currency }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Delivery Charge
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->delivery_charge }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Total Amount
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->total_amount . ' ' . $order->currency }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Ordered On
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->created_at }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Order Status
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->status }}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="my-posts" class="tab-pane fade ">
                                        <div class="my-post-content pt-3">

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Name
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->name }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Email
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->email }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Phone
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->phone_code . $order->shippingAddress->phone }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Country
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->country }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">State
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->state }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Pincode
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->pincode }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Street
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->street }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Area
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->area }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Block
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->block }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Building
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $order->shippingAddress->building }}</span>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Address Type
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ ucfirst($order->shippingAddress->type) }}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="profile-settings" class="tab-pane fade">
                                        <div class="table-responsive">
                                            <table class="table table-responsive-md">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Description</th>
                                                        <th>Unit Price</th>
                                                        <th>Quantity</th>
                                                        <th>Current Stock</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order->item as $key => $item)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td><img src="{{ Storage::disk('public')->url($item->product->thumbnail_img) }}" width="100" height="100" /></td>
                                                            <td>{{ $item->product->name }}</td>
                                                            <td>{{ $item->product->description }}</td>
                                                            <td>{{ $item->unit_price . ' ' . $item->product->currency->code }}</td>
                                                            <td>{{ $item->quantity }}</td>
                                                            <td>{{ $item->product->current_stock }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="replyModal">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Post Reply</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <textarea class="form-control" rows="4">Message</textarea>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Reply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layout>