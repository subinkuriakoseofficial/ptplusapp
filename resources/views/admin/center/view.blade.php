<x-admin.layout>

@push('script')
    <script src="{{ asset('assets/admin/js/centers/viewCenters.js') }}"></script>
@endpush

<div class="content-body">
    <!-- row -->
    <div class="container-fluid">

        <!-- row -->
        @if (session('messageSuccess'))
            <div class="alert alert-success">
                {{ session('messageSuccess') }}
            </div>
        @endif
        @if (session('messageError'))
            <div class="alert alert-warning">
                {{ session('messageError') }}
            </div>
        @endif
        
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-tab">
                            <div class="custom-tab-1">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a href="#profile" data-toggle="tab" class="nav-link active show">Profile</a></li>
                                    <li class="nav-item"><a href="#banners" data-toggle="tab" class="nav-link ">Banners</a></li>
                                    <li class="nav-item"><a href="#categories" data-toggle="tab" class="nav-link">Categories</a></li>
                                    <li class="nav-item"><a href="#specialities" data-toggle="tab" class="nav-link">Specialities</a></li>
                                    <li class="nav-item"><a href="#plans" data-toggle="tab" class="nav-link">Plans</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="profile" class="tab-pane fade active show">

                                        <div class="profile-personal-info pt-4">
                                            
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Center Type
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6 mb-1">
                                                    <span>{{ $centerTypesStr }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Name
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6 mb-1">
                                                    <span>{{ $user->name }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Email
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->email }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Phone
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->phone_code . ' ' . $user->phone }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Country
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->country }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Registered On
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $user->created_at }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Rating
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->rating }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Phone
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->phone }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">About
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->about }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Address
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->address }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Latitude
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->latitude }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Longitude
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ @$user->centerDetails->longitude }}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">Password
                                                        <span class="d-none d-sm-block pull-right">:</span>
                                                    </h5>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-6">
                                                    <span>{{ $pswdNonHashed }}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="banners" class="tab-pane fade ">
                                        <div class="my-post-content pt-3">


                                            <div class="table-responsive">
                                                <table class="display min-w850" id="banners_list_datatable" data-url="{{ route('admin_user_center_banner_table') }}" data-id="{{ $user->idEnc }}">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Image</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                            

                                        </div>
                                    </div>

                                    <div id="categories" class="tab-pane fade">
                                        <h3>Main Categories: </h3>
                                        @foreach($mainCategories as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->mainCategory->name }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <h3>Categories: </h3>
                                        @foreach($categories as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->category->name }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div id="specialities" class="tab-pane fade">
                                        @foreach($specialities as $row)
                                        <div class="profile-personal-info pt-4">
                                            <div class="row mb-4">
                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                    <h5 class="f-w-500">{{ $row->speciality }} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div id="plans" class="tab-pane fade">
                                        <div class="my-post-content pt-3">

                                            <div class="table-responsive">
                                                <table class="display min-w850" id="plans_list_datatable" data-url="{{ route('admin_user_center_plan_table') }}" data-id="{{ $user->idEnc }}">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Image</th>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                            <th>Price</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/tabs-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layout>