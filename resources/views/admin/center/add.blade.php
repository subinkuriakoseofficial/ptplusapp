<x-admin.layout>

    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
    
                <div class="col-12">
                    <div class="card">
                        <div class="card-header justify-content-between">
                            <h4 class="card-title">Add Center</h4>
                            <a href="{{ route('admin_user_center_list') }}" class="btn btn-rounded btn-dark"> List All</a>
                        </div>
                        <div class="card-body">
    
                            <form action="{{ route('admin_user_center_add_data') }}" method="post" enctype="multipart/form-data" id="userForm">
                                @csrf
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Name</label>
                                        <input type="text" name="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                                        @error('name')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email (Username)</label>
                                        <input type="email" name="email" placeholder="Email" class="form-control" value="{{ old('email') }}">
                                        @error('email')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>  
                                
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Phone Code</label>
                                        <select name="phone_code" class="form-control">
                                            <option value="">Select country code</option>
                                            @foreach ($countryCodes as $row)
                                                <option value="{{ $row->country_code }}" {{ old('phone_code') == $row->country_code ? 'selected' : '' }}>+{{ $row->country_code }}</option>
                                            @endforeach
                                        </select>
                                        @error('phone_code')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Phone</label>
                                        <input type="text" name="phone" placeholder="Mobile Phone Number" class="form-control" value="{{ old('phone') }}">
                                        @error('phone')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div> 
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Date of Birth</label>
                                        <input type="date" name="dob" placeholder="dob" class="form-control" value="{{ old('dob') }}">
                                        @error('dob')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Country</label>
                                        <select name="country" class="form-control">
                                            <option value="">Select country</option>
                                            @foreach ($countryCodes as $row)
                                                <option value="{{ $row->short_name_en }}" {{ old('country') == $row->short_name_en ? 'selected' : '' }}>{{ $row->short_name_en }}</option>
                                            @endforeach
                                        </select>
                                        @error('country')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>  
    
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Password</label>
                                        <input type="text" name="password" placeholder="password" class="form-control" value="{{ old('password') }}">
                                        @error('password')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Gender: </label>
                                        <label for="male">Male</label>
                                        <input type="radio" name="gender" value="male" checked class="" id="male">
                                        <label for="female">Female</label>
                                        <input type="radio" name="gender" value="female" class="" id="female" {{ old('gender') == 'female' ? 'checked' : '' }}>
                                        @error('gender')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>  
                                 
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Profile Picture</label>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" name="image" class="custom-file-input">
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                        @error('image')<small class="form-text red-text text-danger">{{ $message }}</small>@enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
    
                        </div>
                    </div>
                </div>
    
    
    
    
    
            </div>
        </div>
    </div>
    
    </x-layout>