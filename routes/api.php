<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\CommonController;
use App\Http\Controllers\Api\StoreController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\CartGuestController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\OrderGuestController;
use App\Http\Controllers\Api\TrainerController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\BookingGuestController;
use App\Http\Controllers\Api\CenterController;
use App\Http\Controllers\Api\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(CommonController::class)->group(function () {
    Route::get('/countries', 'getCountries');
    Route::get('/cities', 'getCities');
    Route::get('/blocks', 'getBlocks');
    Route::get('/banners', 'getBanners');
});

// Trainer
Route::get('/activities', [CommonController::class, 'getActivities']);
Route::get('/main-categories', [CommonController::class, 'getMainCategories']);
Route::controller(TrainerController::class)->prefix('trainer')->group(function () {
    Route::get('/list', 'getTrainer');
    Route::get('/list/all', 'getTrainerAll');
    Route::get('/categories', 'getCategories');
    Route::get('/subcategories/{cat_id}', 'getSubCategories');
    Route::get('/filter-service-types', 'getFilterServiceTypes');
    Route::get('/filter-categories', 'getFilterCategories');
    Route::get('/filter-categories-test', 'getFilterCategoriesTest');
    Route::post('/list/byfilters', 'getTrainerByFilters');
    Route::post('/detail', 'getTrainerDetail');
    Route::post('/plans', 'getPlans');
    Route::post('/gallery', 'getGallery');
    Route::post('/fav-products', 'getFavProducts');
    Route::get('/plan-detail/{id}', 'getPlanDetail');
    Route::post('/time-slots', 'getTrainerTimeSlotsAvailable');

    Route::prefix('wishlist')->middleware('auth:sanctum')->group(function () {
        Route::get('/get', 'getWishlist');
        Route::post('/add', 'addToWishlist');
        Route::post('/delete', 'deleteWishlist');
    });

    Route::prefix('plan')->middleware('auth:sanctum')->group(function () {
        Route::get('/wishlist/get', [CommonController::class, 'getWishlist']);
        Route::post('/wishlist/add', [CommonController::class, 'addToWishlist']);
        Route::post('/wishlist/delete', [CommonController::class, 'deleteWishlist']);
    });

    Route::prefix('booking')->group(function () {
        Route::controller(BookingController::class)->middleware('auth:sanctum')->group(function () {
            Route::post('/make', 'create');
            Route::get('/my-bookings', 'myBookings');
            Route::post('/detail', 'bookingDetail');
            Route::post('/cancel', 'cancelBooking');
        });
        Route::controller(BookingGuestController::class)->prefix('guest')->group(function () {
            Route::post('/make', 'create');
        });
    });


});

Route::controller(AuthController::class)->prefix('user')->group(function () {
    Route::post('/register', 'createUser');
    Route::post('/login', 'login');
});

Route::controller(UserController::class)->middleware('auth:sanctum')->prefix('user')->group(function () {
    Route::post('/address/add', 'addAddress');
    Route::get('/address/get', 'getAddress');
    Route::post('/profile/dp/update', 'updateDp');
    Route::post('/profile/update', 'updateProfile');
    Route::get('/profile/get', 'getProfile');
});

Route::controller(StoreController::class)->prefix('store')->name('store_')->group(function () {
    Route::get('/banners', 'getBanners');
    Route::get('/categories', 'getCategories');
    Route::get('/subcategories/{cat_id}', 'getSubCategories');
    Route::get('/best-sellers', 'getBestSellers');
    Route::get('/best-sellers/all', 'getBestSellersAll');
    Route::get('/new-arrivals', 'getNewArrivals');
    Route::get('/new-arrivals/all', 'getNewArrivalsAll');
    Route::get('/products-by-category/{cat_id}', 'getProductsByCategory');
    Route::get('/product-detail/{pro_id}', 'getProductDetail');
    Route::prefix('wishlist')->middleware('auth:sanctum')->group(function () {
        Route::get('/get', 'getWishlist');
        Route::post('/add', 'addToWishlist');
        Route::post('/delete', 'deleteWishlist');
    });

    Route::prefix('cart')->group(function () {
        Route::controller(CartController::class)->middleware('auth:sanctum')->group(function () {
            Route::post('/add-item', 'addToCart');
            Route::get('/get', 'getCart');
            Route::post('/update', 'updateCart');
        });
        Route::controller(CartGuestController::class)->prefix('guest')->group(function () {
            Route::post('/add-item', 'addToCart');
            Route::get('/get', 'getCart');
            Route::post('/update', 'updateCart');
        });
    });

    Route::prefix('order')->group(function () {
        Route::controller(OrderController::class)->middleware('auth:sanctum')->group(function () {
            Route::post('/make', 'create');
            Route::post('/make/single-product', 'createSingleProduct');
            Route::get('/my-orders', 'myOrders');
            Route::post('/detail', 'orderItemDetail');
            Route::post('transactions/detail', 'transactionDetail');
        });
        Route::controller(OrderGuestController::class)->prefix('guest')->group(function () {
            Route::post('/make', 'create');
            Route::post('/make/single-product', 'createSingleProduct');
            /*Route::get('/my-orders', 'myOrders');
            Route::post('/detail', 'orderItemDetail');*/
        });
    });

    Route::prefix('checkout')->group(function () {
        Route::controller(OrderController::class)->middleware('auth:sanctum')->group(function () {
            Route::post('/get-deliverycharge/cart', 'getDeliveryCharge');
            Route::post('/get-deliverycharge/single-product', 'getDeliveryChargeSingleProduct');
        });
        Route::controller(OrderGuestController::class)->prefix('guest')->group(function () {
            Route::post('/get-deliverycharge/cart', 'getDeliveryCharge');
            Route::post('/get-deliverycharge/single-product', 'getDeliveryChargeSingleProduct');
        });
    });

    Route::controller(PaymentController::class)->prefix('payment')->name('payment_')->group(function () {
        Route::get('/success', 'paymentSuccess')->name('success');
        Route::get('/error', 'paymentError')->name('error');
    });

});

// Centers
Route::controller(CenterController::class)->prefix('center')->group(function () {
    Route::get('/banners', 'getBanners');
    Route::get('/categories', 'getCategories');
    Route::get('/subcategories/{cat_id}', 'getSubCategories');
    Route::post('/list', 'getCentersByFilter');
    Route::post('/detail', 'getCenterDetail');
    Route::post('/trainers/list', 'getTrainers');
    Route::post('/plans/list', 'getPlans');
    Route::post('/products/list', 'getProducts');
    Route::post('/gallery/list', 'getGallery');
    Route::prefix('wishlist')->middleware('auth:sanctum')->group(function () {
        Route::get('/get', 'getWishlist');
        Route::post('/add', 'addToWishlist');
        Route::post('/delete', 'deleteWishlist');
    });
});