<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\MainCategoryController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TrainingPlanController;
use App\Http\Controllers\Admin\TrainingPlanBannerController;
use App\Http\Controllers\Admin\ActivityController;
use App\Http\Controllers\Admin\HomeBannerController;
use App\Http\Controllers\Admin\StoreBannerController;
use App\Http\Controllers\Admin\CenterBannerController;
use App\Http\Controllers\Admin\TrainerController;
use App\Http\Controllers\Admin\CenterController;
use App\Http\Controllers\Admin\VendorController;
use App\Http\Controllers\Admin\CustomerController;


Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'index')->name('login');
    Route::get('/login', 'index');
    Route::post('/authenticate', 'authenticate')->name('validate_login');
    Route::get('/logout', 'logout')->name('logout');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth')->name('dashboard');

Route::controller(MainCategoryController::class)->prefix('main-category')->name('maincategory_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'listData')->name('table');
    Route::get('/edit/{id}', 'edit')->name('edit');
    Route::post('/update', 'update')->name('update');
    Route::get('/add', 'create')->name('add');
    Route::post('/add', 'store')->name('add_data');
    Route::get('/delete/{id}', 'destroy')->name('delete');
});

Route::controller(CategoryController::class)->prefix('category')->name('category_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'listData')->name('table');
    Route::get('/edit/{id}', 'edit')->name('edit');
    Route::post('/update', 'update')->name('update');
    Route::get('/add', 'create')->name('add');
    Route::post('/add', 'store')->name('add_data');
    Route::get('/delete/{id}', 'destroy')->name('delete');
    Route::get('/list/bymaincategory', 'ajaxListByMainCategory')->name('ajax_list_by_main_category');
});

Route::controller(UserController::class)->prefix('users')->name('user_')->middleware('auth')->group(function () {
    // Route::get('/list', 'index')->name('list');
    // Route::get('/table', 'listData')->name('table');
    Route::controller(TrainerController::class)->prefix('trainer')->name('trainer_')->middleware('auth')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/view/{id}', 'show')->name('view');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
        Route::get('favproduct/table', 'favProductlistData')->name('favproduct_table');
        Route::get('favproduct/add/table', 'createfavProductListdata')->name('favproduct_add_table');
        Route::get('favproduct/update', 'updateFavProduct')->name('favproduct_update');
        Route::get('favproduct/add/{trainerId}', 'createfavProduct')->name('favproduct_add');
        Route::get('favproduct/delete/{id}/{trainerId}', 'removeFavProduct')->name('favproduct_delete');
        Route::get('activity/table', 'activityListData')->name('activity_table');
        Route::get('activity/update', 'updateRegActivity')->name('activity_update');
        Route::get('activity/add/{trainerId}', 'createActivity')->name('activity_add');
        Route::get('activity/delete/{id}/{trainerId}', 'removeActivity')->name('activity_delete');
        Route::get('plan/table', 'planlistData')->name('plan_table');
        Route::get('cateogry/update', 'updateRegCategory')->name('category_update');
        Route::get('cateogry/add/{trainerId}', 'createRegCategory')->name('category_add');
        Route::get('speciality/table', 'specialityListData')->name('speciality_table');
        Route::get('speciality/delete/{id}/{trainerId}', 'removeSpeciality')->name('speciality_delete');
        Route::get('speciality/add/{trainerId}', 'createSpeciality')->name('speciality_add');
        Route::post('speciality/add', 'storeSpeciality')->name('speciality_add_data');
        Route::get('servicetype/add/{trainerId}', 'createServiceType')->name('servicetype_add');
        Route::get('servicetype/update', 'updateRegServiceType')->name('servicetype_update');
    });
    Route::controller(CenterController::class)->prefix('center')->name('center_')->middleware('auth')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/view/{id}', 'show')->name('view');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
        Route::get('banner/table', 'bannerlistData')->name('banner_table');
        Route::get('banner/delete/{id}/{centerId}', 'deleteBanner')->name('banner_delete');
        Route::get('plan/table', 'planlistData')->name('plan_table');
    });
    Route::controller(VendorController::class)->prefix('vendor')->name('vendor_')->middleware('auth')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/view/{id}', 'show')->name('view');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
        Route::get('product/table', 'productlistData')->name('product_table');
        Route::get('product/delete/{id}/{vendorId}', 'deleteProduct')->name('product_delete');
    });
    Route::controller(CustomerController::class)->prefix('customer')->name('customer_')->middleware('auth')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/view/{id}', 'show')->name('view');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
    });
});

Route::controller(ProductController::class)->prefix('product')->name('product_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'productListData')->name('table');
    Route::get('/add', 'create')->name('add');
    Route::post('/add', 'store')->name('add_data');
    Route::get('/edit/{id}', 'edit')->name('edit');
    Route::post('/update', 'update')->name('update');
    Route::get('/delete_gallery_image/{id}/{product_id}', 'deleteGalleryImage')->name('delete_gallery_image');
    Route::get('/delete/{id}', 'destroy')->name('delete');
});

Route::controller(OrderController::class)->prefix('order')->name('order_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'listData')->name('table');
    Route::get('/view/{id}', 'show')->name('view');
});

Route::controller(TrainingPlanController::class)->prefix('training-plan')->name('training_plan_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'listData')->name('table');
    Route::get('/edit/{id}', 'edit')->name('edit');
    Route::post('/update', 'update')->name('update');
    Route::get('/add', 'create')->name('add');
    Route::get('trainer/add/{trainerId}', 'create')->name('trainer_add');
    Route::get('center/add/{centerId}', 'create')->name('center_add');
    Route::post('/add', 'store')->name('add_data');
    Route::get('/delete/{id}', 'destroy')->name('delete');
    Route::controller(TrainingPlanBannerController::class)->prefix('banner')->name('banner_')->group(function () {
        Route::get('/list/{planId}', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/add/{planId}', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}/{planId}', 'destroy')->name('delete');
    });
});

Route::controller(ActivityController::class)->prefix('activity')->name('activity_')->middleware('auth')->group(function () {
    Route::get('/list', 'index')->name('list');
    Route::get('/table', 'listData')->name('table');
    Route::get('/edit/{id}', 'edit')->name('edit');
    Route::post('/update', 'update')->name('update');
    Route::get('/add', 'create')->name('add');
    Route::post('/add', 'store')->name('add_data');
    Route::get('/delete/{id}', 'destroy')->name('delete');
});

Route::prefix('banner')->name('banner_')->middleware('auth')->group(function () {
    Route::controller(HomeBannerController::class)->prefix('home')->name('home_')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
    });
    Route::controller(StoreBannerController::class)->prefix('store')->name('store_')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
    });
    Route::controller(CenterBannerController::class)->prefix('center')->name('center_')->group(function () {
        Route::get('/list', 'index')->name('list');
        Route::get('/table', 'listData')->name('table');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/add', 'create')->name('add');
        Route::post('/add', 'store')->name('add_data');
        Route::get('/delete/{id}', 'destroy')->name('delete');
    });
});