<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->comment('trainer id - users.id - null for center plan');
            $table->foreign('user_id')->references('id')->on('users')->nullOnDelete();
            $table->unsignedBigInteger('center_id')->nullable()->comment('center id - users.id - null for trainer plan');
            $table->foreign('center_id')->references('id')->on('users')->nullOnDelete();
            $table->string('title_en');
            $table->string('title_ar');
            $table->string('sub_title_en');
            $table->string('sub_title_ar');
            $table->text('description_en');
            $table->text('description_ar');
            $table->unsignedInteger('num_of_trainers');
            $table->string('thumbnail_img');
            $table->string('slug')->unique();
            $table->decimal('price', 8, 2);
            $table->foreignId('currency_id')->constrained();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_plans');
    }
};
