<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('booking_number', 25);
            $table->foreignId('user_id')->nullable()->comment('booked by')->constrained()->onDelete('cascade');
            $table->string('guest_user_id', 20)->nullable();
            $table->unsignedBigInteger('trainer_id')->nullable();
            $table->foreign('trainer_id')->references('id')->on('users')->nullOnDelete();
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->foreign('plan_id')->references('id')->on('training_plans')->nullOnDelete();
            $table->unsignedBigInteger('center_id')->nullable();
            $table->foreign('center_id')->references('id')->on('users')->nullOnDelete();
            $table->enum('service_type', ['trainer', 'center']);
            $table->date('appointment_date')->nullable();
            $table->string('appointment_time', 20)->nullable();
            $table->enum('payment_type', ['cod', 'online'])->default('cod');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
