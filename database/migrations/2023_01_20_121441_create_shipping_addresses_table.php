<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained()->onDelete('cascade');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone_code', 7);
            $table->unsignedBigInteger('phone');
            $table->string('country');
            $table->string('state')->nullable();
            $table->string('pincode');
            $table->string('street')->nullable();
            $table->string('area')->nullable();
            $table->string('block')->nullable();
            $table->string('building')->nullable();
            $table->enum('type', ['home', 'work', 'other']);
            $table->enum('gender', ['male', 'female', 'other'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_addresses');
    }
};
