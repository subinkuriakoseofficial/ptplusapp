<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade')->comment('trainer id - users.id');
            $table->unsignedDecimal('rating', 1, 1)->nullable();
            $table->string('phone')->nullable();
            $table->enum('status', ['Available', 'Not Available', 'Temporarily Not Available', 'On Leave'])->default('Available');
            $table->tinyInteger('experience')->comment('in years');
            $table->text('about')->nullable();
            $table->string('address')->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_details');
    }
};
