<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->string('order_number', 25)->after('id');
            $table->foreignId('user_id')->nullable()->after('order_number')->constrained()->onDelete('cascade');
            $table->string('guest_user_id', 20)->after('user_id')->nullable();
            $table->enum('status', ['Placed', 'Processing', 'In progress', 'Shipped', 'Delivered', 'Completed', 'Cancelled', 'Refunded'])->nullable()->after('quantity')->default('Placed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('order_number');
            $table->dropColumn('user_id');
            $table->dropColumn('guest_user_id');
            $table->dropColumn('status');
        });
    }
};
