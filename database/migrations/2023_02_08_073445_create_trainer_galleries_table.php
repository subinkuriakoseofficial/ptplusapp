<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_galleries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('trainer id - users.id')->constrained()->onDelete('cascade');
            $table->foreignId('training_plan_id')->nullable()->constrained()->onDelete('cascade');
            $table->string('video');
            $table->string('thumbnail_img');
            $table->enum('type', ['free', 'paid']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_galleries');
    }
};
