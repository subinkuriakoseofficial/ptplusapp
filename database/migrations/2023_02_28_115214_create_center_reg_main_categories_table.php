<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('center_reg_main_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('center id - users.id')->constrained()->onDelete('cascade');
            $table->foreignId('main_category_id')->comment('main_categories.id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('center_reg_main_categories');
    }
};
