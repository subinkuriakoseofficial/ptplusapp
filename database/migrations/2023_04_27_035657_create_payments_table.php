<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->nullable()->constrained()->restrictOnDelete();
            $table->foreignId('booking_id')->nullable()->constrained()->restrictOnDelete();
            $table->foreignId('user_id')->nullable()->constrained()->restrictOnDelete();
            $table->string('guest_user_id', 20)->nullable();
            $table->string('knet_payment_type')->nullable();
            $table->string('knet_payment_id')->comment('Knet order id - important for future reference');
            $table->string('knet_result')->comment('CANCELED or CAPTURED');
            $table->string('knet_order_id')->comment('sent as parameter while making payment request');
            $table->string('knet_post_date')->nullable();
            $table->string('knet_transaction_id')->nullable();
            $table->string('knet_reference_id')->nullable();
            $table->string('knet_track_id')->nullable();
            $table->string('knet_auth')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
