<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->date('delivery_date')->nullable()->comment('date choosen by customer');
            $table->enum('delivery_type', ['normal', 'express']);
            $table->enum('payment_type', ['cod', 'online']);
            $table->decimal('sub_total', 8, 2);
            $table->decimal('delivery_charge', 8, 2);
            $table->decimal('total_amount', 8, 2);
            $table->string('currency', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
