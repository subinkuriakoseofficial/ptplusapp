<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Review;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Training plans review
        Review::create(
            [
                'user_id' => 17,
                'training_plan_id' => 1,
                'rating' => 2.1,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'training_plan_id' => 1,
                'rating' => 3,
                'review' => 'dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'training_plan_id' => 1,
                'rating' => 3.5,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'training_plan_id' => 1,
                'rating' => 5,
                'review' => 'ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 21,
                'training_plan_id' => 1,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'training_plan_id' => 2,
                'rating' => 2.1,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'training_plan_id' => 2,
                'rating' => 3,
                'review' => 'dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'training_plan_id' => 2,
                'rating' => 3.5,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'training_plan_id' => 2,
                'rating' => 5,
                'review' => 'ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 21,
                'training_plan_id' => 2,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'training_plan_id' => 36,
                'rating' => 2.1,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'training_plan_id' => 36,
                'rating' => 3,
                'review' => 'dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'training_plan_id' => 36,
                'rating' => 3.5,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'training_plan_id' => 36,
                'rating' => 5,
                'review' => 'ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 21,
                'training_plan_id' => 36,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'training_plan_id' => 35,
                'rating' => 2.1,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'training_plan_id' => 35,
                'rating' => 3,
                'review' => 'dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'training_plan_id' => 35,
                'rating' => 3.5,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'training_plan_id' => 35,
                'rating' => 5,
                'review' => 'ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 21,
                'training_plan_id' => 35,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'trainer_id' => 2,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'trainer_id' => 2,
                'rating' => 3,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'trainer_id' => 2,
                'rating' => 3.5,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'trainer_id' => 2,
                'rating' => 3.5,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create( 
            [
                'user_id' => 21,
                'trainer_id' => 2,
                'rating' => 4,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'trainer_id' => 3,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'trainer_id' => 3,
                'rating' => 3,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'trainer_id' => 3,
                'rating' => 3.5,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'trainer_id' => 3,
                'rating' => 3.5,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create( 
            [
                'user_id' => 21,
                'trainer_id' => 3,
                'rating' => 4,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'trainer_id' => 4,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'trainer_id' => 4,
                'rating' => 3,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'trainer_id' => 4,
                'rating' => 3.5,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'trainer_id' => 4,
                'rating' => 3.5,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create( 
            [
                'user_id' => 21,
                'trainer_id' => 4,
                'rating' => 4,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'trainer_id' => 5,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'trainer_id' => 5,
                'rating' => 3,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'trainer_id' => 5,
                'rating' => 3.5,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'trainer_id' => 5,
                'rating' => 3.5,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create( 
            [
                'user_id' => 21,
                'trainer_id' => 5,
                'rating' => 4,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 17,
                'trainer_id' => 6,
                'rating' => 2,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 18,
                'trainer_id' => 6,
                'rating' => 3,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 19,
                'trainer_id' => 6,
                'rating' => 3.5,
                'review' => 'consectetur adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 20,
                'trainer_id' => 6,
                'rating' => 3.5,
                'review' => 'Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );

        Review::create(
            [
                'user_id' => 21,
                'trainer_id' => 6,
                'rating' => 4,
                'review' => 'adipiscing elit. Phasellus condimentum enim et lacus pellentesque ultrices. Proin blandit lacus quis varius tincidunt. Vestibulum ultrices',
            ]
        );
    }
}
