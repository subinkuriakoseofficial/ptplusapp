<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerRegMainCategory;

class TrainerRegMainCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerRegMainCategory::truncate();
        
        TrainerRegMainCategory::create(
            [
                'user_id' => 2,
                'main_category_id' => 1,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 2,
                'main_category_id' => 2,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 3,
                'main_category_id' => 1,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 3,
                'main_category_id' => 2,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 3,
                'main_category_id' => 3,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 4,
                'main_category_id' => 1,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 5,
                'main_category_id' => 1,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 5,
                'main_category_id' => 2,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 5,
                'main_category_id' => 3,
            ]
        );

        TrainerRegMainCategory::create(
            [
                'user_id' => 6,
                'main_category_id' => 2,
            ]
        );
    }
}
