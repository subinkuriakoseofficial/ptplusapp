<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create(
            [
                'name_en' => 'Life Fitness',
                'name_ar' => 'حياة الرشاقة',
                'slug' => 'life-fitness',
                'status' => 1
            ]
        );

        Brand::create(
            [
                'name_en' => 'Sports Art',
                'name_ar' => 'الفن الرياضي',
                'slug' => 'sports-art',
                'status' => 1
            ]
        );

        Brand::create(
            [
                'name_en' => 'Fitline',
                'name_ar' => 'Fitline',
                'slug' => 'fitline',
                'status' => 1
            ]
        );

        Brand::create(
            [
                'name_en' => 'Adidas',
                'name_ar' => 'Adidas',
                'slug' => 'adidas',
                'status' => 1
            ]
        );

        Brand::create(
            [
                'name_en' => 'Nike',
                'name_ar' => 'Nike',
                'slug' => 'nike',
                'status' => 1
            ]
        );
    }
}
