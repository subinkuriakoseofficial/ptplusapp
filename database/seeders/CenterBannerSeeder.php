<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterBanner;

class CenterBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterBanner::create(
            [
                'title_en' => 'Turn your goals into reality',
                'title_ar' => 'حول أهدافك إلى حقيقة',
                'description_en' => 'Strength does not come from physical capacity. It comes from an indomitable will',
                'description_ar' => 'القوة لا تأتي من ال القدرة الشخصية. إنها تنبع من إرادة لاتقهر',
                'image' => 'banner/banner-1.jpg',
            ]
        );

        CenterBanner::create(
            [
                'title_en' => 'The body achieves what the mind believes',
                'title_ar' => 'الجسم يحقق ما يؤمن العقل',
                'description_en' => 'If you want something you have never had, you must be willing to do something you have never done',
                'description_ar' => 'إذا كنت تريد شيئًا لم يكن لديك من قبل ، فيجب أن تكون على استعداد لفعل شيء لم تفعله من قبل',
                'image' => 'banner/banner-2.jpg',
            ]
        );

    }
}
