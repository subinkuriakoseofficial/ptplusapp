<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CenterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CenterTypesSeeder::class);
        $this->call(CenterBannerSeeder::class);
        $this->call(CenterCategorySeeder::class);
        $this->call(CenterDetailsSeeder::class);
        $this->call(CenterRegTrainerSeeder::class);
        $this->call(CenterPlanSeeder::class);
        $this->call(CenterProductSeeder::class);
        $this->call(CenterProductGallerySeeder::class);
        $this->call(CenterTrainingGallerySeeder::class);
    }
}
