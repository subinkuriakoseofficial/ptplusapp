<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\StoreBanner;

class StoreBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StoreBanner::create(
            [
                'title_en' => 'Save on bundles',
                'title_ar' => 'انقاذ في الحزم',
                'description_en' => 'Shop our full collection of must have bundles for your home gym and get free delivery',
                'description_ar' => 'تسوق مجموعتنا الكاملة من الحزم الضرورية لصالة الألعاب الرياضية المنزلية واحصل على توصيل مجاني',
                'image' => 'banner/banner-1.jpg',
                'product_id' => 1
            ]
        );

        StoreBanner::create(
            [
                'title_en' => 'New arrivals',
                'title_ar' => 'الوافدون الجدد',
                'description_en' => 'Shop our new arrivals and get free delivery',
                'description_ar' => 'تسوق من الوافدين الجدد واحصل على توصيل مجاني',
                'image' => 'banner/banner-2.jpg',
                'product_id' => 2
            ]
        );

        StoreBanner::create(
            [
                'title_en' => 'EOY sale',
                'title_ar' => 'بيع نهاية العام',
                'description_en' => 'Shop for 1999 KD above from our year end sale section and get free delivery',
                'description_ar' => 'تسوق بسعر 1999 د.ك أعلاه من قسم التخفيضات في نهاية العام واحصل على توصيل مجاني',
                'image' => 'banner/banner-3.jpg',
                'product_id' => 3
            ]
        );

        
    }
}
