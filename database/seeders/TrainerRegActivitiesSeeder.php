<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerRegActivity;

class TrainerRegActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerRegActivity::truncate();
        
        TrainerRegActivity::create(
            [
                'user_id' => 2,
                'activity_id' => 1,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 2,
                'activity_id' => 4,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 3,
                'activity_id' => 2,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 3,
                'activity_id' => 3,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 4,
                'activity_id' => 1,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 5,
                'activity_id' => 1,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 5,
                'activity_id' => 3,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 6,
                'activity_id' => 1,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 6,
                'activity_id' => 2,
            ]
        );

        TrainerRegActivity::create(
            [
                'user_id' => 6,
                'activity_id' => 4,
            ]
        );
    }
}
