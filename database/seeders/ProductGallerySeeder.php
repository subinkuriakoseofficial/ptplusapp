<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ProductGallery;

class ProductGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [1, 2, 3, 4, 5, 6, 7, 8];
        
        foreach($products as $prod){
            ProductGallery::create(
                [
                    'product_id' => $prod,
                    'image' => 'products/sample-product-gallery-9.jpg'
                ]
            );

            ProductGallery::create(
                [
                    'product_id' => $prod,
                    'image' => 'products/sample-product-gallery-11.jpg'
                ]
            );

            ProductGallery::create(
                [
                    'product_id' => $prod,
                    'image' => 'products/sample-product-gallery-10.jpg'
                ]
            );

            ProductGallery::create(
                [
                    'product_id' => $prod,
                    'image' => 'products/sample-product-gallery-12.jpg'
                ]
            );
        }
    }
}
