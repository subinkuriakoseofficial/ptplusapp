<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TrainerDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TrainerDetailsSeeder::class);
        $this->call(TrainerRegActivitiesSeeder::class);
        $this->call(TrainerRegMainCategoriesSeeder::class);
        $this->call(TrainerRegcategoriesSeeder::class);
        $this->call(TrainerRegSpecialitiesSeeder::class);
        $this->call(TrainerRegServiceTypeSeeder::class);
        $this->call(TrainerFavProductsSeeder::class);
        $this->call(TrainingPlansSeeder::class);
        $this->call(TrainerGallerySeeder::class);
        $this->call(TrainingSubPlansSeeder::class);
        $this->call(TrainingPlansBannerSeeder::class);
        $this->call(TrainerAvailableTimeSlotsSeeder::class);
    }
}
