<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegTrainer;

class CenterRegTrainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegTrainer::create(
            [
                'center_id' => 7,
                'trainer_id' => 2,
            ]
        );
        
        CenterRegTrainer::create(
            [
                'center_id' => 7,
                'trainer_id' => 3,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 7,
                'trainer_id' => 4,
            ]
        );
        
        CenterRegTrainer::create(
            [
                'center_id' => 7,
                'trainer_id' => 5,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 7,
                'trainer_id' => 6,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 8,
                'trainer_id' => 2,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 8,
                'trainer_id' => 3,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 9,
                'trainer_id' => 4,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 9,
                'trainer_id' => 5,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 9,
                'trainer_id' => 6,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 10,
                'trainer_id' => 4,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 10,
                'trainer_id' => 5,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 10,
                'trainer_id' => 6,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 11,
                'trainer_id' => 1,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 11,
                'trainer_id' => 3,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 11,
                'trainer_id' => 6,
            ]
        );

        CenterRegTrainer::create(
            [
                'center_id' => 11,
                'trainer_id' => 4,
            ]
        );
    }
}
