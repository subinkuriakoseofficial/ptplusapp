<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerAvailableTimeSlots;

class TrainerAvailableTimeSlotsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=5; $i<=11; $i++) {
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 2,
                    'time_slot' => $i.' AM',
                    'part' => 'Morning'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 3,
                    'time_slot' => $i.' AM',
                    'part' => 'Morning'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 4,
                    'time_slot' => $i.' AM',
                    'part' => 'Morning'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 5,
                    'time_slot' => $i.' AM',
                    'part' => 'Morning'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 6,
                    'time_slot' => $i.' AM',
                    'part' => 'Morning'
                ]
            );
        }

        for($i=5; $i<=10; $i++) {
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 2,
                    'time_slot' => $i.' PM',
                    'part' => 'Evening'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 3,
                    'time_slot' => $i.' PM',
                    'part' => 'Evening'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 4,
                    'time_slot' => $i.' PM',
                    'part' => 'Evening'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 5,
                    'time_slot' => $i.' PM',
                    'part' => 'Evening'
                ]
            );
            TrainerAvailableTimeSlots::create(
                [
                    'trainer_id' => 6,
                    'time_slot' => $i.' PM',
                    'part' => 'Evening'
                ]
            );
        }

        
    }
}
