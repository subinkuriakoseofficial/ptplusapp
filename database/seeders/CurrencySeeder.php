<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(
            [ 'name' => 'U.S. Dollar','symbol' => '$','exchange_rate' => '1.00000','status' => '1','code' => 'USD' ]
        );

        Currency::create(
            [ 'name' => 'Kuwaiti Dinar','symbol' => 'KD','exchange_rate' => '0.31','status' => '1','code' => 'KWD' ]
        );

        Currency::create(
            [ 'name' => 'United Arab Emirates Dirham','symbol' => 'د.إ','exchange_rate' => '3.67','status' => '1','code' => 'AED' ]
        );

        Currency::create(
            [ 'name' => 'Indian Rupee','symbol' => 'Rs','exchange_rate' => '68.45000','status' => '1','code' => 'Rupee' ]
        );

        Currency::create(
            [
                'name' => 'Australian Dollar','symbol' => '$','exchange_rate' => '1.28000','status' => '1','code' => 'AUD'
            ]
        );

        Currency::create(
            [
                'name' => 'Brazilian Real','symbol' => 'R$','exchange_rate' => '3.25000','status' => '1','code' => 'BRL'
            ]
        );

        Currency::create(
            [
                'name' => 'Canadian Dollar','symbol' => '$','exchange_rate' => '1.27000','status' => '1','code' => 'CAD'
            ]
        );

        Currency::create(
            [
                'name' => 'Czech Koruna','symbol' => 'Kč','exchange_rate' => '20.65000','status' => '1','code' => 'CZK'
            ]
        );

        Currency::create(
            [ 'name' => 'Danish Krone','symbol' => 'kr','exchange_rate' => '6.05000','status' => '1','code' => 'DKK' ]
        );

        Currency::create(
            [ 'name' => 'Euro','symbol' => '€','exchange_rate' => '0.85000','status' => '1','code' => 'EUR' ]
        );

        Currency::create(
            [ 'name' => 'Hong Kong Dollar','symbol' => '$','exchange_rate' => '7.83000','status' => '1','code' => 'HKD' ]
        );

        Currency::create(
            [ 'name' => 'Hungarian Forint','symbol' => 'Ft','exchange_rate' => '255.24000','status' => '1','code' => 'HUF' ]
        );

        Currency::create(
            [ 'name' => 'Israeli New Sheqel','symbol' => '₪','exchange_rate' => '3.48000','status' => '1','code' => 'ILS' ]
        );

        Currency::create(
            [ 'name' => 'Japanese Yen','symbol' => '¥','exchange_rate' => '107.12000','status' => '1','code' => 'JPY' ]
        );

        Currency::create(
            [ 'name' => 'Malaysian Ringgit','symbol' => 'RM','exchange_rate' => '3.91000','status' => '1','code' => 'MYR' ]
        );

        Currency::create(
            [ 'name' => 'Mexican Peso','symbol' => '$','exchange_rate' => '18.72000','status' => '1','code' => 'MXN' ]
        );

        Currency::create(
            [ 'name' => 'Norwegian Krone','symbol' => 'kr','exchange_rate' => '7.83000','status' => '1','code' => 'NOK' ]
        );

        Currency::create(
            [ 'name' => 'New Zealand Dollar','symbol' => '$','exchange_rate' => '1.38000','status' => '1','code' => 'NZD' ]
        );

        Currency::create(
            [ 'name' => 'Philippine Peso','symbol' => '₱','exchange_rate' => '52.26000','status' => '1','code' => 'PHP' ]
        );

        Currency::create(
            [ 'name' => 'Polish Zloty','symbol' => 'zł','exchange_rate' => '3.39000','status' => '1','code' => 'PLN' ]
        );

        Currency::create(
            [ 'name' => 'Pound Sterling','symbol' => '£','exchange_rate' => '0.72000','status' => '1','code' => 'GBP' ]
        );

        Currency::create(
            [ 'name' => 'Russian Ruble','symbol' => 'руб','exchange_rate' => '55.93000','status' => '1','code' => 'RUB' ]
        );

        Currency::create(
            [ 'name' => 'Singapore Dollar','symbol' => '$','exchange_rate' => '1.32000','status' => '1','code' => 'SGD' ]
        );

        Currency::create(
            [ 'name' => 'Swedish Krona','symbol' => 'kr','exchange_rate' => '8.19000','status' => '1','code' => 'SEK' ]
        );

        Currency::create(
            [ 'name' => 'Swiss Franc','symbol' => 'CHF','exchange_rate' => '0.94000','status' => '1','code' => 'CHF' ]
        );

        Currency::create(
            [ 'name' => 'Thai Baht','symbol' => '฿','exchange_rate' => '31.39000','status' => '1','code' => 'THB' ]
        );

        Currency::create(
            [ 'name' => 'Taka','symbol' => '৳','exchange_rate' => '84.00000','status' => '1','code' => 'BDT' ]
        );

    }
}
