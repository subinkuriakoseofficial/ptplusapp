<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterDetail;

class CenterDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterDetail::create(
            [
                'user_id' => 7,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );

        CenterDetail::create(
            [
                'user_id' => 8,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );

        CenterDetail::create(
            [
                'user_id' => 9,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );

        CenterDetail::create(
            [
                'user_id' => 10,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );

        CenterDetail::create(
            [
                'user_id' => 11,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );
    }
}
