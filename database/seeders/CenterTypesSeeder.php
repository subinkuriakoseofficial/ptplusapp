<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterType;

class CenterTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterType::create(
            [
                'name_en' => 'Club',
                'name_ar' => 'النادي',
                'thumbnail_img' => 'categories/club.jpg',
                'slug' => 'club',
            ]
        );

        CenterType::create(
            [
                'name_en' => 'Academies',
                'name_ar' => 'الأكاديميات',
                'thumbnail_img' => 'categories/academies.jpg',
                'slug' => 'academies',
            ]
        );

        CenterType::create(
            [
                'name_en' => 'Gym',
                'name_ar' => 'نادي رياضي',
                'thumbnail_img' => 'categories/gym.jpg',
                'slug' => 'gym',
            ]
        );
    }
}
