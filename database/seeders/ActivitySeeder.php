<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Activity;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::create(
            [
                'title_en' => 'Fitness',
                'title_ar' => 'لياقة بدنية',
                'thumbnail_img' => 'activities/fitness.png',
                'status' => 1,
                'slug' => 'fitness',
            ]
        );

        Activity::create(
            [
                'title_en' => 'Aerobic',
                'title_ar' => 'الهوائية',
                'thumbnail_img' => 'activities/aerobic.png',
                'status' => 1,
                'slug' => 'aerobic',
            ]
        );

        Activity::create(
            [
                'title_en' => 'Boxing',
                'title_ar' => 'ملاكمة',
                'thumbnail_img' => 'activities/boxing.png',
                'status' => 1,
                'slug' => 'boxing',
            ]
        );

        Activity::create(
            [
                'title_en' => 'Yoga',
                'title_ar' => 'يوجا',
                'thumbnail_img' => 'activities/yoga.png',
                'status' => 1,
                'slug' => 'yoga',
            ]
        );
    }
}
