<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ProductCategory;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [1, 2, 3, 4, 5, 6, 7, 8];
        
        foreach($products as $prod){
            ProductCategory::create(
                [
                    'product_id' => $prod,
                    'category_id' => 1
                ]
            );
    
            ProductCategory::create(
                [
                    'product_id' => $prod,
                    'category_id' => 16
                ]
            );
        }
        
    }
}
