<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = fopen(public_path('vendor/countries/worldcities.csv'), "r");
        $row = 0;
        while (($city = fgetcsv($file, 1000, ",")) !== FALSE) { 
            if($row != 0) { 
                $iso2 = $city[5];
                $country = Country::select('id')->where('iso2', $iso2)->first();
                if(isset($country->id) && $country->id != NULL) { 
                    City::create([
                        'country_id' => $country->id,
                        'name_en' => $city[0],
                        'name_ar' => $city[0],
                        'country_iso2' => $iso2,
                    ]);
                }
            }
            $row++;
        }
        fclose($file);
    }
}
