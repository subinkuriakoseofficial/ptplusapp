<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CenterCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'GYM',
                'name_ar' => 'نادي رياضي',
                'thumbnail_img' => 'categories/gym.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'gym-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Sports',
                'name_ar' => 'رياضات',
                'thumbnail_img' => 'categories/sports.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'sports-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Martial Arts',
                'name_ar' => 'الفنون العسكرية',
                'thumbnail_img' => 'categories/martial-arts.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'martial-arts-trainer-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Become a Trainer',
                'name_ar' => 'كن مدربًا',
                'thumbnail_img' => 'categories/become-center.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'become-a-trainer-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 26,
                'name_en' => 'Fitness',
                'name_ar' => 'لياقة بدنية',
                'thumbnail_img' => 'categories/running.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'fitness-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 27,
                'name_en' => 'Basketball',
                'name_ar' => 'كرة سلة',
                'thumbnail_img' => 'categories/basketball.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'basketball-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 27,
                'name_en' => 'Cricket',
                'name_ar' => 'كريكيت',
                'thumbnail_img' => 'categories/cricket.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'cricket-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 27,
                'name_en' => 'Football',
                'name_ar' => 'كرة القدم',
                'thumbnail_img' => 'categories/football.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'football-center',
            ]
        );

        Category::create(
            [
                'parent_id' => 27,
                'name_en' => 'Table Tennis',
                'name_ar' => 'تنس طاولة',
                'thumbnail_img' => 'categories/table-tennis.png',
                'type' => 'center',
                'status' => 1,
                'slug' => 'table-tennis-center',
            ]
        );
    }
}
