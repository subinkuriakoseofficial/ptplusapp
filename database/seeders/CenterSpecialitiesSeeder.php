<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegSpeciality;

class CenterSpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Open space and cardio zone',
                'speciality_ar' => 'مساحة مفتوحة ومنطقة القلب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Strength zone',
                'speciality_ar' => 'منطقة القوة',
            ]
        );
        
        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Weight training',
                'speciality_ar' => 'تدريب الوزن',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Functional training',
                'speciality_ar' => 'التدريب الوظيفي',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 7,
                'speciality_en' => 'Steam bath',
                'speciality_ar' => 'حمام البخار',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Open space and cardio zone',
                'speciality_ar' => 'مساحة مفتوحة ومنطقة القلب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Strength zone',
                'speciality_ar' => 'منطقة القوة',
            ]
        );
        
        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Weight training',
                'speciality_ar' => 'تدريب الوزن',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Functional training',
                'speciality_ar' => 'التدريب الوظيفي',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 8,
                'speciality_en' => 'Steam bath',
                'speciality_ar' => 'حمام البخار',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Open space and cardio zone',
                'speciality_ar' => 'مساحة مفتوحة ومنطقة القلب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Strength zone',
                'speciality_ar' => 'منطقة القوة',
            ]
        );
        
        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Weight training',
                'speciality_ar' => 'تدريب الوزن',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Functional training',
                'speciality_ar' => 'التدريب الوظيفي',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 9,
                'speciality_en' => 'Steam bath',
                'speciality_ar' => 'حمام البخار',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Open space and cardio zone',
                'speciality_ar' => 'مساحة مفتوحة ومنطقة القلب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Strength zone',
                'speciality_ar' => 'منطقة القوة',
            ]
        );
        
        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Weight training',
                'speciality_ar' => 'تدريب الوزن',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Functional training',
                'speciality_ar' => 'التدريب الوظيفي',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 10,
                'speciality_en' => 'Steam bath',
                'speciality_ar' => 'حمام البخار',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Open space and cardio zone',
                'speciality_ar' => 'مساحة مفتوحة ومنطقة القلب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Strength zone',
                'speciality_ar' => 'منطقة القوة',
            ]
        );
        
        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Weight training',
                'speciality_ar' => 'تدريب الوزن',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Functional training',
                'speciality_ar' => 'التدريب الوظيفي',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        CenterRegSpeciality::create(
            [
                'user_id' => 11,
                'speciality_en' => 'Steam bath',
                'speciality_ar' => 'حمام البخار',
            ]
        );
    }
}
