<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegMainCategory;

class CenterRegMainCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegMainCategory::create(
            [
                'user_id' => 7,
                'main_category_id' => 1,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 7,
                'main_category_id' => 2,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 8,
                'main_category_id' => 1,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 8,
                'main_category_id' => 2,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 8,
                'main_category_id' => 3,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 9,
                'main_category_id' => 1,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 10,
                'main_category_id' => 1,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 10,
                'main_category_id' => 2,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 10,
                'main_category_id' => 3,
            ]
        );

        CenterRegMainCategory::create(
            [
                'user_id' => 11,
                'main_category_id' => 2,
            ]
        );
    }
}
