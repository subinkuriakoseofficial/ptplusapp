<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerRegSpeciality;

class TrainerRegSpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerRegSpeciality::truncate();

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Strength and conditioning coach',
                'speciality_ar' => 'مدرب القوة والتكييف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Group excercise instructor',
                'speciality_ar' => 'مدرب تمارين جماعية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Fitness manager',
                'speciality_ar' => 'مدير اللياقة البدنية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Senior fitness specialist',
                'speciality_ar' => 'أخصائي أول لياقة بدنية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Emotional Intelligence and Empathy',
                'speciality_ar' => 'الذكاء العاطفي والتعاطف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 2,
                'speciality_en' => 'Communication Skill',
                'speciality_ar' => 'مهارة الاتصال',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 3,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 3,
                'speciality_en' => 'Emotional Intelligence and Empathy',
                'speciality_ar' => 'الذكاء العاطفي والتعاطف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 3,
                'speciality_en' => 'Communication Skill',
                'speciality_ar' => 'مهارة الاتصال',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 3,
                'speciality_en' => 'Motivational Skill',
                'speciality_ar' => 'مهارة تحفيزية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 4,
                'speciality_en' => 'Group excercise instructor',
                'speciality_ar' => 'مدرب تمارين جماعية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 4,
                'speciality_en' => 'Fitness manager',
                'speciality_ar' => 'مدير لياقة',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 4,
                'speciality_en' => 'Senior fitness specialist',
                'speciality_ar' => 'أخصائي أول لياقة بدنية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 4,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 4,
                'speciality_en' => 'Emotional Intelligence and Empathy',
                'speciality_ar' => 'الذكاء العاطفي والتعاطف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 5,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 5,
                'speciality_en' => 'Emotional Intelligence and Empathy',
                'speciality_ar' => 'الذكاء العاطفي والتعاطف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 5,
                'speciality_en' => 'Communication Skill',
                'speciality_ar' => 'مهارة الاتصال',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 5,
                'speciality_en' => 'Motivational Skill',
                'speciality_ar' => 'مهارة تحفيزية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 5,
                'speciality_en' => 'Patience',
                'speciality_ar' => 'الصبر',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Group excercise instructor',
                'speciality_ar' => 'مدرب تمارين جماعية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Fitness manager',
                'speciality_ar' => 'مدير لياقة',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Senior fitness specialist',
                'speciality_ar' => 'أخصائي أول لياقة بدنية',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Youth fitness specialist',
                'speciality_ar' => 'اخصائي لياقة الشباب',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Emotional Intelligence and Empathy',
                'speciality_ar' => 'الذكاء العاطفي والتعاطف',
            ]
        );

        TrainerRegSpeciality::create(
            [
                'user_id' => 6,
                'speciality_en' => 'Technical Knowledge',
                'speciality_ar' => 'المعرفة التقنية',
            ]
        );
    }
}
