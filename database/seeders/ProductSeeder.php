<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(
            [
                'name_en' => 'Olympic Rubber Bumper Plates - Pair',
                'name_ar' => 'ألواح مطاطية أولمبية - زوج',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-1.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 2999,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'olympic-rubber-bumper-plates-pair',
            ]
        );

        Product::create(
            [
                'name_en' => 'Olympic Barbell',
                'name_ar' => 'الحديد الاولمبي',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-2.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 4999,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'olympic-barbell',
            ]
        );

        Product::create(
            [
                'name_en' => 'Cerakote Olympic Barbell',
                'name_ar' => 'الحديد الاولمبي',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-3.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 1789,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'cerakote-olympic-barbell',
            ]
        );

        Product::create(
            [
                'name_en' => 'SF Curl Bar',
                'name_ar' => 'SF حليقة بار',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-4.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 2599,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'sf-curl-bar',
            ]
        );

        Product::create(
            [
                'name_en' => 'Black Bumper Plates 2.0 - 150 kgs',
                'name_ar' => 'ألواح سوداء ممتصة للصدمات من 2.0 - 150 كجم',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-5.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 3999,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'black-bumper-plates-20-150-kgs',
            ]
        );

        Product::create(
            [
                'name_en' => 'SF Care Kit',
                'name_ar' => 'طقم العناية SF',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-6.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 2400,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'sf-care-kit',
            ]
        );

        Product::create(
            [
                'name_en' => 'Fractional Plates',
                'name_ar' => 'لوحات كسور',
                'added_by' => 12,
                'thumbnail_img' => 'products/sample-product-7.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 7999,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'fractional-plates',
            ]
        );

        Product::create(
            [
                'name_en' => 'Rubber Bumper Plates',
                'name_ar' => 'لوحات المطاط الوفير',
                'added_by' => 13,
                'thumbnail_img' => 'products/sample-product-1.jpg',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius augue mi, a mattis orci molestie fermentum. Vivamus interdum est eleifend mauris convallis commodo. Fusce luctus vitae neque id pellentesque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum fringilla, sapien condimentum condimentum interdum, libero ex porttitor eros, a vehicula ligula diam eget diam. Phasellus sagittis magna nec posuere pulvinar. In elementum accumsan bibendum. Praesent a velit consequat, bibendum lacus in, aliquet nunc. Quisque porta eu magna vitae feugiat. Maecenas augue nisi, ultrices eget quam a, fermentum congue purus. Vivamus facilisis sapien quis euismod congue. Donec et lorem ipsum. Donec vel eros et nunc sagittis maximus. Praesent vel neque massa. Aliquam tincidunt laoreet justo non hendrerit. Ut vel turpis dui.',
                'description_ar' => 'التمرين هو في الأساس أي نشاط بدني نقوم به على أساس متكرر لإرخاء أجسامنا وإزالة كل الإجهاد الذهني. من المهم القيام بالتمارين الرياضية بانتظام. عندما تفعل هذا بشكل يومي ، تصبح لائقًا بدنيًا وعقليًا. علاوة على ذلك ، فإن عدم ممارسة الرياضة يوميًا يمكن أن يجعل الشخص عرضة للإصابة بأمراض مختلفة. وهكذا ، تمامًا مثل تناول الطعام يوميًا ، يجب علينا أيضًا ممارسة الرياضة يوميًا. ستلقي أهمية مقال التمرين مزيدًا من الضوء عليها.',
                'unit_price' => 1999,
                'currency_id' => 2,
                'current_stock' => 100,
                'status' => 1,
                'approved' => 1,
                'slug' => 'rubber-bumper-plates',
            ]
        );

    }
}
