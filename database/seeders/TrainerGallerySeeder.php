<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerGallery;

class TrainerGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerGallery::truncate();

        TrainerGallery::create(
            [
                'user_id' => 2,
                'video' => 'gallery/training-aerobic-1.mp4',
                'thumbnail_img' => 'gallery/training-aerobic-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 2,
                'video' => 'gallery/training-boxing-1.mp4',
                'thumbnail_img' => 'gallery/training-boxing-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 2,
                'video' => 'gallery/training-fitness-1.mp4',
                'thumbnail_img' => 'gallery/training-fitness-1.jpg',
                'type' => 'paid'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 2,
                'video' => 'gallery/training-gym-1.mp4',
                'thumbnail_img' => 'gallery/training-gym-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 3,
                'video' => 'gallery/training-gym-2.mp4',
                'thumbnail_img' => 'gallery/training-gym-2.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 3,
                'video' => 'gallery/training-home-workout-1.mp4',
                'thumbnail_img' => 'gallery/training-home-workout-1.jpg',
                'type' => 'paid'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 3,
                'video' => 'gallery/training-running-1.mp4',
                'thumbnail_img' => 'gallery/training-running-1.jpg',
                'type' => 'paid'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 4,
                'video' => 'gallery/training-running-1.mp4',
                'thumbnail_img' => 'gallery/training-running-1.jpg',
                'type' => 'paid'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 4,
                'video' => 'gallery/training-home-workout-1.mp4',
                'thumbnail_img' => 'gallery/training-home-workout-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 4,
                'video' => 'gallery/training-gym-2.mp4',
                'thumbnail_img' => 'gallery/training-gym-2.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 5,
                'video' => 'gallery/training-gym-1.mp4',
                'thumbnail_img' => 'gallery/training-gym-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 5,
                'video' => 'gallery/training-fitness-1.mp4',
                'thumbnail_img' => 'gallery/training-fitness-1.jpg',
                'type' => 'paid'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 5,
                'video' => 'gallery/training-boxing-1.mp4',
                'thumbnail_img' => 'gallery/training-boxing-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 5,
                'video' => 'gallery/training-aerobic-1.mp4',
                'thumbnail_img' => 'gallery/training-aerobic-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 6,
                'video' => 'gallery/training-running-1.mp4',
                'thumbnail_img' => 'gallery/training-running-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 6,
                'video' => 'gallery/training-fitness-1.mp4',
                'thumbnail_img' => 'gallery/training-fitness-1.jpg',
                'type' => 'free'
            ]
        );

        TrainerGallery::create(
            [
                'user_id' => 6,
                'video' => 'gallery/training-running-1.mp4',
                'thumbnail_img' => 'gallery/training-running-1.jpg',
                'type' => 'paid'
            ]
        );
    }
}
