<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Country;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Country::truncate();
        $countries = json_decode(file_get_contents(public_path('vendor/countries/countries.json')));
        foreach ($countries as $country) {
            $filePath = $country->flag;
            Storage::disk('public')->putFileAs('country', new File(public_path('vendor/countries/flags/' . $country->flag)), $country->flag);
            Country::create([
                'iso2' => $country->iso2,
                'iso3' => $country->iso3,
                'short_name_en' => $country->short_name_en,
                'short_name_ar' => $country->short_name_ar,
                'long_name' => $country->long_name,
                'flag' => 'country/' . $filePath,
                'status' => 1,
                'country_code' => $country->country_code,
            ]);
        }
    }
}
