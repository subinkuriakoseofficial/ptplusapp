<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class StoreCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'GYM',
                'name_ar' => 'نادي رياضي',
                'thumbnail_img' => 'categories/gym.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'gym-trainer',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Sports',
                'name_ar' => 'رياضات',
                'thumbnail_img' => 'categories/sports.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'sports-trainer',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Martial Arts',
                'name_ar' => 'الفنون العسكرية',
                'thumbnail_img' => 'categories/martial-arts.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'martial-arts-trainer',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Become a Trainer',
                'name_ar' => 'كن مدربًا',
                'thumbnail_img' => 'categories/become-trainer.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'become-a-trainer',
            ]
        );

        Category::create(
            [
                'parent_id' => 18,
                'name_en' => 'Basketball',
                'name_ar' => 'كرة سلة',
                'thumbnail_img' => 'categories/basketball.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'basketball',
            ]
        );

        Category::create(
            [
                'parent_id' => 18,
                'name_en' => 'Cricket',
                'name_ar' => 'كريكيت',
                'thumbnail_img' => 'categories/cricket.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'cricket',
            ]
        );

        Category::create(
            [
                'parent_id' => 18,
                'name_en' => 'Football',
                'name_ar' => 'كرة القدم',
                'thumbnail_img' => 'categories/football.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'football',
            ]
        );

        Category::create(
            [
                'parent_id' => 18,
                'name_en' => 'Running',
                'name_ar' => 'جري',
                'thumbnail_img' => 'categories/running.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'running',
            ]
        );

        Category::create(
            [
                'parent_id' => 18,
                'name_en' => 'Table Tennis',
                'name_ar' => 'تنس طاولة',
                'thumbnail_img' => 'categories/table-tennis.png',
                'type' => 'trainer',
                'status' => 1,
                'slug' => 'table-tennis',
            ]
        );
    }
}
