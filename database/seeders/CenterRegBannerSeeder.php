<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegBanner;

class CenterRegBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegBanner::create(
            [
                'center_id' => 7,
                'image' => 'center/banner-1.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 7,
                'image' => 'center/banner-2.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 7,
                'image' => 'center/banner-3.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 8,
                'image' => 'center/banner-1.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 8,
                'image' => 'center/banner-2.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 8,
                'image' => 'center/banner-3.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 9,
                'image' => 'center/banner-1.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 9,
                'image' => 'center/banner-2.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 9,
                'image' => 'center/banner-3.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 10,
                'image' => 'center/banner-1.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 10,
                'image' => 'center/banner-2.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 10,
                'image' => 'center/banner-3.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 11,
                'image' => 'center/banner-1.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 11,
                'image' => 'center/banner-2.jpg',
            ]
        );

        CenterRegBanner::create(
            [
                'center_id' => 11,
                'image' => 'center/banner-3.jpg',
            ]
        );

        
    }
}
