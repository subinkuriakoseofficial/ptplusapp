<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerFavProduct;

class TrainerFavProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerFavProduct::truncate();

        TrainerFavProduct::create(
            [
                'user_id' => 2,
                'product_id' => 1,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 2,
                'product_id' => 2,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 2,
                'product_id' => 3,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 2,
                'product_id' => 4,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 3,
                'product_id' => 4,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 3,
                'product_id' => 5,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 3,
                'product_id' => 6,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 4,
                'product_id' => 7,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 4,
                'product_id' => 8,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 5,
                'product_id' => 2,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 5,
                'product_id' => 3,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 5,
                'product_id' => 4,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 6,
                'product_id' => 1,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 6,
                'product_id' => 6,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 6,
                'product_id' => 3,
            ]
        );

        TrainerFavProduct::create(
            [
                'user_id' => 6,
                'product_id' => 4,
            ]
        );

    }
}
