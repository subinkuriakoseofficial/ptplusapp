<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerRegCategory;

class TrainerRegcategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerRegCategory::truncate();
        
        TrainerRegCategory::create(
            [
                'user_id' => 2,
                'category_id' => 17,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 3,
                'category_id' => 18,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 3,
                'category_id' => 21,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 4,
                'category_id' => 18,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 4,
                'category_id' => 21,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 5,
                'category_id' => 18,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 5,
                'category_id' => 23,
            ]
        );

        TrainerRegCategory::create(
            [
                'user_id' => 6,
                'category_id' => 19,
            ]
        );
    }
}
