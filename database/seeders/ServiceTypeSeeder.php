<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ServiceType;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceType::create(
            [
                'title_en' => 'Indoor',
                'title_ar' => 'داخلي',
                'group' => '1dsGd',
                'group_title_en' => 'Indoor / Outdoor',
                'group_title_ar' => 'داخلي وخارجي',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Outdoor',
                'title_ar' => 'في الخارج',
                'group' => '1dsGd',
                'group_title_en' => 'Indoor / Outdoor',
                'group_title_ar' => 'داخلي وخارجي',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Online',
                'title_ar' => 'متصل',
                'group' => '2dsGd',
                'group_title_en' => 'Online / Offline',
                'group_title_ar' => 'متصل غير متصل',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Offline',
                'title_ar' => 'غير متصل على الانترنت',
                'group' => '2dsGd',
                'group_title_en' => 'Online / Offline',
                'group_title_ar' => 'متصل غير متصل',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Personal Training',
                'title_ar' => 'تمرين شخصي',
                'group' => '3dsGd',
                'group_title_en' => 'Training Type',
                'group_title_ar' => 'نوع التدريب',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Group Training',
                'title_ar' => 'تدريب جماعي',
                'group' => '3dsGd',
                'group_title_en' => 'Training Type',
                'group_title_ar' => 'نوع التدريب',
            ]
        );

        ServiceType::create(
            [
                'title_en' => 'Home Training',
                'title_ar' => 'تدريب منزلي',
                'group' => '3dsGd',
                'group_title_en' => 'Training Type',
                'group_title_ar' => 'نوع التدريب',
            ]
        );
    }
}
