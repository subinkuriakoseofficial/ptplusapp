<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->admin()->create();
        User::factory(5)->trainer()->create();
        User::factory(5)->center()->create();
        User::factory(5)->vendor()->create();
        User::factory(5)->customer()->create();

        $this->call(MainCategorySeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ProductGallerySeeder::class);
        $this->call(StoreBannerSeeder::class);
        $this->call(ActivitySeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CountrySeeder::class);
        // $this->call(CitySeeder::class);
        $this->call(ServiceTypeSeeder::class);
        $this->call(StoreCategorySeeder::class);
        $this->call(TrainerDataSeeder::class);
        $this->call(CenterDataSeeder::class);
        $this->call(CenterDataSeeder1::class);
        $this->call(ReviewSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(CenterRegBannerSeeder::class);

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
