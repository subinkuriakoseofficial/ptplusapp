<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegType;

class CenterRegTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegType::create(
            [
                'user_id' => 7,
                'center_type_id' => 1,
            ]
        );

        CenterRegType::create(
            [
                'user_id' => 8,
                'center_type_id' => 2,
            ]
        );

        CenterRegType::create(
            [
                'user_id' => 9,
                'center_type_id' => 3,
            ]
        );

        CenterRegType::create(
            [
                'user_id' => 10,
                'center_type_id' => 3,
            ]
        );

        CenterRegType::create(
            [
                'user_id' => 11,
                'center_type_id' => 3,
            ]
        );

        CenterRegType::create(
            [
                'user_id' => 7,
                'center_type_id' => 3,
            ]
        );
    }
}
