<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Barbells & Plates',
                'name_ar' => 'الحديد والألواح',
                'thumbnail_img' => 'categories/barbells-plates.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'barbells-plates',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Cardio Machines',
                'name_ar' => 'آلات القلب',
                'thumbnail_img' => 'categories/cardio-machines.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'cardio-machines',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Gym Accessories',
                'name_ar' => 'اكسسوارات الصالة الرياضية',
                'thumbnail_img' => 'categories/gym-accessories.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'gym-accessories',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Crossfit Equipment',
                'name_ar' => 'معدات كروس فيت',
                'thumbnail_img' => 'categories/crossfit-equipment.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'crossfit-equipment',
            ]
        );

        Category::create(
            [
                'parent_id' => 0,
                'name_en' => 'Home Gym Equipment',
                'name_ar' => 'معدات رياضية منزلية',
                'thumbnail_img' => 'categories/home-gym-equipment.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'home-gym-equipment',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Clothings',
                'name_ar' => 'الملابس',
                'thumbnail_img' => 'categories/clothings.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'clothings',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Shoes',
                'name_ar' => 'أحذية',
                'thumbnail_img' => 'categories/shoes.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'shoes',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Gloves & Support',
                'name_ar' => 'القفازات والدعم',
                'thumbnail_img' => 'categories/gloves-support.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'gloves-support',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Gym Bags',
                'name_ar' => 'حقائب رياضية',
                'thumbnail_img' => 'categories/gym-bags.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'gym-bags',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Skipping Rope',
                'name_ar' => 'حبل النط',
                'thumbnail_img' => 'categories/skipping-rope.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'skipping-rope',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Gym Mat',
                'name_ar' => 'حصيرة الصالة الرياضية',
                'thumbnail_img' => 'categories/gym-mat.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'gym-mat',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Abs & Balance Equipment',
                'name_ar' => 'معدات Abs & Balance',
                'thumbnail_img' => 'categories/abs-balance-equipment.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'abs-balance-equipment',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Mobility',
                'name_ar' => 'إمكانية التنقل',
                'thumbnail_img' => 'categories/mobility.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'mobility',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Gym Ball & Pilate',
                'name_ar' => 'رياضة الكرة وبيلاتس',
                'thumbnail_img' => 'categories/gym-ball-pilate.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'gym-ball-pilate',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Resistance Training Bands',
                'name_ar' => 'رياضة الكرة وبيلاتس',
                'thumbnail_img' => 'categories/resistance-training-bands.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'resistance-training-bands',
            ]
        );

        Category::create(
            [
                'parent_id' => 3,
                'name_en' => 'Strength Training',
                'name_ar' => 'رياضة الكرة وبيلاتس',
                'thumbnail_img' => 'categories/strength-training.jpg',
                'type' => 'store',
                'status' => 1,
                'slug' => 'strength-training',
            ]
        );
    }
}
