<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerRegServiceType;

class TrainerRegServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerRegServiceType::truncate();

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 1,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 2,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 3,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 4,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 5,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 6,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 2,
                'service_type_id' => 7,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 1,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 2,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 3,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 4,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 5,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 3,
                'service_type_id' => 6,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 4,
                'service_type_id' => 1,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 5,
                'service_type_id' => 2,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 4,
                'service_type_id' => 3,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 5,
                'service_type_id' => 4,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 4,
                'service_type_id' => 5,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 5,
                'service_type_id' => 6,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 6,
                'service_type_id' => 1,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 6,
                'service_type_id' => 2,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 6,
                'service_type_id' => 3,
            ]
        );

        TrainerRegServiceType::create(
            [
                'user_id' => 6,
                'service_type_id' => 7,
            ]
        );
    }
}
