<?php

namespace Database\Seeders;

use App\Models\CenterRegType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CenterDataSeeder1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CenterRegTypesSeeder::class);
        $this->call(CenterRegCategoriesSeeder::class);
        $this->call(CenterRegMainCategoriesSeeder::class);
    }
}
