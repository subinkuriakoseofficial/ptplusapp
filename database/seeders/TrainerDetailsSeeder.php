<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainerDetail;

class TrainerDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainerDetail::truncate();
        
        TrainerDetail::create(
            [
                'user_id' => 2,
                'rating' => 4.2,
                'phone' => '+918874547875',
                'status' => 'Available',
                'experience' => 3,
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 2e2962t, Safat 13090, Kuwait City',
                'latitude' => 29.3900288,
                'longitude' => 48.0001531,
            ]
        );

        TrainerDetail::create(
            [
                'user_id' => 3,
                'rating' => 3,
                'phone' => '+918874547445',
                'status' => 'Available',
                'experience' => 5,
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'Shuwaikh Industry 15256, Safat 13090, Kuwait City',
                'latitude' => 29.3753574,
                'longitude' => 47.9776552,
            ]
        );

        TrainerDetail::create(
            [
                'user_id' => 4,
                'rating' => 3.5,
                'phone' => '+919974547415',
                'status' => 'Available',
                'experience' => 7,
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P.O.Box: 24204; 13103 Safat, Safat 13090, Kuwait City',
                'latitude' => 29.3715596,
                'longitude' => 47.9714572,
            ]
        );

        TrainerDetail::create(
            [
                'user_id' => 5,
                'rating' => 5,
                'phone' => '+919977788994',
                'status' => 'Available',
                'experience' => 4,
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'Po Box 44489 35089, Safat 13090, Kuwait City',
                'latitude' => 29.3715596,
                'longitude' => 47.9714572,
            ]
        );

        TrainerDetail::create(
            [
                'user_id' => 6,
                'rating' => 3,
                'phone' => '+915545445412',
                'status' => 'Available',
                'experience' => 6,
                'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis nisl placerat, viverra lectus nec, porttitor nisl. Maecenas congue tristique odio, a sagittis lorem aliquam ut. Phasellus a pellentesque nibh, ac ultrices justo. Nullam vestibulum iaculis tortor. Maecenas gravida augue sit amet eros tempor eleifend. Morbi ac lectus congue nulla finibus blandit quis sed ante. In maximus purus sapien, at molestie odio pulvinar ut. Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pulvinar est ac leo aliquet, eget accumsan magna suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur molestie nulla ac facilisis ullamcorper. Vivamus dapibus erat ex, ac mollis quam porta eu. Pellentesque mollis, neque eget lacinia faucibus, lectus magna fermentum nisi, eu auctor felis erat in dui.',
                'address' => 'P O Box: 21 Surra, Safat 13090, Kuwait City',
                'latitude' => 29.3715596,
                'longitude' => 47.9714572,
            ]
        );
    }
}
