<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create(
            [
                'title_en' => 'The Ultimate Functional Fitness Programme',
                'title_ar' => 'برنامج اللياقة الوظيفية النهائي',
                'description_en' => 'Strength does not come from physical capacity. It comes from an indomitable will',
                'description_ar' => 'القوة لا تأتي من ال القدرة الشخصية. إنها تنبع من إرادة لاتقهر',
                'image' => 'banner/home-banner-1.jpg',
            ]
        );

        Banner::create(
            [
                'title_en' => 'Turn your goals into reality',
                'title_ar' => 'حول أهدافك إلى حقيقة',
                'description_en' => 'Strength does not come from physical capacity. It comes from an indomitable will',
                'description_ar' => 'القوة لا تأتي من ال القدرة الشخصية. إنها تنبع من إرادة لاتقهر',
                'image' => 'banner/home-banner-2.jpg',
            ]
        );
    }
}
