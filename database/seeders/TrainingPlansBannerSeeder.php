<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainingPlansBanner;

class TrainingPlansBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainingPlansBanner::create(
            [
                'training_plan_id' => 1,
                'image' => 'plans/plan-basketball-3.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 1,
                'image' => 'plans/plan-basketball-4.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 1,
                'image' => 'plans/plan-basketball-1.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 2,
                'image' => 'plans/plan-basketball-2.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 2,
                'image' => 'plans/plan-basketball-5.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 3,
                'image' => 'plans/plan-basketball-4.jpg',
            ]
        );

        TrainingPlansBanner::create(
            [
                'training_plan_id' => 4,
                'image' => 'plans/weight-loss-1.jpg',
            ]
        );
    }
}
