<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\MainCategory;

class MainCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MainCategory::create(
            [
                'name_en' => 'MEN',
                'name_ar' => 'رجال',
                'thumbnail_img' => 'categories/men.jpg',
                'slug' => 'men',
            ]
        );

        MainCategory::create(
            [
                'name_en' => 'WOMEN',
                'name_ar' => 'النساء',
                'thumbnail_img' => 'categories/women.jpg',
                'slug' => 'women',
            ]
        );

        MainCategory::create(
            [
                'name_en' => 'KIDS',
                'name_ar' => 'أطفال',
                'thumbnail_img' => 'categories/kids.jpg',
                'slug' => 'kids',
            ]
        );
    }
}
