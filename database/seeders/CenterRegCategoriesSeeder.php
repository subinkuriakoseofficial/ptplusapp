<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CenterRegCategory;

class CenterRegCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CenterRegCategory::create(
            [
                'user_id' => 7,
                'category_id' => 26,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 8,
                'category_id' => 27,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 8,
                'category_id' => 30,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 9,
                'category_id' => 31,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 9,
                'category_id' => 31,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 10,
                'category_id' => 31,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 10,
                'category_id' => 33,
            ]
        );

        CenterRegCategory::create(
            [
                'user_id' => 11,
                'category_id' => 28,
            ]
        );
    }
}
