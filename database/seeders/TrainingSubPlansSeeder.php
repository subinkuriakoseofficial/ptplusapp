<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TrainingPlan;

class TrainingSubPlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrainingPlan::create(
            [
                'user_id' => 2,
                'parent_id' => 1,
                'title_en' => 'The Ultimate Basketball Training Program',
                'title_ar' => 'البرنامج التدريبي النهائي لكرة السلة',
                'sub_title_en' => '04 workouts for beginner',
                'sub_title_ar' => '04 تمارين للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '93',
                'thumbnail_img' => 'plans/plan-basketball-3.jpg',
                'slug' => 'the-ultimate-basketball-training-program-class-sub',
                'price' => '120',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 2,
                'parent_id' => 1,
                'title_en' => 'The 12 Months Basketball Training Program 1st class',
                'title_ar' => 'برنامج تدريب كرة السلة لمدة 12 شهرًا',
                'sub_title_en' => '1 year plan for beginners',
                'sub_title_ar' => 'خطة لمدة سنة للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '100',
                'thumbnail_img' => 'plans/plan-basketball-4.jpg',
                'slug' => 'the-12-months-basketball-training-program-sub',
                'price' => '225',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 2,
                'parent_id' => 1,
                'title_en' => '6 Week Basketball Training class 2',
                'title_ar' => 'تدريب كرة السلة لمدة 6 أسابيع',
                'sub_title_en' => '6 week for beginners',
                'sub_title_ar' => '6 أسابيع للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '50',
                'thumbnail_img' => 'plans/plan-basketball-1.jpg',
                'slug' => '6-week-basketball-training-sub',
                'price' => '190',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 2,
                'parent_id' => 1,
                'title_en' => '3 Basketball Practice Plans for All Age Groups (7 - 18 Years Old)',
                'title_ar' => '3 خطط لممارسة كرة السلة لجميع الفئات العمرية (من 7 إلى 18 عامًا)',
                'sub_title_en' => '4 workouts for beginners',
                'sub_title_ar' => '4 تمارين للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '75',
                'thumbnail_img' => 'plans/plan-basketball-2.jpg',
                'slug' => '3-basketball-practice-plans-for-all-age-groups-7-18-years-old-sub',
                'price' => '99',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 3,
                'parent_id' => 5,
                'title_en' => 'The Ultimate Basketball Training Program Preliminary',
                'title_ar' => 'البرنامج التدريبي النهائي لكرة السلة',
                'sub_title_en' => '04 workouts for beginner',
                'sub_title_ar' => '04 تمارين للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '93',
                'thumbnail_img' => 'plans/plan-basketball-5.jpg',
                'slug' => 'the-ultimate-basketball-training-program-3-sub',
                'price' => '120',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 3,
                'parent_id' => 5,
                'title_en' => 'The 12 Months Basketball Training Program Sub',
                'title_ar' => 'برنامج تدريب كرة السلة لمدة 12 شهرًا',
                'sub_title_en' => '1 year plan for beginners',
                'sub_title_ar' => 'خطة لمدة سنة للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '100',
                'thumbnail_img' => 'plans/plan-basketball-4.jpg',
                'slug' => 'the-12-months-basketball-training-program-3-sub',
                'price' => '225',
                'currency_id' => 2,
            ]
        );

        TrainingPlan::create(
            [
                'user_id' => 3,
                'parent_id' => 6,
                'title_en' => '3 Months Weight Loss Program sub',
                'title_ar' => 'برنامج إنقاص الوزن لمدة 3 أشهر',
                'sub_title_en' => 'Weight loss',
                'sub_title_ar' => 'خطة لمدة سنة للمبتدئين',
                'description_en' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in ante eu leo scelerisque luctus eu at nisi. Duis porttitor, leo eget molestie molestie, nulla justo lacinia sapien, vitae elementum velit est et metus. In congue dignissim viverra. Donec est lectus, vestibulum in porttitor sodales, luctus ornare velit. Nulla at feugiat velit. Nulla facilisi. Proin hendrerit egestas sem eu tristique. Nunc quis fermentum nisi, et pretium diam. Fusce posuere convallis ex non aliquam. Etiam rhoncus, dolor ac ullamcorper posuere, nulla diam aliquet felis, sed fringilla erat arcu eu neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'description_ar' => 'Lorem ipsum dolor sit amet، consectetur adipiscing elit. Aliquam في ante eu leo ​​scelerisque luctus eu at nisi. Duis porttitor، leo eget molestie، nulla justo lacinia sapien، vitae elementum velit est et metus. في كونيج كريمينسيم فيفيررا. Donec est lectus، vestibulum in porttitor sodales، luctus ornare velit. نولا في فوجيات فيليت. Nulla Facilisi. Proin هندريريت egestas sem eu tristique. Nunc quis fermentum nisi، et premium diam. صهر معقد غير مقشر. Etiam rhoncus، dolor ac ullamcorper posuere، nulla diam aliquet felis، sed fringilla erat arcu eu neque. الدهليز ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
                'num_of_trainers' => '100',
                'thumbnail_img' => 'plans/weight-loss-1.jpg',
                'slug' => '3-months-weight-loss-program-sub',
                'price' => '225',
                'currency_id' => 2,
            ]
        );
    }
}
